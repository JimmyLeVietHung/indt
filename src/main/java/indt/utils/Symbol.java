package indt.utils;

public class Symbol {
	public String displayName;

	public Symbol(String dname) {
		displayName = dname;
	}

    @Override
    public int hashCode() {
    	return displayName.hashCode();
    }
 
    @Override
    public boolean equals(Object obj) {
    	if(obj == null || obj.getClass() != getClass())
            return false;
    	return equals((Symbol)obj);
    }
    
    private boolean equals(Symbol s) {
    	return displayName.equals(s.displayName);
    }
}
