package indt.utils;

import java.util.HashMap;

public class SymbolIndex {
	public static final int MAXSYMBOLINDEX = 64;
	private HashMap<Symbol, Integer> sm = new HashMap<Symbol, Integer>();
	private Symbol[] im = new Symbol[MAXSYMBOLINDEX];
	private int idxcnt = 0;

	public boolean addSymbol(Symbol s) {
		if (s != null && idxcnt < MAXSYMBOLINDEX && getIndex(s) < 0) {
			sm.put(s, idxcnt);
			im[idxcnt] = s;
			idxcnt++;
			return true;
		}
		return false;
	}
	
	public int getIndex(Symbol s) {
		if (s == null)
			return -1;
		Integer idx = sm.get(s);
		if (idx == null)
			return -1;
		return idx.intValue();
	}
	
	public Symbol getSymbol(int idx) {
		if (idx < 0 || idx >= idxcnt)
			return null;
		return im[idx];
	}
	
	public int getCount() {
		return sm.size();
	}
}
