package indt.utils;

import java.util.ArrayList;
import java.util.HashMap;

import indt.event.BookEvent.ActionType;
import indt.event.Event;
import indt.event.EventListener;
import indt.market.BookListener;
import indt.market.BookOrder;

//Keeps track of all current prices and snapshot of historical prices
public class PricingManager implements BookListener, EventListener {
	//maps from symbolIndex to PriceDataPoint
	private HashMap<Integer, PriceDataPoint> latestPriceMap = new HashMap<Integer, PriceDataPoint>();
	private HashMap<Integer, ArrayList<PriceDataPoint>> priceHistoryMap = new HashMap<Integer, ArrayList<PriceDataPoint>>();
	private final int priceHistoryInterval = 60*1000; //every minute 
	
	private long timestamp; 
		
	@Override
	public void onEvent(Event e) {
		timestamp = e.timestamp;
	}
	
	@Override
	public void onBookChange(BookOrder bo, ActionType at, boolean done) {
		
		if (at == ActionType.ADD || at== ActionType.UPDATE) {
			int price = bo.px;
			int symbolIndexInt = bo.symbolIndex;
			Integer symbolIndexKey = new Integer(symbolIndexInt);
			
			//update latestPriceMap
			PriceDataPoint pdp = null;
			if (!latestPriceMap.containsKey(symbolIndexKey)) {
				pdp = new PriceDataPoint();
				latestPriceMap.put(symbolIndexKey, pdp);
			} else {
				pdp  = latestPriceMap.get(symbolIndexKey);
			}
			
			if (bo.side == 0) { //bid
				pdp.bid = price;
			} else { //ask
				pdp.ask = price;
			}
			pdp.pxDivisor = bo.pxDivisor;
			pdp.timestamp = timestamp;
			
			if (pdp.bid <0 || pdp.ask < 0) { //data not complete yet
				return;
			}
			
			//update priceHistoryMap
			ArrayList<PriceDataPoint> priceHistory = null;
			if (!priceHistoryMap.containsKey(symbolIndexKey)) {
				priceHistory = new ArrayList<PriceDataPoint>();
				priceHistoryMap.put(symbolIndexKey, priceHistory);
			} else {
				priceHistory  = priceHistoryMap.get(symbolIndexKey);
			}
			if (priceHistory.size() == 0 || (priceHistory.get(priceHistory.size()-1).timestamp < timestamp - priceHistoryInterval)) { //time to update
				priceHistory.add(pdp.getDeepCopy());
			}
		} 
	}
	
	public void reset() {
		latestPriceMap = new HashMap<Integer, PriceDataPoint>();
		priceHistoryMap = new HashMap<Integer, ArrayList<PriceDataPoint>>();
	}
	
	public int getRecentMovingAvg (int aSymbolIndexInt, long aLookback) {
		int total = 0;
		int count = 0;
		
		Integer symbolIndexKey = new Integer(aSymbolIndexInt);
		ArrayList<PriceDataPoint> priceHistory = priceHistoryMap.get(symbolIndexKey);
		if (priceHistory != null) {
			for (int i = priceHistory.size()-1; (i>= 0 && count < aLookback) ; i--) {
				PriceDataPoint pdp = priceHistory.get(i);
				int mid = (pdp.bid + pdp.ask) / 2;
				total += mid;
				count++;
			}
		}
		return total/count;
	}
	
	public int getMidNearTime(int aSymbolIndexInt, long aTime)
	{
		Integer symbolIndexKey = new Integer(aSymbolIndexInt);
		ArrayList<PriceDataPoint> priceHistory = priceHistoryMap.get(symbolIndexKey);
		if (priceHistory != null) {
			//TODO optimize later eg binary search
			for (int i = priceHistory.size()-1; i>= 0; i--) {
				PriceDataPoint pdp = priceHistory.get(i);
				if (pdp.timestamp < aTime) {
					return (pdp.bid + pdp.ask) / 2;
				}
			}
		}
		return -1;
	}
	
	public int getLatestMid(int aSymbolIndexInt) {
		int bid = getLatestBid(aSymbolIndexInt);
		int ask = getLatestAsk(aSymbolIndexInt);
		return (bid+ask)/2 ;
	}
	
	public int getLatestBid(int aSymbolIndexInt) {
		Integer symbolIndexKey = new Integer(aSymbolIndexInt);
		PriceDataPoint pdp = latestPriceMap.get(symbolIndexKey);
		if (pdp == null) {
			return -1;
		} else {
			return pdp.bid;
		}
	}
	
	public int getLatestAsk(int aSymbolIndexInt) {
		Integer symbolIndexKey = new Integer(aSymbolIndexInt);
		PriceDataPoint pdp = latestPriceMap.get(symbolIndexKey);
		if (pdp == null) {
			return -1;
		} else {
			return pdp.ask;
		}
	}
	
	public int getLatestDivisor(int aSymbolIndexInt) {
		Integer symbolIndexKey = new Integer(aSymbolIndexInt);
		PriceDataPoint pdp = latestPriceMap.get(symbolIndexKey);
		if (pdp == null) {
			return -1;
		} else {
			return pdp.pxDivisor;
		}
	}
	
	private class PriceDataPoint {
		public int bid = -1;
		public int ask = -1;
		public int pxDivisor = -1;
		public long timestamp = -1; 
		
		public PriceDataPoint getDeepCopy() {
			PriceDataPoint pdp = new PriceDataPoint();
			pdp.bid = bid;
			pdp.ask = ask;
			pdp.pxDivisor = pxDivisor;
			pdp.timestamp = timestamp;
			return pdp;
		}
	}

}
