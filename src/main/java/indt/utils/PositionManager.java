package indt.utils;

import java.util.HashMap;
import java.util.Vector;

//Keeps track of all existing trades & positions and calculates PnL
public class PositionManager {

	PricingManager pricingManager = null;
	private HashMap<Integer, Vector<TradeEntry>> tradeHistoryMap = new HashMap<Integer, Vector<TradeEntry>>();
	//maps from symbolIndexKey to number of lots
	private HashMap<Integer, Integer> positionMap = new HashMap<Integer, Integer>(); 
	
	public PositionManager (PricingManager aPricingManager) {
		pricingManager = aPricingManager;
	}
	
	//for theoretical analysis, take the price at mid
	public void TradeMid(int aSymbolIndexInt, int aSize) {
		int bid = pricingManager.getLatestBid(aSymbolIndexInt); 
		int ask = pricingManager.getLatestAsk(aSymbolIndexInt);
		if (bid <= 0 || ask <= 0) {
			System.err.println("Fatal .. Bid or Ask price not positive");
			return;
		}
		int mid = (bid+ask)/2;
		Trade(aSymbolIndexInt, aSize, mid);
	}
	
	//take the price by crossing spread
	public void TradeAggress(int aSymbolIndexInt, int aSize) {
		int price = -1;
		if (aSize < 0) { //selling
			price = pricingManager.getLatestBid(aSymbolIndexInt);
		} else { //buying
			price = pricingManager.getLatestAsk(aSymbolIndexInt);
		}
		Trade(aSymbolIndexInt, aSize, price);
	}
	
	public void Trade(int aSymbolIndexInt, int aSize, int aPrice) {
		if (aPrice <= 0) {
			System.err.println("Fatal .. Trading price not positive");
			return;
		}
		
		Integer symbolIndexKey = new Integer(aSymbolIndexInt);
		
		//add to tradeHistoryMap
		Vector<TradeEntry> tradeHistory = tradeHistoryMap.get(symbolIndexKey);
		if (tradeHistory == null) {
			tradeHistory = new Vector<TradeEntry>();
			tradeHistoryMap.put(symbolIndexKey, tradeHistory);
		}
		TradeEntry te = new TradeEntry(aSize, aPrice);
		tradeHistory.add(te);
		
		//update positionMap
		Integer position = positionMap.get(symbolIndexKey);
		if (position == null) {
			position = new Integer(aSize);
		} else {
			positionMap.remove(symbolIndexKey);
			position = position + aSize;
		}
		positionMap.put(symbolIndexKey, position);
		
	}
	
	public int GetPosition(int aSymbolIndexInt) {
		Integer symbolIndexKey = new Integer(aSymbolIndexInt);
		Integer position = positionMap.get(symbolIndexKey);
		if (position == null) {
			return 0;
		} else {
			return position.intValue();
		}
	}
	
	public long GetTotalPnL(int aSymbolIndexInt) {
		long totalPnL = 0;
		
		Integer symbolIndexKey = new Integer(aSymbolIndexInt);

		Vector<TradeEntry> tradeHistory = tradeHistoryMap.get(symbolIndexKey);
		if (tradeHistory != null) {
			int totalBuyPrice = 0;
			int totalBuySize = 0;
			int totalSellPrice = 0;
			int totalSellSize = 0;
			for (TradeEntry te: tradeHistory) {
				if (te.size > 0) { //buy
					totalBuyPrice += (te.size * te.price);
					totalBuySize += te.size;
				} else { //sell
					totalSellPrice += (-te.size * te.price);
					totalSellSize += (-te.size);
				}
			}
			
			int netBuySize = totalBuySize - totalSellSize;
			int mtmPrice;
			if (netBuySize > 0) { //remaining long, mtm with bid
				mtmPrice  = pricingManager.getLatestBid(symbolIndexKey.intValue());
				totalSellPrice += mtmPrice * netBuySize; 
			} else { //remaining short, mtm with ask 
				mtmPrice  = pricingManager.getLatestAsk(symbolIndexKey.intValue());
				totalBuyPrice += mtmPrice * (-netBuySize);
			}
			
			int pxDivisor = pricingManager.getLatestDivisor(symbolIndexKey.intValue());
			
			int preDivisorPnL = totalSellPrice - totalBuyPrice;
			totalPnL = preDivisorPnL * pxDivisor; 
		}
	
		return totalPnL;
	}
		
	private class TradeEntry {
		public int size;
		public int price;
		
		public TradeEntry (int aSize, int aPrice) {
			size = aSize;
			price = aPrice;
		}
	}
}
