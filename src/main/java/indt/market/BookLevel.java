package indt.market;

import java.util.*;

public class BookLevel {
	private int px;
	private int pxDivisor;
	private int orderCount;
	private HashMap<Long, BookOrder> orderMap = new HashMap<Long, BookOrder>();
	private LinkedList<BookOrder> orderList = new LinkedList<BookOrder>();

	public BookLevel(int p, int pd) {
		px = p;
		pxDivisor = pd;
		orderCount = 0;
	}
	
	public int getPx() {
		return px;
	}
	
	public int getPxDivisor() {
		return pxDivisor;
	}
	
	public int getOrderCount() {
		return orderCount;
	}
	
	public BookOrder findOrder(long oid, int sz) {
		if (oid > 0) {
			return orderMap.get(oid);
		} else {
			if (orderList.size() == 1) {
				return orderList.get(0);
			} else {
				ListIterator<BookOrder> it = orderList.listIterator();
				while (it.hasNext()) {
					BookOrder fbo = it.next();
					if (fbo.size == sz)
						return fbo;
				}
				return null;
			}
		}
	}


	public void addOrder(BookOrder bo) {
		if (bo.orderID > 0) {
			if (orderMap.get(bo.orderID) != null) {
				System.err.println("Fatal .. adding duplicate order id " + bo.orderID + " to book level map!");
				return;
			}
			orderMap.put(bo.orderID,  bo);
		} else {
			orderList.add(bo);
		}
		orderCount++;
	}


	public void removeOrder(long oid, int sz) {
		if (oid > 0) {
			if (orderMap.get(oid) == null) {
				System.err.println("Fatal .. removing non-existent order id " + oid + " from book level map!");
				return;
			}
			orderMap.remove(oid);
		} else {
			ListIterator<BookOrder> found = null;
			ListIterator<BookOrder> it = orderList.listIterator();
			while (it.hasNext()) {
				ListIterator<BookOrder> li = it;
				if (it.next().size == sz) {
					if (found != null) {
						System.err.println("Fatal .. cannot remove ambiguous order " + oid + ", " + px + ", " + sz);
						return;
					}
					found = li;
				}
			}
			if (found == null) {
				System.err.println("Fatal .. cannot find order by size " + sz);
				return;
			}
			found.remove();
		}
		orderCount--;
	}

	public LinkedList<BookOrder> getBookOrderList() {
		LinkedList<BookOrder> bolist = new LinkedList<BookOrder>();
		if (orderMap.size() > 0) {
			for (Long key : orderMap.keySet())
				bolist.add(orderMap.get(key));
		} else if (orderList.size() > 0) {
			//private LinkedList<BookOrder> orderList = new LinkedList<BookOrder>();
			ListIterator<BookOrder> it = orderList.listIterator();
			while (it.hasNext())
				bolist.add(it.next());
		}
		return bolist;
	}
}
