package indt.market;

import indt.event.*;

public interface BookListener {
	public void onBookChange(BookOrder bo, BookEvent.ActionType at, boolean done);
}
