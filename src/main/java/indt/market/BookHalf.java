package indt.market;

import java.util.*;
import indt.event.BookEvent.*;

public class BookHalf {
	private int side;
	private HashMap<Long, BookOrder> orderIDMap = new HashMap<Long, BookOrder>();
	private TreeMap<Integer, BookLevel> pxLvlMap = new TreeMap<Integer, BookLevel>();

	public BookHalf(int s) {
		side = s;
	}
	
	public int getSide() {
		return side;
	}

	public boolean processOrder(BookOrder bo, ActionType at) {
		// Sanity check
		if (bo.side != side) {
			System.err.println("Fatal .. cannot update book half " + side + " for book order side " + bo.side);
			return false;
		}

		// Update book half
		switch (at) {
		case ADD:
			// Add to book level
			getBookLevel(bo.px, bo.pxDivisor).addOrder(bo);
			
			// Add to order id map
			if (bo.orderID > 0) {
				if (orderIDMap.get(bo.orderID) != null) {
					System.err.println("Fatal .. adding duplicate order id " + bo.orderID + " to book half map!");
					return false;
				}
				orderIDMap.put(bo.orderID, bo);
			}
			break;
		case REMOVE:
			if (bo.size <= 0) {
				// Remove from book level
				getBookLevel(bo.px, bo.pxDivisor).removeOrder(bo.orderID, bo.size);

				// Remove empty book level
				removeBookLevel(bo.px);
				
				// Remove from order id map
				if (bo.orderID > 0) {
					if (orderIDMap.get(bo.orderID) == null) {
						System.err.println("Fatal .. removing non-existent order id " + bo.orderID + " from book half map!");
						return false;
					}
					orderIDMap.remove(bo.orderID);
				}
			}
			break;
		case UPDATE:
			// Px change or size change to 0
			if (bo.px != bo.oldPx || bo.size <= 0) {
				// Remove from old px level				
				getBookLevel(bo.oldPx, bo.pxDivisor).removeOrder(bo.oldOrderID, bo.size);
				
				// Remove empty book level
				removeBookLevel(bo.oldPx);
			}
			
			// Px change and non-trivial size
			if (bo.px != bo.oldPx && bo.size > 0)
				// Add to new px level
				getBookLevel(bo.px, bo.pxDivisor).addOrder(bo);

			// Order ID change or size change to 0
			if (bo.orderID > 0 && bo.oldOrderID > 0 && (bo.orderID != bo.oldOrderID || bo.size <= 0)) {
				// Remove old from order id map
				if (orderIDMap.get(bo.oldOrderID) == null) {
					System.err.println("Fatal .. removing non-existent old order id " + bo.oldOrderID + " from book half map!");
					return false;
				}
				orderIDMap.remove(bo.oldOrderID);
				
				// Add new to order id map
				if (bo.side > 0) {
					if (orderIDMap.get(bo.orderID) != null) {
						System.err.println("Fatal .. adding duplicate new order id " + bo.orderID + " to book half map!");
						return false;
					}
					orderIDMap.put(bo.orderID, bo);
				}
			}
			break;
		default:
			System.err.println("Unhandled book event action type " + at.ordinal());
			return false;
		}
		return true;
	}

	private BookLevel getBookLevel(int px, int pd) {
		BookLevel bl = pxLvlMap.get(px);
		if (bl == null) {
			bl = new BookLevel(px, pd);
			pxLvlMap.put(px,  bl);
		}
		return bl;
	}
	
	private Boolean removeBookLevel(int px) {
		BookLevel bl = pxLvlMap.get(px);
		if (bl == null)
			return true;
		if (bl.getOrderCount() <= 0) {
			pxLvlMap.remove(px);
			return true;
		}
		return false;
	}

	public BookOrder findOrder(long oid, int s, int px, int sz) {
		// Sanity check
		if (s != side) {
			System.err.println("Fatal .. cannot find book order side " + s + " in book half " + side);
			return null;
		}

		// Find
		if (oid > 0) {
			// By order id
			return orderIDMap.get(oid);
		} else {
			// By px level
			BookLevel bl = pxLvlMap.get(px);
			if (bl == null)
				return null;
			return bl.findOrder(-1, sz);
		}
	}
	
	public LinkedList<BookOrder> getBookOrderList() {
		LinkedList<BookOrder> bolist = new LinkedList<BookOrder>();
		for (Integer key : pxLvlMap.keySet()) {
			BookLevel bl = pxLvlMap.get(key);
			LinkedList<BookOrder> tlist = bl.getBookOrderList();
			bolist.addAll(tlist);
		}
		return bolist;
	}

	public BookOrder getTopOrder() {
		for (Integer key : pxLvlMap.keySet()) {
			BookLevel bl = pxLvlMap.get(key);
			return bl.getBookOrderList().getFirst();
		}
		return null;
	}
}
