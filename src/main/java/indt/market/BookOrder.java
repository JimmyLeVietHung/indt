package indt.market;

import indt.utils.MarketIndex.MarketMaker;

public class BookOrder {
	public int symbolIndex;
	public MarketMaker market;
	public int side;
	public int size;
	public int px;
	public int pxDivisor;
	public long orderID;
	public int oldSize;
	public int oldPx;
	public long oldOrderID;

	public BookOrder(int sym, MarketMaker mm, long oid, int s, int p, int pd, int sz) {
		symbolIndex = sym;
		market = mm;
		side = s;
		size = sz;
		px = p;
		pxDivisor = pd;
		orderID = oid;
		oldSize = -1;
		oldPx = -1;
		oldOrderID = -1;
	}
}
