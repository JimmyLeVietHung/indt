package indt.market;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

import indt.event.*;
import indt.event.BookEvent.*;
import indt.event.Event.*;
import indt.utils.*;
import indt.utils.MarketIndex.*;

public class Book implements EventListener {
	protected ArrayList<BookHalf[]> bookhalf = new ArrayList<BookHalf[]>();
	protected LinkedList<BookListener> bl = new LinkedList<BookListener>();
	protected SymbolIndex si;
	private MarketIndex.MarketMaker mm;

	public Book(EventLoop e, SymbolIndex _si) {
		this(e, _si, MarketIndex.MarketMaker.NA);
	}

	public Book(EventLoop e, SymbolIndex _si, MarketIndex.MarketMaker _mm) {
		si = _si;
		mm = _mm;
		if (e != null)
			e.addListener(this);
	}

	public MarketIndex.MarketMaker getMM() {
		return mm;
	}

	public void addListener(BookListener _bl) {
		bl.offer(_bl);
	}

	@Override
	public void onEvent(Event e) {
		// Filter book events
		if (e.getEventType() != EventType.BOOK)
			return;

		// Sanity check
		if (e.getMM() != mm) {
			System.err.println("Fatal .. inconsistent market maker in book " + e.getMM().ordinal() + ", " + mm.ordinal());
			return;
		}
		
		// Update book
		if (e.getEventType() == EventType.BOOK) {
			// BookEvents
			BookOrder bo = null;
			BookEvent be = (BookEvent)e;
			
			switch (be.getActionType()) {
			case ADD:
				bo = addOrder(be.symbolIndex, be.getMM(), be.orderID, be.side, be.px, be.pxDivisor, be.size);
				break;
			case REMOVE:
				bo = removeOrder(be.symbolIndex, be.getMM(), be.orderID, be.side, be.px, be.pxDivisor, be.size);
				break;
			case UPDATE:
				bo = updateOrder(be.symbolIndex, be.getMM(), be.orderID, be.side, be.px, be.pxDivisor, be.size, be.oldOrderID, be.oldPx, be.oldSize);
				break;
			default:
				System.err.println("Unhandled book event action type " + be.getActionType().ordinal());
				break;
			}
			if (bo != null)
				processOrder(bo, be.getActionType(), be.done);
			
		} else {
			System.err.println("Unhandled event type " + e.getEventType().ordinal());
		}
	}

	private BookOrder findOrder(int sym, MarketMaker mm, long oid, int s, int px, int sz) {
		BookHalf bh = getBookHalf(sym, s);
		return bh.findOrder(oid, s, px, sz);
	}
	
	private BookOrder addOrder(int sym, MarketMaker mm, long oid, int s, int px, int pd, int sz) {
		// Check if order exists
		BookOrder bo = findOrder(sym, mm, oid, s, px, sz);
		if (bo != null) {
			System.err.println("Possible duplicate order, attempting to add existing order (" + sym + ", " + mm.ordinal() + ", " + oid + ", " + s + ", " + px + ")");
			return null;
		}
		
		// Create new order
		BookOrder nbo = new BookOrder(sym, mm, oid, s, px, pd, sz);
		return nbo;
	}
	
	private BookOrder removeOrder(int sym, MarketMaker mm, long oid, int s, int px, int pd, int sz) {
		// Find existing order
		BookOrder bo = findOrder(sym, mm, oid, s, px, -1);
		if (bo == null) {
			System.err.println("Possible missing order, unable to find order to remove (" + sym + ", " + mm.ordinal() + ", " + oid + ", " + s + ", " + px + ")");
			return null;
		}

		// Adjust order size
		bo.oldSize = bo.size;
		bo.size = sz;
		return bo;
	}

	private BookOrder updateOrder(int sym, MarketMaker mm, long oid, int s, int px, int pd, int sz, long ooid, int opx, int osz) {
		// Find existing order
		BookOrder bo = findOrder(sym, mm, ooid, s, opx, osz);
		if (bo == null) {
			System.err.println("Possible missing order, unable to find order to update (" + sym + ", " + mm.ordinal() + ", " + ooid + ", " + s + ", " + opx + ", " + osz + ")");
			return null;
		}

		// Adjust order details
		bo.oldSize = bo.size;
		bo.oldPx = bo.px;
		bo.oldOrderID = bo.orderID;
		bo.size = sz;
		bo.px = px;
		bo.orderID = oid;
		return bo;
	}
	
	protected void processOrder(BookOrder bo, ActionType at, boolean done) {
		// Update respective book half
		BookHalf bh = getBookHalf(bo.symbolIndex, bo.side);
		if (bh.processOrder(bo, at)) {
			// Call book listeners
			for (BookListener l : bl)
				l.onBookChange(bo, at, done);
		}
	}

	public BookHalf getBookHalf(int symIdx, int s) {
		if (symIdx >= bookhalf.size()) {
			int delta = symIdx - bookhalf.size() + 1;
			for (int i = 0; i < delta; i++)
				bookhalf.add(new BookHalf[2]);
		}
		BookHalf bh = bookhalf.get(symIdx)[s];
		if (bh == null) {
			bh = new BookHalf(s);
			bookhalf.get(symIdx)[s] = bh; 
		}
		return bh;
	}

	public void prettyPrint(int symIdx) {
		// Collate book
		LinkedList<BookOrder> bidOrders = new LinkedList<BookOrder>();
		BookHalf bidhalf = getBookHalf(symIdx, 0);
		if (bidhalf != null)
			bidOrders = bidhalf.getBookOrderList();
		LinkedList<BookOrder> askOrders = new LinkedList<BookOrder>();
		BookHalf askhalf = getBookHalf(symIdx, 1);
		if (askhalf != null)
			askOrders = askhalf.getBookOrderList();

		// Print
		System.out.println(" *** Book for " + si.getSymbol(symIdx).displayName + " ***");
		ListIterator<BookOrder> bidIter = bidOrders.listIterator(bidOrders.size());
		ListIterator<BookOrder> askIter = askOrders.listIterator();
		while (bidIter.hasPrevious() || askIter.hasNext()) {
			if (bidIter.hasPrevious()) {
				BookOrder bidO = bidIter.previous();
				System.out.format(" %5s %8d %5d |", bidO.market, bidO.size, bidO.px);
			} else {
				System.out.print(" ----- -------- ----- |");
			}
			if (askIter.hasNext()) {
				BookOrder askO = askIter.next();
				System.out.format(" %5d %8d %5s\n", askO.px, askO.size, askO.market);
			} else {
				System.out.print(" ----- -------- -----\n");
			}
		}
	}
}
