package indt.market;

import java.util.*;

import indt.event.*;
import indt.event.Event.EventType;
import indt.utils.MarketIndex.MarketMaker;
import indt.utils.SymbolIndex;

public class AggrBook extends Book implements BookListener {
	private ArrayList<Book> books = new ArrayList<Book>();
	//protected LinkedList<BookListener> bl = new LinkedList<BookListener>();
	public AggrBook(EventLoop e, SymbolIndex s) {
		super(e, s);
	}

	@Override
	public void onEvent(Event e) {
		// Filter book events
		if (e.getEventType() != EventType.BOOK)
			return;

		// Push to market-specific book (note: self book will populate on book listener callback)
		Book mktbk = getBook(e.getMM());
		mktbk.onEvent(e);
	}
	
	public Book getBook(MarketMaker mm) {
		int mmIdx = mm.ordinal();
		if (mmIdx >= books.size()) {
			int delta = mmIdx - books.size() + 1;
			for (int i = 0; i < delta; i++)
				books.add(null);
		}
		Book mktbk = books.get(mmIdx);
		if (mktbk == null) {
			mktbk = new Book(null, si, mm);
			mktbk.addListener(this);
			books.set(mmIdx, mktbk);		
		}
		return mktbk;
	}
	
	@Override
	public void onBookChange(BookOrder bo, BookEvent.ActionType at, boolean done) {
		// Push to self book & call listeners
		processOrder(bo, at, done);
	}
}
