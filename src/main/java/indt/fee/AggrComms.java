package indt.fee;

import java.util.HashMap;

import indt.order.*;
import indt.order.Order.*;
import indt.utils.*;
import indt.utils.MarketIndex.*;

public class AggrComms extends Comms {
	public HashMap<Integer, Comms> mmcomms = new HashMap<Integer, Comms>();
	
	public AggrComms(SymbolIndex _si) {
		super(_si);
	}

	@Override
	protected void populateFeeSchedule() {}

	protected Comms getComms(MarketMaker mm) {
		Comms fees = mmcomms.get(mm.ordinal());
		if (fees == null) {
			// Try to initialize market-specific fee schedule
			switch (mm) {
			case SHFE:
				fees = new SHFEComms(si);
				mmcomms.put(mm.ordinal(), fees);
				break;
			default: break;
			}
		}
		return fees;
	}

	@Override
	public long getFees(Order o, boolean useFill) {
		Comms fees = getComms(o.mm);
		if (fees != null) {
			return fees.getFees(o, useFill);
		}
		System.err.println("ERROR: unable to initialize fee 1 schedule for " + o.mm);
		return 0;
	}
	
	@Override
	public long getFees(OrderType ot, int symbolIndex, OrderSide s, int sz, int px, int pxd, MarketMaker mm) {
		Comms fees = getComms(mm);
		if (fees != null) {
			return fees.getFees(ot, symbolIndex, s, sz, px, pxd, mm);
		}
		System.err.println("ERROR: unable to initialize fee 2 schedule for " + mm);
		return 0;
	}
}
