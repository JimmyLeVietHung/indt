package indt.fee;

import indt.order.*;
import indt.order.Order.*;
import indt.utils.MarketIndex.*;
import indt.utils.*;

public class Comms {
	protected SymbolIndex si;

	public Comms(SymbolIndex _si) {
		si = _si;
		populateFeeSchedule();
	}

	protected void populateFeeSchedule() {
		
		// TODO: define & populate generic|base fee schedule
		
	}
	
	public long getFees(Order o, boolean useFill) {
		if (useFill)
			return getFees(o.oType, o.symbolIndex, o.side, o.fillSize.getFirst(), o.fillPx.getFirst(), o.pxDivisor, o.mm);
		return getFees(o.oType, o.symbolIndex, o.side, o.currentSize.getFirst(), o.currentPx.getFirst(), o.pxDivisor, o.mm);
	}
	
	public long getFees(OrderType ot, int symbolIndex, OrderSide s, int sz, int px, int pxd, MarketMaker mm) {
		
		// TODO: compute fees
		
		return 0;
	}
}
