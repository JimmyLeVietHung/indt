package indt.strategy;

import java.util.ArrayList;

import indt.utils.*;
import indt.event.*;
import indt.datasource.*;
import indt.market.*;

public class TrendSimple implements EventListener, BookListener {
	private SymbolIndex si = new SymbolIndex();
	private AggrFileDataSource ads = new AggrFileDataSource();
	private EventLoop eloop = new EventLoop(si, ads);
	private AggrBook mb = new AggrBook(eloop, si);

	
	private int bidPrice = 0;
	private int askPrice = 0;
	private int midPrice = 0;
	private long latestTimeStamp = 0;
	private int latestHour = 0;
	private int cumPnl = 0;
	private int posPrice = 0;
	private int pos = 0; 
	
	private TrendMeter trendMeter = new TrendMeter();
		
	public static void main(String[] args) {
		TrendSimple st = new TrendSimple();

		Symbol sym = new Symbol("DCa0001");
		st.addSymbol(sym);	
		
		CNINFileDataSource d = new CNINFileDataSource(201309, sym, 1000);
		st.addSource(d);
		
		st.start();
		System.out.println("@@ DONE @@");
	}

	public void addSymbol(Symbol s) {
		si.addSymbol(s);
	}
	
	private void addSource(DataSource d) {
		ads.addSource((FileDataSource) d);
	}

	public void start() {
		// Add listeners
		eloop.addListener(this);
		mb.addListener(this);
		
		// Start
		eloop.start();
	}
	
	@Override
	public void onEvent(Event e) {
		latestTimeStamp = e.timestamp;
	}
	
	@Override
	public void onBookChange(BookOrder bo, BookEvent.ActionType at, boolean done) {
		if (bo.side == 0) { 
			bidPrice = bo.px;
		}
		else {
			askPrice = bo.px;
			midPrice = (bidPrice + askPrice) / 2;
			
			processStrategy();
		}
	}
	
	private void processStrategy()
	{
		trendMeter.recordReading(latestTimeStamp, midPrice);
		
		//int second = (int) ((latestTimeStamp / 1000) % 60);
		int minute = (int) ((latestTimeStamp / (1000 * 60)) % 60);
		int hour = (int) ((latestTimeStamp / (1000 * 60 * 60)) % 24);
		hour += 8; //convert from GMT to local
		int hourMinutes = (hour * 100) + minute;
		//int day = (int) (latestTimeStamp / (1000 * 60 * 60 * 24));
		
		if (hour < latestHour) { //Changed to next day. Close positions by end of day
			closePos();
			System.out.println("cumPnl: " + cumPnl);
			System.out.println();
		}
		latestHour = hour;
		
		if (hourMinutes >= 935 && hourMinutes <= 1425) { //only consider trading between these times
			int windowInMillisecs = 1000*60*30; //30 minutes
			trendMeter.calcSpeedAcceleration(windowInMillisecs);
			float speed = trendMeter.speed * 1000;
			float acceleration = trendMeter.acceleration * 1000;
			
			//System.out.println("Speed: "+ speed + " acceleration: " + acceleration);
			
			//Experiment of trend based on speed and acceleration
			//Buy when speed is high and acceleration is positive, Sell when vice-versa
			//Close when speed starts to flip
			if (pos == 0) {
				if (speed > 10 && acceleration > 1) {
					buy();
				}
				if (speed < -10 && acceleration < -1) {
					sell();
				}
			} 
			/*
			else if (pos == -1) {
				if (speed > 1) {
					buy();
				}
			} else if (pos == 1) {
				if (speed < -1) {
					sell();
				}
			}
			*/
			
			/*
			if (speed > 100) {
				if (pos == -1) {
					buy();
				} else if (pos == 0 && acceleration > 20) {
					buy();
				}
			}
			
			if (speed < -100) {
				if (pos == 1) {
					sell();
				} else if (pos == 0 && acceleration < -20) {
					sell();
				}
			}
			*/
		}
		
	}
	
	private void closePos() {
		if (pos > 0) {
			sell();
		}
		if (pos < 0) {
			buy();
		}
	}
	
	private void buy() {
		System.out.println("Buy");
		if (pos > 0 || pos < -1)
			System.err.println("Fatal .. can only buy when pos is -1 or 0");
		if (pos == -1) {
			//int pnl = posPrice - askPrice;
			int pnl = posPrice - midPrice;
			cumPnl += pnl;
		}
		//posPrice = askPrice;
		posPrice = midPrice;
		pos++;
		
	}
	
	private void sell() {
		System.out.println("Sell");
		if (pos > 1 || pos < 0)
			System.err.println("Fatal .. can only sell when pos is 0 or 1");
		if (pos == 1) {
			//int pnl = bidPrice - posPrice;
			int pnl = midPrice - posPrice;
			cumPnl += pnl;
		}
		//posPrice = bidPrice;
		posPrice = midPrice;
		pos--;
	}
	
	private class TrendMeter {
		
		private ArrayList<TimePrice> timeSeries = new ArrayList<TimePrice>();
		public float speed = 0;
		public float acceleration = 0; //not really acceleration; just a simple difference of speed between first half and second half of period
		
		public TrendMeter() {
		}
		
		public void recordReading(long time, int price) {
			timeSeries.add(new TimePrice(time,price));	
		}
		
		public void calcSpeedAcceleration(int windowInMillisecs) {
			TimePrice lastTP = timeSeries.get(timeSeries.size()-1);
			long lastTime = lastTP.time;
			int lastPrice = lastTP.price;
			long midTime = lastTime - (windowInMillisecs/2);
			long earlierTime = lastTime - windowInMillisecs;
			
			boolean speed2Set = false;
			float speed1 = 0;
			float speed2 = 0;
			
			for (int i = timeSeries.size()-1; i >= 0; i--) {
				TimePrice tp = timeSeries.get(i);
				if (tp.time < midTime && !speed2Set) { //found midTime
					float midPrice = tp.price;
					speed2 = (lastPrice - midPrice) / (lastTime - midTime);
					speed2Set = true;
				}
				if (tp.time < earlierTime) { //found earlierTime
					float earlierPrice = tp.price;
					speed1 = (lastPrice - earlierPrice) / (lastTime - earlierTime);
					speed = (speed1 + speed2) / 2;
					acceleration = speed2 - speed1;
					return;
				}
			}
		}
		
	}
	
	private class TimePrice {
		public long time;
		public  int price;
		public TimePrice(long aTime, int aPrice) {
			time = aTime;
			price = aPrice;
		}
	}
}
