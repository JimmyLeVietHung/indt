package indt.strategy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.logging.Level;

import org.apache.log4j.Logger;

import com.ib.client.ComboLeg;
import com.ib.client.CommissionReport;
import com.ib.client.EClientSocket;
import com.ib.client.EWrapper;
import com.ib.client.Execution;
import com.ib.client.Contract;
import com.ib.client.ContractDetails;
import com.ib.client.UnderComp;
import com.ib.client.Order;
import com.ib.client.OrderState;
import com.numericalmethod.suanshu.algebra.linear.matrix.doubles.matrixtype.dense.DenseMatrix;
import com.numericalmethod.suanshu.algebra.linear.vector.doubles.ImmutableVector;
import com.numericalmethod.suanshu.algebra.linear.vector.doubles.Vector;
import com.numericalmethod.suanshu.algebra.linear.vector.doubles.dense.DenseVector;
import com.numericalmethod.suanshu.stats.cointegration.CointegrationMLE;
import com.numericalmethod.suanshu.stats.cointegration.JohansenAsymptoticDistribution;
import com.numericalmethod.suanshu.stats.cointegration.JohansenTest;
import com.numericalmethod.suanshu.stats.test.timeseries.adf.AugmentedDickeyFuller;
import com.numericalmethod.suanshu.stats.timeseries.datastructure.multivariate.realtime.inttime.MultivariateSimpleTimeSeries;

public class CointegrationStratregy implements EWrapper {
	final static Logger logger = Logger.getLogger(CointegrationStratregy.class);
	private int movingAverageSize1;
	private int movingAverageSize2;
	private int maxx = 1;
	// trade maximum = 1 combination of 2 contracts (ex: A and 5.5B).
	// need
	private int paraTime = 15;// trade every 15 minutes
	private int position = 0;
	private int[] positions = new int[2];
	private static DateFormat dateFormat = new SimpleDateFormat(
			"yyyy/MM/dd HH:mm");
	private static String keepTime = dateFormat.format(Calendar.getInstance()
			.getTime());
	private static long keepCurrentTime = 0;
	private static long count = 0;
	private boolean trade = false;
	private int orderId = 1;
	private EClientSocket client;
	private ArrayList<Contract> contracts = new ArrayList<Contract>();

	final int tickerIdForContract1 = 1;
	final int tickerIdForContract2 = 2;
	private Contract contract2 = new Contract(0, "EUR", "CASH", "", 0, "", "",
			"IDEALPRO", "USD", "", "", new java.util.Vector<ComboLeg>(),
			"IDEALPRO", false, "", "");
	private Contract contract1 = new Contract(0, "USD", "CASH", "", 0, "", "",
			"IDEALPRO", "DKK", "", "", new java.util.Vector<ComboLeg>(),
			"IDEALPRO", false, "", "");
	private double[] coVector;
	private ArrayList<Double> c1_30 = new ArrayList<Double>();
	private ArrayList<Double> c1_90 = new ArrayList<Double>();
	private ArrayList<Double> c2_30 = new ArrayList<Double>();
	private ArrayList<Double> c2_90 = new ArrayList<Double>();
	// store price within a minutes. Then choose the price of the last second to
	// closed price
	private ArrayList<Double> price1 = new ArrayList<Double>();
	private ArrayList<Double> price2 = new ArrayList<Double>();

	CointegrationStratregy(int maxx, int paraTime, int movingAverageSize1,
			int movingAverageSize2) {
		contracts.add(contract1);
		contracts.add(contract2);
		Arrays.fill(positions, 0);
		this.movingAverageSize1 = movingAverageSize1;
		this.movingAverageSize2 = movingAverageSize2;
		this.maxx = maxx;
		this.paraTime = paraTime;
		client = new EClientSocket(this);
		client = new EClientSocket(this);
		this.setUp();
		// get current date time with Date()
	}

	public static void main(String[] args) throws Exception {
		CointegrationStratregy co = new CointegrationStratregy(1, 15, 30, 90);
		co.connect();
		co.strategyCheckLoop();
	}

	public void strategyCheckLoop() {
		try {
			Thread.sleep(5000);/*
			//check TWS is connected
			if (client.isConnected())
				logger.info(" TWS is not connected");
			
			//check TickPrice() respond too long
			long time = (long) System.currentTimeMillis() / 1000;
			if (Math.abs(time - keepCurrentTime) > 30)
				logger.info("TickPrice function not update");
			
			// check  strategy went crazy
			if (positions[0] > 100) {
				closeALLposition();
				logger.error(
						"strategy went crazy, buy sell a lot, need to stop",
						new positionsAreTooBigException(positions[1]));

			}
			if (positions[1] > 100) {
				closeALLposition();
				logger.error(
						"strategy went crazy, buy sell a lot, need to stop",
						new positionsAreTooBigException(positions[1]));
			}*/

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setUp() {
		// 1) download, update recent data to calculate coVector
		//download();
		// 2)calculate coVector
		coVector = calculateCovectorForTodayTrading();
		// 3) connect();

	}

	public void download() {
		cleanUP();
		String path2 = "Data" + File.separator + "EURUSD" + File.separator
				+ "BID" + File.separator;
		IBDownloadHistoricalData downloader2 = new IBDownloadHistoricalData(
				path2, contract2);
		String path1 = "Data" + File.separator + "DKKUSD" + File.separator
				+ "BID" + File.separator;
		IBDownloadHistoricalData downloader1 = new IBDownloadHistoricalData(
				path1, contract1);
		downloader2.run();
		downloader1.run();
	}

	private void closeALLposition() {
		// for contract1
		if (positions[0] > 0)
			sell(contract1, positions[0]);
		else if (positions[0] < 0)
			buy(contract1, (-1) * positions[0]);
		// for contract2
		if (positions[1] > 0)
			sell(contract1, positions[1]);
		else if (positions[1] < 0)
			buy(contract1, (-1) * positions[1]);

	}

	public void cleanUP() {
		String path2 = "Data" + File.separator + "EURUSD" + File.separator
				+ "BID";
		String path1 = "Data" + File.separator + "DKKUSD" + File.separator
				+ "BID";
		File f1 = new File(path1);
		for (File f : f1.listFiles())
			f.delete();
		File f2 = new File(path2);
		for (File f : f2.listFiles())
			f.delete();
	}

	public void connect() {
		int clientId = 3;
		// client.eConnect("", 7496, clientId);
		client.eConnect(null, 7496, clientId);
		// client.reqMktData(tickerIdForContract1, contract1, "", true);
		client.reqMktData(tickerIdForContract1, contract1, null, false);
		client.reqMktData(tickerIdForContract2, contract2, null, false);
	}

	public static double[] cointegrated(double level,
			ArrayList<ArrayList<Double>> myDATA) {
		// level = 0.1 =>90%
		// the i-th column represent for the i-th time series
		for (int j = 0; j < myDATA.size(); j++) {
			System.out.println("data " + myDATA.get(j).get(0));
		}
		// 1.125 6.62835
		int rowNumber = myDATA.get(0).size();
		int colNumber = myDATA.size();
		DenseMatrix matrix = new DenseMatrix(rowNumber, colNumber);
		for (int i = 0; i < colNumber; i++) {
			matrix.setColumn(i + 1, new DenseVector(myDATA.get(i)));
		}
		MultivariateSimpleTimeSeries ts = new MultivariateSimpleTimeSeries(
				matrix);
		double[] beta = new double[0];
		try {
			CointegrationMLE coint = new CointegrationMLE(ts, true, 2);
			Vector eigenvalues = coint.getEigenvalues();
			JohansenTest johansenTest = new JohansenTest(
					JohansenAsymptoticDistribution.Test.EIGEN,
					JohansenAsymptoticDistribution.TrendType.CONSTANT,
					eigenvalues.size());

			ImmutableVector stats = johansenTest.getStats(coint);
			int r = johansenTest.r(coint, level);
			System.out.println("r= " + r);
			if (r > 0 && r < 2) {
				beta = coint.beta(1).toArray();
				System.out.println("cointegrating factors");
				System.out.println(coint.beta(1));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return beta;
	}

	public  double[] calculateCovectorForTodayTrading() {
		ArrayList<ArrayList<Double>> data = new ArrayList<ArrayList<Double>>(2);
		data.add(retrieve_last_week_DATA("Data" + File.separator + "DKKUSD"
				+ File.separator + "BID"));

		data.add(retrieve_last_week_DATA("Data" + File.separator + "EURUSD"
				+ File.separator + "BID"));
		double[] coV = cointegrated(0.1, data);
		int n = data.get(0).size();
		double[] residual = new double[n];
		double[] residual2 = new double[n];

		for (int i = 0; i < n; i++) {
			residual[i] = coV[0] * data.get(0).get(i) + coV[1]
					* data.get(1).get(i);
		}
		AugmentedDickeyFuller adf1 = new AugmentedDickeyFuller(residual);
		//System.out.println(adf.getNullHypothesis());
		//System.out.println(adf.getAlternativeHypothesis());
		System.out.println(adf1.statistics());
		System.out.println(adf1.pValue());
		
		for (int i = 0; i < n; i++) {
			residual2[i] = this.round(10*coV[0]) * data.get(0).get(i) +  this.round(10*coV[1])
					* data.get(1).get(i);
		}
		AugmentedDickeyFuller adf2 = new AugmentedDickeyFuller(residual2);
		//System.out.println(adf.getNullHypothesis());
		//System.out.println(adf.getAlternativeHypothesis());
		System.out.println(adf2.statistics());
		System.out.println(adf2.pValue());
		
		return coV;
	}

	public static ArrayList<Double> retrieve_last_week_DATA(String path) {
		File folder = new File(path);
		File[] listFiles = folder.listFiles();
		ArrayList<Double> DATA = new ArrayList<Double>();
		for (int i = listFiles.length - 1; i > -1; i = i - 1) {
			File f = listFiles[i];
			System.out.println(f.getName());
			BufferedReader br = null;
			String line = "";
			try {

				br = new BufferedReader(new FileReader(f));
				while ((line = br.readLine()) != null) {
					String[] r = line.split("\\s+");
					// System.out.println(r[10]);
					double close = Double.parseDouble(r[10].substring(6,
							r[10].length()));
					DATA.add(close);
				}
			}

			catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

		}
		System.out.println("size" + DATA.size());
		return DATA;
	}

	public ArrayList<Double> getC1_30() {
		return c1_30;
	}

	public ArrayList<Double> getC1_90() {
		return c1_90;
	}

	public ArrayList<Double> getC2_30() {
		return c2_30;
	}

	public ArrayList<Double> getC2_90() {
		return c2_90;
	}

	public void error(Exception e) {
		e.printStackTrace();
		System.out.println(e);
	}

	public void error(String str) {
		System.out.println(str);
	}

	public void error(int id, int errorCode, String errorMsg) {
		System.out.println("error: id = " + id + ", code = " + errorCode
				+ ", msg = " + errorMsg);
	}

	public void connectionClosed() {
		System.out
				.println("--------------------- CLOSED ---------------------");
	}

	/* ***************************************************************
	 * EWrapper***************************************************************
	 */

	public void tickPrice(int tickerId, int field, double price,
			int canAutoExecute) {
		try {
			//this.closeALLposition();
			// 1 = bid, 2 = ask, 4 = last
			// 6 = high, 7 = low, 9 = close
			//keepCurrentTime = (long) System.currentTimeMillis() / 1000;
			String currentMinute = dateFormat.format(Calendar.getInstance()
					.getTime());

			if (keepTime.compareTo(currentMinute) == 0) {
				if (field == 1) {
					if (tickerId == tickerIdForContract1)
						price1.add(price);
					else if (tickerId == tickerIdForContract2)
						price2.add(price);
				}
			} else {
				count++;
				trade = true;
				System.out.println(currentMinute);
				keepTime = currentMinute;
				if (price1.size() > 0 && price2.size() > 0) {
					update();
				}

				// System.out.println(price1.size());
				// System.out.println(price2.size());

				price1 = new ArrayList<Double>();
				price2 = new ArrayList<Double>();

			}

			// System.out.println("tickID: " + tickerId + "," + field + "," +
			// price);
			// System.out.println("contract1 : " + c1_30.size() + ","
			// + c1_90.size());
			// System.out.println("contract2 : " + c2_30.size() + ","
			// + c2_90.size());

			if (count % paraTime == 0 && count > 89&trade) {
				trade();
				System.out.println(positions[0]);
				System.out.println(positions[1]);
				//shortOne();
				trade = false;

			}

		}

		catch (Exception e) {
			e.printStackTrace();
			System.out.println("ERROR in TickPrice: " + e);
		}

	}

	// historicalDataForContract1

	public void copyFile(String path1, String path2) {
		InputStream inStream = null;
		OutputStream outStream = null;

		try {

			File afile = new File(path1);
			File bfile = new File(path2);

			inStream = new FileInputStream(afile);
			outStream = new FileOutputStream(bfile);

			byte[] buffer = new byte[1024];

			int length;
			// copy the file content in bytes
			while ((length = inStream.read(buffer)) > 0) {

				outStream.write(buffer, 0, length);

			}

			inStream.close();
			outStream.close();

			// delete the original file
			// afile.delete();

			System.out.println("File is copied successful!");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void update() {
		double lastUpdatedPrice1 = price1.get(price1.size() - 1);
		if (c1_30.size() < movingAverageSize1)
			c1_30.add(lastUpdatedPrice1);
		else {
			c1_30.remove(0);
			c1_30.add(lastUpdatedPrice1);
		}

		if (c1_90.size() < movingAverageSize2)
			c1_90.add(lastUpdatedPrice1);
		else {
			c1_90.remove(0);
			c1_90.add(lastUpdatedPrice1);
		}

		double lastUpdatedPrice2 = price2.get(price2.size() - 1);
		if (c2_30.size() < movingAverageSize1)
			c2_30.add(lastUpdatedPrice2);
		else {
			c2_30.remove(0);
			c2_30.add(lastUpdatedPrice2);
		}

		if (c2_90.size() < movingAverageSize2)
			c2_90.add(lastUpdatedPrice2);
		else {
			c2_90.remove(0);
			c2_90.add(lastUpdatedPrice2);
		}
	}

	public void longOne() {
		for (int j = 0; j < coVector.length; j++) {
			if (coVector[j] > 0) {
				int quantity = this.round(10 * coVector[j]);
				this.buy(contracts.get(j), quantity);
				positions[j] = positions[j] + quantity;
			} else if (coVector[j] < 0) {
				int quantity = this.round((-10) * coVector[j]);
				this.sell(contracts.get(j), quantity);
				positions[j] = positions[j] - quantity;
			}
		}
		position += 1;
	}

	public void longALL() {
		for (int j = 0; j < coVector.length; j++) {
			if (coVector[j] > 0) {
				this.buy(contracts.get(j), positions[j]);
				positions[j] = 0;
			} else if (coVector[j] < 0) {
				this.sell(contracts.get(j), positions[j]);
				positions[j] = 0;
			}
		}
		position = 0;
	}

	public void shortALL() {
		for (int j = 0; j < coVector.length; j++) {
			if (coVector[j] > 0) {
				this.sell(contracts.get(j), positions[j]);
				positions[j] = 0;
			} else if (coVector[j] < 0) {
				this.buy(contracts.get(j), positions[j]);
				positions[j] = 0;
			}
		}
		position = 0;
	}

	public void shortOne() {
		for (int j = 0; j < coVector.length; j++) {
			if (coVector[j] > 0) {
				int quantity = this.round(10 * coVector[j]);
				this.sell(contracts.get(j), quantity);
				positions[j] = positions[j] + quantity;
			} else if (coVector[j] < 0) {
				int quantity = this.round((-10) * coVector[j]);
				this.buy(contracts.get(j), quantity);
				positions[j] = positions[j] - quantity;
			}
		}

		position -= 1;
	}

	public void trade() {
		double MA30 = this.MA30();
		double MA90 = this.MA90();
		System.out.println("MA90= " + MA90);
		System.out.println("MA30= " + MA30);
		if (position > 0) {
			if (MA30 < MA90) {
				if (position < maxx) {
					// if we have some long lots, and it current down, and will
					// up, then we long more until numberOf lots=maxx
					// a lot = a combination of long and short A, B, C: 2A - 3B+
					// 5C
					// 2A, 5C will up,3B will down =>long 2A,5C, short 3B
					// <=>long only one (2A - 3B+ 5C)
					longOne();
				}
			}

			else if (MA30 > MA90) {
				// if we have some long lots, and price current up, and will
				// down=>short ALL lots
				// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B
				// <=>short all
				shortALL();
			}
		}// if position >0

		else if (position == 0) {
			if (MA30 < MA90)
				longOne();// long only one (2A - 3B+ 5C)
			else if (MA30 > MA90)
				shortOne();// short one (2A - 3B+ 5C)
		}

		else {// position < 0
			if (MA30 > MA90) {
				// if we have some short lots, and it now up, will down, then we
				// short one lot more until -maxx
				if (position > -maxx)
					shortOne();// short one more (2A - 3B+ 5C)
			}

			else if (MA30 < MA90)
				longALL(); // long all
		}// else position<0

	}

	public int round(double x) {
		int xx = (int) Math.floor(x);
		if (x < xx + 0.5)
			return xx;
		else
			return xx + 1;
	}

	private void buy(Contract con, int quantity) {

		System.out.println("Now Placing A Order BUY " + quantity);
		Order ord = new Order();
		ord.m_totalQuantity = quantity;
		// tickerId++;
		ord.m_orderId = orderId;
		ord.m_action = "BUY";
		ord.m_orderType = "MKT";
		ord.m_lmtPrice = 0.0;
		client.placeOrder(orderId, con, ord);
		orderId++;
		try {
			Thread.sleep(500);
		} catch (Exception e) {
		}

	}

	private void sell(Contract con, int quantity) {

		System.out.println("Now Placing A Order SELL " + quantity);
		Order ord = new Order();
		ord.m_totalQuantity = quantity;
		// tickerId++;
		ord.m_orderId = orderId;
		ord.m_action = "SELL";
		ord.m_orderType = "MKT";
		ord.m_lmtPrice = 0.0;
		client.placeOrder(orderId, con, ord);
		orderId++;
		try {
			Thread.sleep(500);
		} catch (Exception e) {
		}

	}

	public double MA90() {
		ArrayList<Double> c1_90 = this.getC1_90();
		ArrayList<Double> c2_90 = this.getC2_90();
		ArrayList<ArrayList<Double>> C90 = new ArrayList<ArrayList<Double>>(2);
		C90.add(c1_90);
		C90.add(c2_90);
		double MA90 = MovingAverage(coVector, C90);
		return MA90;//(double) Math.round(MA90 * 100000) / 100000;

	}

	public static double MovingAverage(double[] beta,
			ArrayList<ArrayList<Double>> M) {
		int windowSize = M.get(0).size();
		ArrayList<Double> residual = new ArrayList<Double>(windowSize);
		for (int i = 0; i < windowSize; i++) {
			double i_th = 0;
			for (int j = 0; j < beta.length; j++) {
				i_th = i_th + beta[j] * M.get(j).get(i);
			}
			residual.add(i_th);
		}

		return average(residual);
	}

	public static double average(List<Double> list) {
		if (list == null || list.isEmpty())
			return 0.0;
		double sum = 0;
		int n = list.size();
		for (int i = 0; i < n; i++)
			sum += list.get(i);
		return sum / n;
	}

	public double MA30() {
		ArrayList<Double> c1_30 = this.getC1_30();
		ArrayList<Double> c2_30 = this.getC2_30();
		ArrayList<ArrayList<Double>> C30 = new ArrayList<ArrayList<Double>>(2);
		C30.add(c1_30);
		C30.add(c2_30);
		double MA30 = MovingAverage(coVector, C30);
		return MA30;//(double) Math.round(MA30 * 100000) / 100000;
	}

	public void tickSize(int tickerId, int field, int size) {
		// System.out.println("Size Fundtion - tickSize: tickerId = "+tickerId+", field = "+field+", size = "+size);
	}

	public void tickGeneric(int tickerId, int tickType, double value) {
		String toBeReturned = "Ticker ID = " + tickerId + "\n";
		toBeReturned += "Tick Type = " + tickType + "\n";
		toBeReturned += "Value = " + value + "\n";
		System.out.println(toBeReturned);
	}

	public void tickString(int tickerId, int tickType, String value) {
		String toBeReturned = "Ticker ID = " + tickerId + "\n";
		toBeReturned += "Tick Type = " + tickType + "\n";
		toBeReturned += "Value = " + value + "\n";
		System.out.println(toBeReturned);
	}

	public void tickSnapshotEnd(int tickerId) {
		String toBeReturned = "Ticker ID = " + tickerId + "\n";
		System.out.println(toBeReturned);
	}

	public void tickOptionComputation(int tickerId, int field,
			double impliedVol, double delta, double modelPrice,
			double pvDividend) {
		System.out.println("tickOptionComputation");
	}

	public void tickEFP(int tickerId, int tickType, double basisPoints,
			String formattedBasisPoints, double impliedFuture, int holdDays,
			String futureExpiry, double dividendImpact, double dividendsToExpiry) {
		String toBeReturned = "Ticker ID = " + tickerId + "\n";
		toBeReturned += "Tick Type = " + tickType + "\n";
		toBeReturned += "Basis Points = " + basisPoints + "\n";
		toBeReturned += "Formatted Basis Points = " + formattedBasisPoints
				+ "\n";
		toBeReturned += "Implied Future = " + impliedFuture + "\n";
		toBeReturned += "Hold Days = " + holdDays + "\n";
		toBeReturned += "Future Expiry = " + futureExpiry + "\n";
		toBeReturned += "Dividend Impact = " + dividendImpact + "\n";
		toBeReturned += "Dividends to Expiry = " + dividendsToExpiry + "\n";
		System.out.println(toBeReturned);

	}

	public void orderStatus(int orderId, String status, int filled,
			int remaining, double avgFillPrice, int permId, int parentId,
			double lastFillPrice, int clientId, String whyHeld) {
		String toBeReturned = "Client ID = " + clientId + "\n";
		toBeReturned += "Order ID = " + orderId + "\n";
		toBeReturned += "Status := " + status + "\n";
		toBeReturned += "Filled = " + filled + "\n";
		toBeReturned += "Remaining = " + remaining + "\n";
		toBeReturned += "Avg. Fill Price = " + avgFillPrice + "\n";
		toBeReturned += "Perm ID = " + permId + "\n";
		toBeReturned += "Parent ID = " + parentId + "\n";
		toBeReturned += "Last Fill Price = " + lastFillPrice + "\n";
		toBeReturned += "HELD ?? = " + whyHeld + "\n";
		System.out.println(toBeReturned);
	}

	public void openOrder(int orderId, Contract contract, Order order,
			OrderState orderState) {
		System.out.println("openOrder");
	}

	public void openOrderEnd() {
		System.out.println("openOrderEnd");
	}

	public void updateAccountValue(String key, String value, String currency,
			String accountName) {
		System.out.println("updateAccountValue");
	}

	public void updatePortfolio(Contract contract, int position,
			double marketPrice, double marketValue, double averageCost,
			double unrealizedPNL, double realizedPNL, String accountName) {
		System.out.println("updatePortfolio");
	}

	public void updateAccountTime(String timeStamp) {
		System.out.println("updateAccountTime: " + timeStamp);
	}

	public void accountDownloadEnd(String accountName) {
		System.out.println("accountDownloadEnd");
	}

	public void nextValidId(int orderId) {
		System.out.println("nextValidId: " + orderId);

	}

	public void contractDetails(int reqId, ContractDetails contractDetails) {
		System.out.println("contractDetails");
	}

	public void contractDetailsEnd(int reqId) {
		System.out.println("contractDetailsEnd");
	}

	public void bondContractDetails(int reqId, ContractDetails contractDetails) {
		System.out.println("bondContractDetails");
	}

	public void execDetails(int reqId, Contract contract, Execution execution) {
		System.out.println("execDetails");
	}

	public void execDetailsEnd(int reqId) {
		System.out.println("execDetailsEnd");
	}

	public void updateMktDepth(int tickerId, int position, int operation,
			int side, double price, int size) {
		System.out.println("updateMktDepth");
	}

	public void updateMktDepthL2(int tickerId, int position,
			String marketMaker, int operation, int side, double price, int size) {
		System.out.println("updateMktDepthL2");
	}

	public void updateNewsBulletin(int msgId, int msgType, String message,
			String origExchange) {
		System.out.println("updateNewsBulletin");
	}

	public void managedAccounts(String accountsList) {
		System.out.println("managedAccounts");
	}

	public void receiveFA(int faDataType, String xml) {
		System.out.println("receiveFA");
	}

	public void historicalData(int reqId, String date, double open,
			double high, double low, double close, int volume, int count,
			double WAP, boolean hasGaps) {
		// logIn("historicalData, "+reqId+" , "+date+" , "+open+" , "+close+" , hasgaps: , "+hasGaps);
	}

	public void scannerParameters(String xml) {
		System.out.println("scannerParameters");
	}

	public void scannerData(int reqId, int rank,
			ContractDetails contractDetails, String distance, String benchmark,
			String projection, String legsStr) {
		System.out.println("scannerData");
	}

	public void scannerDataEnd(int reqId) {
		System.out.println("scannerDataEnd");
	}

	public void realtimeBar(int reqId, long time, double open, double high,
			double low, double close, long volume, double wap, int count) {
		System.out.println("realtimeBar");
	}

	public void currentTime(long millis) {
		System.out.println("currentTime");
	}

	public void fundamentalData(int reqId, String data) {
		System.out.println("fundamentalData");
	}

	public void deltaNeutralValidation(int reqId, UnderComp underComp) {
		System.out.println("deltaNeutralValidation");
	}

	@Override
	public void tickOptionComputation(int tickerId, int field,
			double impliedVol, double delta, double optPrice,
			double pvDividend, double gamma, double vega, double theta,
			double undPrice) {
		// TODO Auto-generated method stub

	}

	@Override
	public void marketDataType(int reqId, int marketDataType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void commissionReport(CommissionReport commissionReport) {
		// TODO Auto-generated method stub

	}

	@Override
	public void position(String account, Contract contract, int pos,
			double avgCost) {
		// TODO Auto-generated method stub

	}

	@Override
	public void positionEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountSummary(int reqId, String account, String tag,
			String value, String currency) {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountSummaryEnd(int reqId) {
		// TODO Auto-generated method stub

	}

}
