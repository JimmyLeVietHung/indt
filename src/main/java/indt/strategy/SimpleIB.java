package indt.strategy;

import java.util.*;

import indt.trader.*;
import indt.utils.*;
import indt.utils.MarketIndex.MarketMaker;
import indt.event.*;
import indt.event.EventListener;
import indt.datasource.*;
import indt.market.*;
import indt.order.*;
import indt.order.Order.*;

public class SimpleIB implements EventListener, BookListener, OrderListener, RiskListener {
	private SymbolIndex si = new SymbolIndex();
	private AggrFileDataSource ads = new AggrFileDataSource();
	private EventLoop eloop = new EventLoop(si, ads);
	private AggrBook mb = new AggrBook(eloop, si);
	private SHFESimTrader st = new SHFESimTrader(eloop, si, ads);
	private OrderManager om = new OrderManager(eloop, si);
	private RiskManager rm = new RiskManager(om, si);
	
	public static void main(String[] args) {
		SimpleIB scr = new SimpleIB();

		// Populate symbol list
		ArrayList<Symbol> sl = new ArrayList<Symbol>();
		sl.add(new Symbol("DoesNotMatter"));
		
		// Setup data
		for (Symbol sym : sl) {
			// Add symbol
			scr.addSymbol(sym);
			
			// Add datasources
			IBLiveDataSource d = new IBLiveDataSource(sym);
			
			scr.addSource(d);
		}

		// Trade
		scr.start();
		System.out.println(" @@ DONE @@");
	}

	public void addSymbol(Symbol s) {
		si.addSymbol(s);
	}
	
	private void addSource(DataSource d) {
		ads.addSource((FileDataSource) d);
	}

	public void start() {
		// Add listeners
		eloop.addListener(this);
		mb.addListener(this);
		om.addListener(this);
		rm.addListener(this);

		// Add traders
		om.addTrader(st);
		
		// Setup risk limits
		rm.addPositionLimitSymbol(0, 30);
		rm.addNotionalLimitSymbol(0, 150000000);
		rm.addPositionLimitSymbolMm(0, MarketMaker.IB, 20);
		rm.addNotionalLimitSymbolMm(0, MarketMaker.IB, 110000000);

		// Start
		eloop.start();
	}
		
	@Override
	public void onEvent(Event e) {
		System.out.println(" ** Event: " + e.getEventType() + " .. " + e.counter + ", " + e.timestamp);
		
	}
	
	@Override
	public void onBookChange(BookOrder bo, BookEvent.ActionType at, boolean done) {
		System.out.println(" ** BookChange : " + bo.toString() + " .. ActionType: " + at.toString() + ".. done: " + done);
		
		/*
		if (done) {
			// Send crossing trade every 10 updates, random side
			if (ocnt % 20 == 0) {
				mb.prettyPrint(bo.symbolIndex);
				
				OrderSide[] osval = OrderSide.values();
				OrderSide s = osval[rnd.nextInt(osval.length-1)];
				int bs = 0;
				if (s == OrderSide.BUY)
					bs = 1;
				BookOrder bbo = mb.getBook(bo.market).getBookHalf(bo.symbolIndex, bs).getTopOrder();
				int px = bbo.px;
				int sz = Math.min(10, bbo.size);
				SHFEOrder o = new SHFEOrder(bo.symbolIndex, si.getSymbol(bo.symbolIndex).displayName, OrderType.MARKET, s, 1000, px, sz, om.getNewClientID());
				if (rm.validTrade(o)) {
					System.out.println(" ** OMTrade: " + bo.symbolIndex + " -> " + s + ", " + px + ", " + sz);
					om.trade(o);
				} else {
					System.out.println(" ** OMTrade: " + bo.symbolIndex + " -> " + s + ", " + px + ", " + sz + " [limits exceeded, not traded!]");
				}
			}
			ocnt++;

			// REMOVE AFTER TESTING!
			//if (ocnt > 500)
			//	eloop.stop();
			
			
		}
		*/
	}

	@Override
	public void onSequence(Order o) {
		System.out.println(" ** onSeq: " + o.symbolIndex + "|" + o.clientID + " [" + o.seqTime.getFirst() + "]");
	}

	@Override
	public void onConfirm(Order o, Boolean pxchg) {
		if (pxchg)
			System.out.println(" ** onCnf: " + o.symbolIndex + "|" + o.clientID + " .. " + o.currentPx.get(1) + " -> " + o.currentPx.getFirst() + " [" + o.cnfTime.getFirst() + "]");
		else
			System.out.println(" ** onCnf: " + o.symbolIndex + "|" + o.clientID + " .. " + o.currentPx.getFirst() + " [" + o.cnfTime.getFirst() + "]");
		//rm.prettyPrint();
	}

	@Override
	public void onFill(Order o) {
		System.out.println(" ** onFill: " + o.symbolIndex + "|" + o.clientID + " .. " + o.fillPx.getFirst() + ", " + o.fillSize.getFirst() + " [" + o.fillTime.getFirst() + "]");
		rm.prettyPrint();
	}

	@Override
	public void onCancel(Order o) {
		System.out.println(" ** onCxl: " + o.symbolIndex + "|" + o.clientID + " .. " + o.cxlSize.getFirst() + " [" + o.cxlTime.getFirst() + "]");
		rm.prettyPrint();
	}

	@Override
	public void onCancelReplace(Order o, Boolean oidchg) {
		System.out.println(" ** onCxlRpl: ERROR .. should not trigger in SimpleCrosser!");
		rm.prettyPrint();
	}

	@Override
	public void onPositionMismatch(Order o, Boolean pxchg) {
		System.out.println(" ** onPosMis: ERROR .. should not trigger in SimpleCrosser!");
	}

	@Override
	public void onPositionUpdate() {
		System.out.println(" ** onPosUpd: ERROR .. should not trigger in SimpleCrosser!");
	}	
}
