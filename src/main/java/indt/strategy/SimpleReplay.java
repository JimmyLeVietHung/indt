package indt.strategy;

import java.util.ArrayList;
import indt.utils.*;
import indt.event.*;
import indt.datasource.*;
import indt.market.*;

public class SimpleReplay implements EventListener, BookListener {
	private SymbolIndex si = new SymbolIndex();
	private AggrFileDataSource ads = new AggrFileDataSource();
	private EventLoop eloop = new EventLoop(si, ads);
	private AggrBook mb = new AggrBook(eloop, si);
	
	public static void main(String[] args) {
		SimpleReplay srp = new SimpleReplay();

		// Populate symbol list
		ArrayList<Symbol> sl = new ArrayList<Symbol>();
		sl.add(new Symbol("AUF5"));
		sl.add(new Symbol("AUG5"));
		
		// Setup replay
		for (Symbol sym : sl) {
			// Add symbol
			srp.addSymbol(sym);
			
			// Add datasources
			SHFEFileDataSource d = new SHFEFileDataSource(20140915, sym);
			srp.addSource(d);
		}

		// Replay
		srp.start();
		System.out.println("@@ DONE @@");
	}

	public void addSymbol(Symbol s) {
		si.addSymbol(s);
	}
	
	private void addSource(DataSource d) {
		ads.addSource((FileDataSource) d);
	}

	public void start() {
		// Add listeners
		eloop.addListener(this);
		mb.addListener(this);
		
		// Start
		eloop.start();
	}
	
	@Override
	public void onEvent(Event e) {
		//System.out.println("## SRP: received new event ##");
		//System.out.println("   symbol = " + String.valueOf(e.symbol) + ", market = " + String.valueOf(e.getMM()));
	}
	
	@Override
	public void onBookChange(BookOrder bo, BookEvent.ActionType at, boolean done) {
		//System.out.println("** SRP: received new book update **");
		//System.out.println("   action type = " + at.ordinal() + ", done = " + done);
		//System.out.println("   symbol index = " + bo.symbolIndex + ", market index = " + bo.marketIndex);
		//System.out.println("   px = " + bo.px + ", size = " + bo.size + ", side = " + bo.side);
		if (done)
			mb.prettyPrint(bo.symbolIndex);
	}
}
