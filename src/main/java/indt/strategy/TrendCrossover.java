package indt.strategy;

import indt.utils.*;
import indt.event.*;
import indt.datasource.*;
import indt.market.*;

public class TrendCrossover implements EventListener, BookListener {
	private SymbolIndex si = new SymbolIndex();
	private AggrFileDataSource ads = new AggrFileDataSource();
	private EventLoop eloop = new EventLoop(si, ads);
	private AggrBook mb = new AggrBook(eloop, si);

	
	private PricingManager pricingManager = new PricingManager();
	private PositionManager positionManager = new PositionManager(pricingManager);
	private final int tradingInterval = 15*60*1000; //every 15 minutes
	private final int lookbackShort = 15; //15 minutes
	private final int lookbackLong = 60; //60 minutes
	
	private int symbolIndexInt = -1;
	private long latestTimeStamp = 0;
	private int latestHour = 0;
	private long cumPnl = 0;
	private long lastTradedTimeStamp = 0;
	
	public static void main(String[] args) {
		
		long totalPnl = 0;
		
		Symbol sym = new Symbol("ZCSR0001");
		
		int[] months = new int[12];
		months = new int[] {201309, 201310, 201311, 201312, 201401, 201402, 201403, 201404, 201405, 201406, 201407, 201408};
		
		for (int month: months) {
			TrendCrossover tp = new TrendCrossover();
			tp.addSymbol(sym);	
			CNINFileDataSource d = new CNINFileDataSource(month, sym, 1000);
			tp.addSource(d);
			try {
				tp.start();
			} catch (Exception e) {
				System.err.println("Exception: " + e.getMessage());
			}
			totalPnl += tp.cumPnl;
			System.out.println(month + " totalPnl: " + totalPnl);
		}
		
		System.out.println("totalPnl: " + totalPnl);
		
		//System.out.println("@@ DONE @@");
		
	}

	public void addSymbol(Symbol s) {
		si.addSymbol(s);
		symbolIndexInt = si.getIndex(s);
	}
	
	private void addSource(DataSource d) {
		ads.addSource((FileDataSource) d);
	}

	public void start() {
		// Add listeners
		eloop.addListener(this);
		eloop.addListener(pricingManager);
		mb.addListener(this);
		mb.addListener(pricingManager);
		
		// Start
		eloop.start();
	}
	
	@Override
	public void onEvent(Event e) {
		latestTimeStamp = e.timestamp;
	}
	
	@Override
	public void onBookChange(BookOrder bo, BookEvent.ActionType at, boolean done) {
		if (bo.side != 0) { 
			processStrategy();
		}
	}
	
	private void processStrategy()
	{
		//int second = (int) ((latestTimeStamp / 1000) % 60);
		int minute = (int) ((latestTimeStamp / (1000 * 60)) % 60);
		int hour = (int) ((latestTimeStamp / (1000 * 60 * 60)) % 24);
		hour += 8; //convert from GMT to local
		int hourMinutes = (hour * 100) + minute;
		//int day = (int) (latestTimeStamp / (1000 * 60 * 60 * 24));
		
		if ((hourMinutes >= 1015) && latestTimeStamp > (lastTradedTimeStamp + tradingInterval)) { //time to trade
			int pos = positionManager.GetPosition(symbolIndexInt);
			
			int movingAvgShort = pricingManager.getRecentMovingAvg(symbolIndexInt, lookbackShort);
			int movingAvgLong = pricingManager.getRecentMovingAvg(symbolIndexInt, lookbackLong);
			
			if (movingAvgShort > movingAvgLong) {
				if (pos < 0) {
					//positionManager.TradeAggress(symbolIndexInt, 2);
					positionManager.TradeMid(symbolIndexInt, 2);
				} else if (pos == 0) {
					//positionManager.TradeAggress(symbolIndexInt, 1);
					positionManager.TradeMid(symbolIndexInt, 1);
				}
			} else {
				if (pos > 0) {
					//positionManager.TradeAggress(symbolIndexInt, -2);
					positionManager.TradeMid(symbolIndexInt, -2);
				} else if (pos == 0) {
					//positionManager.TradeAggress(symbolIndexInt, -1);
					positionManager.TradeMid(symbolIndexInt, -1);
				}
			}
			lastTradedTimeStamp = latestTimeStamp;
		}
		
		if (hour < latestHour) { //Changed to next day. Close positions by end of day
			long pnl = positionManager.GetTotalPnL(symbolIndexInt);
			cumPnl += pnl;
			System.out.println("Daily pnl: " + pnl);
			//System.out.println("cumPnl: " + cumPnl);
			//System.out.println();
			
			positionManager = new PositionManager(pricingManager);
			pricingManager.reset();
			
		}
		latestHour = hour;
	}
}
