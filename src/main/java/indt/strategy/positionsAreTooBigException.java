package indt.strategy;

public class positionsAreTooBigException extends Exception {
	 
    private long position;
 
    public positionsAreTooBigException(long position){
        this.position = position;
    }
 
    public String toString(){
        return "strategy went crazy, buy sell a lot, need to stop" +position;
    }
 
}