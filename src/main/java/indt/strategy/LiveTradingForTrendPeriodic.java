package indt.strategy;

import indt.utils.*;
import indt.event.*;
import indt.datasource.*;
import indt.market.*;

public class LiveTradingForTrendPeriodic implements EventListener, BookListener {
	private SymbolIndex si = new SymbolIndex();
	private AggrLiveDataSource ads = new AggrLiveDataSource();
	private EventLoop eloop = new EventLoop(si, ads);
	private AggrBook mb = new AggrBook(eloop, si);

	
	private PricingManager pricingManager = new PricingManager();
	private PositionManager positionManager = new PositionManager(pricingManager);
	private final int tradingInterval = 15*60*1000; //every 15 minutes
	private final int lookbackInterval = 60*60*1000; //60 minutes
	
	private int symbolIndexInt = -1;
	private long latestTimeStamp = 0;
	private int latestHour = 0;
	private long cumPnl = 0;
	private long lastTradedTimeStamp = 0;
	
	//Test DiskTrading data
	public static void main(String[] args) {
		
		Symbol sym = new Symbol("ZBZ3");
		LiveTradingForTrendPeriodic tp = new LiveTradingForTrendPeriodic();
		tp.addSymbol(sym);	
		//DiskTradingFileDataSource d = new DiskTradingFileDataSource(0, sym, 1000);
		IBLiveDataSourceForTrendPeriodic d = new IBLiveDataSourceForTrendPeriodic(0, sym, 1000);

		tp.addSource(d);
		try {
			tp.start();
		} catch (Exception e) {
			//System.err.println("Exception: " + e.getMessage());
			e.printStackTrace();
		}
		System.out.println(" totalPnl: " + tp.cumPnl);
		
	}
	
	public void addSymbol(Symbol s) {
		si.addSymbol(s);
		symbolIndexInt = si.getIndex(s);
	}
	
	
	private void addSource(DataSource d) {
		ads.addSource((IBLiveDataSourceForTrendPeriodic)d);
	}

	public void start() {
		// Add event listeners
		eloop.addListener(this);
		eloop.addListener(pricingManager);
		mb.addListener(this);
		mb.addListener(pricingManager);
		
		// Start
		eloop.start();
	}
	
	@Override
	public void onEvent(Event e) {
		latestTimeStamp = e.timestamp;
		
	}
	
	@Override
	public void onBookChange(BookOrder bo, BookEvent.ActionType at, boolean done) {
		if (bo.side != 0) { 
			processStrategy();
		}
	}
	
	private void processStrategy()
	{
	
		//System.out.println("count :"+ count);
		//int second = (int) ((latestTimeStamp / 1000) % 60);
		int minute = (int) ((latestTimeStamp / (1000 * 60)) % 60);
		int hour = (int) ((latestTimeStamp / (1000 * 60 * 60)) % 24);
		hour += 8; //convert from GMT to local
		int hourMinutes = (hour * 100) + minute;
		//int day = (int) (latestTimeStamp / (1000 * 60 * 60 * 24));
		
		if ((hourMinutes >= 1015) && latestTimeStamp > (lastTradedTimeStamp + tradingInterval)) { //time to trade
			int pos = positionManager.GetPosition(symbolIndexInt);
			
			int latestMid = pricingManager.getLatestMid(symbolIndexInt);
			int prevMid = pricingManager.getMidNearTime(symbolIndexInt, latestTimeStamp - lookbackInterval);
			if (latestMid > prevMid) {
				if (pos < 0) {
					//positionManager.TradeAggress(symbolIndexInt, 2);
					positionManager.TradeMid(symbolIndexInt, 2);
				} else if (pos == 0) {
					//positionManager.TradeAggress(symbolIndexInt, 1);
					positionManager.TradeMid(symbolIndexInt, 1);
				}
			} else if (latestMid < prevMid) {
				if (pos > 0) {
					//positionManager.TradeAggress(symbolIndexInt, -2);
					positionManager.TradeMid(symbolIndexInt, -2);
				} else if (pos == 0) {
					//positionManager.TradeAggress(symbolIndexInt, -1);
					positionManager.TradeMid(symbolIndexInt, -1);
				}
			}
			lastTradedTimeStamp = latestTimeStamp;
		}
		
		if (hour < latestHour) { //Changed to next day. Close positions by end of day
			long pnl = positionManager.GetTotalPnL(symbolIndexInt);
			cumPnl += pnl;
			positionManager = new PositionManager(pricingManager);
			pricingManager.reset();
			
		}
		latestHour = hour;
	}
}
