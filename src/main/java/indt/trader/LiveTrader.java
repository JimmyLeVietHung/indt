package indt.trader;

import indt.order.Order;
import indt.utils.MarketIndex.*;

public class LiveTrader implements Trader {
	public LiveTrader() {
		super();
		
		// TODO: connect to live trade server
		
	}

	@Override
	public MarketMaker getMM() {
		return MarketMaker.NA;
	}

	@Override
	public Boolean trade(Order o) {
		
		// TODO: submit order to trade server

		return false;
	}

	@Override
	public Boolean cancel(Order o) {
		
		// TODO: submit cancel to trade server

		return false;
	}

	@Override
	public Boolean cancelreplace(Order o, int newpx, int newsz) {
		
		// TODO: submit cxr to trade server

		return false;
	}

}
