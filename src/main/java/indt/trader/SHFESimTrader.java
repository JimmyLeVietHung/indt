package indt.trader;

import indt.datasource.AggrFileDataSource;
import indt.event.*;
import indt.trader.SimTrader.LagType;
import indt.utils.MarketIndex.*;
import indt.utils.*;

public class SHFESimTrader extends SimTrader {
	public SHFESimTrader(EventLoop eloop, SymbolIndex si, AggrFileDataSource ads) {
		super(eloop, si, ads);
	}

	@Override
	public MarketMaker getMM() {
		return MarketMaker.SHFE;
	}
	
	@Override
	public int getLag(LagType lt) {
		switch (lt) {
		case CLIENT2TS: return 150;
		case TS2EXCH:   return 100000;
		case EXCH2TS:   return 100000;
		case TS2CLIENT: return 150;
		case EXCHPROC:  return 30; 
		default: return 9999999;
		}
	}
}
