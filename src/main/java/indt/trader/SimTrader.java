package indt.trader;

import java.util.Random;

import indt.datasource.*;
import indt.event.*;
import indt.event.TradeEvent.*;
import indt.event.Event.*;
import indt.order.*;
import indt.order.Order.*;
import indt.utils.*;
import indt.utils.MarketIndex.*;

public class SimTrader implements Trader, EventListener {
	private EventLoop eloop;
	private SymbolIndex si;
	private AggrFileDataSource ads;
	private SimDataSource sds;
	protected int c2ts = 9999999;
	protected int t2ex = 9999999;
	protected int ex2t = 9999999;
	protected int ts2c = 9999999;
	protected int exch = 9999999;
	protected int c2tsrt = 9999999;
	protected int c2exrt = 9999999;
	protected int c2ex = 9999999;
	protected int ex2c = 9999999;
	private Random rnd = new Random();

	// TODO: add venue-specific shadow book for filling limit orders

	public SimTrader(EventLoop e, SymbolIndex _si, AggrFileDataSource _ads) {
		si = _si;
		ads = _ads;
		eloop = e;
		eloop.addListener(this);
		
		// Setup SimDataSource
		sds = new SimDataSource(si);
		ads.addSource(sds);
		
		// Populate lags
		c2ts = getLag(LagType.CLIENT2TS);
		t2ex = getLag(LagType.TS2EXCH);
		ex2t = getLag(LagType.EXCH2TS);
		ts2c = getLag(LagType.TS2CLIENT);
		exch = getLag(LagType.EXCHPROC);
		c2tsrt = c2ts + ts2c;
		c2exrt = c2ts + t2ex + ex2t + ts2c;
		c2ex = c2ts + t2ex;
		ex2c = ex2t + ts2c;
	}

	@Override
	public MarketMaker getMM() {
		return MarketMaker.NA;
	}

	public enum LagType {
		CLIENT2TS,  // time for order to reach trade server [in us]
		TS2EXCH,    // time for order to reach exchange (from trade server) [in us]
		EXCH2TS,    // time for reply to reach trade server (from exchange) [in us]
		TS2CLIENT,  // time for reply to reach client (from trade server) [in us]
		EXCHPROC,   // time for exchange to process order [in us]
	}
	public int getLag(LagType lt) {
		switch (lt) {
		case CLIENT2TS: return 100;
		case TS2EXCH:   return 50000;
		case EXCH2TS:   return 50000;
		case TS2CLIENT: return 100;
		case EXCHPROC: return 10;
		default: return 9999999;
		}
	}

	@Override
	public Boolean trade(Order o) {
		// Insert sequence & confirm order events
		sds.insertSequenceOrderEvent(o, eloop.timestamp + c2tsrt);
		sds.insertConfirmOrderEvent(o, eloop.timestamp + c2exrt);
		
		// Insert trade event (delay evaluation of fill)
		sds.insertOrderTradeEvent(o, eloop.timestamp + c2ex);
		
		return true;
	}

	@Override
	public Boolean cancel(Order o) {
		
		// TODO: add for limit orders
		//insertCancelTradeEvent

		return null;
	}

	@Override
	public Boolean cancelreplace(Order o, int newpx, int newsz) {

		// TODO: add for limit orders
		//insertCxrTradeEvent

		return null;
	}

	@Override
	public void onEvent(Event e) {
		// Process relevant events
		if (e.getEventType() == EventType.TRADE)
			processTradeEvent((TradeEvent)e);
		else if (e.getEventType() == EventType.BOOK)
			processBookEvent((BookEvent)e);
	}
	
	protected void processTradeEvent(TradeEvent te) {
		if (te.getActionType() == ActionType.ORDER) {
			OrderTradeEvent oe = (OrderTradeEvent)te;
			if (oe.ot == OrderType.MARKET) {
				// TMP: currently just randomly fills or cancels, REMOVE once shadow book is implemented and cross against it
				int act = rnd.nextInt(2);
				if (act == 0) {
					// Fill
					sds.insertFillOrderEvent(oe.symbol, oe.side, oe.size, 0, oe.px, oe.pxDivisor, oe.exchangeID, oe.mm, eloop.timestamp + exch + ex2c);
				} else {
					// Cancel
					sds.insertCancelOrderEvent(oe.symbol, oe.size, 0, oe.exchangeID, oe.mm, eloop.timestamp + exch + ex2c);
				}
								
				// TODO: check if sim's mktbook still contains a valid px level, fill immediately or cancel
				
			} else if (oe.ot == OrderType.LIMIT) {
				
				// TODO: try to fill order, otherwise add to sim's order book
				
			}
		} else if (te.getActionType() == ActionType.CANCEL) {
			
			// TODO: attempt to cancel existing limit order
			
		} else if (te.getActionType() == ActionType.CXR) {
			
			// TODO: attempt to cxr existing limit order			
			
		}
	}
	
	protected void processBookEvent(BookEvent be) {
	
		// TODO: attempt to fill existing limit order on sim's order book
		
	}
}
