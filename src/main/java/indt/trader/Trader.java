package indt.trader;

import indt.order.*;
import indt.utils.MarketIndex.*;

public interface Trader {
	public MarketMaker getMM();
	public Boolean trade(Order o);
	public Boolean cancel(Order o);
	public Boolean cancelreplace(Order o, int newpx, int newsz);
}
