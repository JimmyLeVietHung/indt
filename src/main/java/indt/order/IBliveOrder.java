package indt.order;

import indt.utils.MarketIndex.*;

public class IBliveOrder extends Order {
	public IBliveOrder(int si, String sym, OrderType ot, OrderSide s, int pxD, int px, int sz, long cid) {
		super(si, sym, ot, s, pxD, px, sz, cid);
		mm = MarketMaker.NA;
	}
}
