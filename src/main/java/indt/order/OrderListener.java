package indt.order;

public interface OrderListener {
	public void onSequence(Order o);
	public void onConfirm(Order o, Boolean pxchg);
	public void onFill(Order o);
	public void onCancel(Order o);
	public void onCancelReplace(Order o, Boolean oidchg);
}
