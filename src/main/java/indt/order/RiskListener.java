package indt.order;

public interface RiskListener {
	public void onPositionMismatch(Order o, Boolean pxchg);
	public void onPositionUpdate();
}
