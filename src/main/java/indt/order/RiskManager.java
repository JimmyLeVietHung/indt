package indt.order;

import java.util.*;

import indt.fee.*;
import indt.order.Order.OrderStatus;
import indt.utils.*;
import indt.utils.MarketIndex.*;

public class RiskManager implements OrderListener {
	protected LinkedList<RiskListener> rl = new LinkedList<RiskListener>();
	protected SymbolIndex si;
	protected class RiskLimit {
		public int posLimit;
		public long notionalLimit;
		public HashMap<Integer, Integer> posMmLimit = new HashMap<Integer, Integer>();
		public HashMap<Integer, Long> notionalMmLimit = new HashMap<Integer, Long>();
		public RiskLimit() {
			posLimit = -1;
			notionalLimit = -1;
			posMmLimit.clear();
			notionalMmLimit.clear();
		}
	}
	protected HashMap<Integer, RiskLimit> symbolLimit = new HashMap<Integer, RiskLimit>();
	protected class MarkToMarket {
		public long totalFees;
		public long closedPnl;     // closed positions
		public int closedPos;
		public int closedCount;
		public long openMargin;  // open orders
		public int openPos;
		public int openCount;
		public HashMap<Integer, Long> totalFeesMm = new HashMap<Integer, Long>();
		public HashMap<Integer, Long> closedPnlMm = new HashMap<Integer, Long>();
		public HashMap<Integer, Integer> closedPosMm = new HashMap<Integer, Integer>();
		public HashMap<Integer, Integer> closedCountMm = new HashMap<Integer, Integer>();
		public HashMap<Integer, Long> openMarginMm = new HashMap<Integer, Long>();
		public HashMap<Integer, Integer> openPosMm = new HashMap<Integer, Integer>();
		public HashMap<Integer, Integer> openCountMm = new HashMap<Integer, Integer>();
		public MarkToMarket() {
			closedPnl = 0;
			closedPos = 0;
			closedCount = 0;
			openMargin = 0;
			openPos = 0;
			openCount = 0;
		}
	}
	protected HashMap<Integer, MarkToMarket> m2m = new HashMap<Integer, MarkToMarket>();
	protected AggrComms comms = new AggrComms(si);
	
	public RiskManager(OrderManager om, SymbolIndex s) {
		si = s;
		om.addListener(this);
	}

	public void addListener(RiskListener _rl) {
		rl.offer(_rl);
	}

	private RiskLimit getSymbolRisk(int symbolIndex) {
		RiskLimit symlim = symbolLimit.get(symbolIndex);
		if (symlim == null) {
			symlim = new RiskLimit();
			symbolLimit.put(symbolIndex, symlim);
		}
		return symlim;
	}
	
	public void addPositionLimitSymbol(int symbolIndex, int lim) {
		RiskLimit symlim = getSymbolRisk(symbolIndex);
		symlim.posLimit = lim;
	}
	
	public void addNotionalLimitSymbol(int symbolIndex, long lim) {
		RiskLimit symlim = getSymbolRisk(symbolIndex);
		symlim.notionalLimit = lim;
	}

	public void addPositionLimitSymbolMm(int symbolIndex, MarketMaker mm, int lim) {
		RiskLimit symlim = getSymbolRisk(symbolIndex);
		symlim.posMmLimit.put(mm.ordinal(), lim);
	}
	
	public void addNotionalLimitSymbolMm(int symbolIndex, MarketMaker mm, long lim) {
		RiskLimit symlim = getSymbolRisk(symbolIndex);
		symlim.notionalMmLimit.put(mm.ordinal(), lim);
	}

	private MarkToMarket getSymbolMark(int symbolIndex) {
		MarkToMarket mrkmkt = m2m.get(symbolIndex);
		if (mrkmkt == null) {
			mrkmkt = new MarkToMarket();
			m2m.put(symbolIndex, mrkmkt);
		}
		return mrkmkt;
	}
	
	private <T> T getValue(HashMap<Integer, T> map, Integer key, T defval) {
		T val = map.get(key);
		if (val == null)
			return defval;
		return val;
	}

	public boolean validTrade(Order o) {
		RiskLimit symlim = getSymbolRisk(o.symbolIndex);
		MarkToMarket mrkmkt = getSymbolMark(o.symbolIndex);
		int s = 1 - 2*o.side.ordinal();
		int p = o.currentSize.getFirst();
		int pos = s * p;
		long marg = -s * o.currentPx.getFirst() * p;
		int mm = o.mm.ordinal();
		if (o.status == OrderStatus.NEW) {
			if (symlim.posLimit > 0) {
				int npos = mrkmkt.closedPos + mrkmkt.openPos + pos;
				if (Math.abs(npos) > symlim.posLimit) {
					System.out.println("WARNING: invalid trade, symbol pos lim exceeded (" + npos + " > " + symlim.posLimit + ")");
					return false;
				}
			}
			int poslim = getValue(symlim.posMmLimit, mm, 0);
			if (poslim > 0) {
				int npos = getValue(mrkmkt.closedPosMm, mm, 0) + getValue(mrkmkt.openPosMm, mm, 0) + pos;
				if (Math.abs(npos) > poslim) {
					System.out.println("WARNING: invalid trade, symbol-mm pos lim exceeded (" + npos + " > " + poslim + ")");
					return false;
				}
			}
			if (symlim.notionalLimit > 0) {
				long nmarg = mrkmkt.closedPnl + mrkmkt.openMargin + marg;
				if (Math.abs(nmarg) > symlim.notionalLimit) {
					System.out.println("WARNING: invalid trade, symbol notional lim exceeded (" + nmarg + " > " + symlim.notionalLimit + ")");
					return false;
				}
			}
			long marglim = getValue(symlim.notionalMmLimit, mm, (long)0);
			if (marglim > 0) {
				long nmarg = getValue(mrkmkt.closedPnlMm, mm, (long)0) + getValue(mrkmkt.openMarginMm, mm, (long)0) + marg;
				if (Math.abs(nmarg) > marglim) {
					System.out.println("WARNING: invalid trade, symbol-mm notional lim exceeded (" + nmarg + " > " + marglim + ")");
					return false;
				}
			}
		} else {
			
			// TODO: handle risk checks for cxlrpl
			return false;
			
		}
		return true;
	}
	
	@Override
	public void onSequence(Order o) {}

	@Override
	public void onConfirm(Order o, Boolean pxchg) {
		int s = 1 - 2*o.side.ordinal();
		int p = o.currentSize.getFirst();
		int pos = s * p;
		long marg = -s * o.currentPx.getFirst() * p;
		int mm = o.mm.ordinal();
		MarkToMarket mrkmkt = getSymbolMark(o.symbolIndex);
		mrkmkt.openCount++;
		mrkmkt.openPos += pos;
		mrkmkt.openMargin += marg;
		Integer m2mcnt = mrkmkt.openCountMm.get(mm);
		mrkmkt.openCountMm.put(mm, m2mcnt != null ? m2mcnt + 1 : 1);
		Integer m2mpos = mrkmkt.openPosMm.get(mm);
		mrkmkt.openPosMm.put(mm, m2mpos != null ? m2mpos + pos : pos);
		Long m2mmarg = mrkmkt.openMarginMm.get(mm);
		mrkmkt.openMarginMm.put(mm, m2mmarg != null ? m2mmarg + marg : marg);
	}

	@Override
	public void onCancel(Order o) {
		int s = 1 - 2*o.side.ordinal();
		int p = o.cxlSize.getFirst();
		int pos = s * p;
		long marg = -s * o.currentPx.getFirst() * p;
		int mm = o.mm.ordinal();
		MarkToMarket mrkmkt = getSymbolMark(o.symbolIndex);
		if (o.currentSize.getFirst() <= 0) {
			mrkmkt.openCount--;
			Integer m2mcnt = mrkmkt.openCountMm.get(mm);
			mrkmkt.openCountMm.put(mm, m2mcnt != null ? m2mcnt - 1 : 0);
		}
		mrkmkt.openPos -= pos;
		mrkmkt.openMargin -= marg;
		Integer m2mpos = mrkmkt.openPosMm.get(mm);
		mrkmkt.openPosMm.put(mm, m2mpos != null ? m2mpos - pos : 0);
		Long m2mmarg = mrkmkt.openMarginMm.get(mm);
		mrkmkt.openMarginMm.put(mm, m2mmarg != null ? m2mmarg - marg : 0);
	}

	@Override
	public void onFill(Order o) {
		int s = 1 - 2*o.side.ordinal();
		int p = o.fillSize.getFirst();
		int pos = s * p;
		long marg = -s * o.fillPx.getFirst() * p;
		int mm = o.mm.ordinal();
		MarkToMarket mrkmkt = getSymbolMark(o.symbolIndex);
		Long fees = comms.getFees(o,  true);
		if (o.currentSize.getFirst() <= 0) {
			mrkmkt.openCount--;
			Integer m2mcnt = mrkmkt.openCountMm.get(mm);
			mrkmkt.openCountMm.put(mm, m2mcnt != null ? m2mcnt - 1 : 0);
		}
		if (o.fillSize.size() == 1) {
			mrkmkt.closedCount++;
			Integer m2mcnt = mrkmkt.closedCountMm.get(mm);
			mrkmkt.closedCountMm.put(mm, m2mcnt != null ? m2mcnt + 1 : 1);
		}
		mrkmkt.openPos -= pos;
		mrkmkt.openMargin -= marg;
		mrkmkt.closedPos += pos;
		mrkmkt.closedPnl += marg;
		mrkmkt.totalFees += fees;
		Integer m2mpos = mrkmkt.openPosMm.get(mm);
		mrkmkt.openPosMm.put(mm, m2mpos != null ? m2mpos - pos : 0);
		Long m2mmarg = mrkmkt.openMarginMm.get(mm);
		mrkmkt.openMarginMm.put(mm, m2mmarg != null ? m2mmarg - marg : 0);
		m2mpos = mrkmkt.closedPosMm.get(mm);
		mrkmkt.closedPosMm.put(mm, m2mpos != null ? m2mpos + pos : pos);
		m2mmarg = mrkmkt.closedPnlMm.get(mm);
		mrkmkt.closedPnlMm.put(mm, m2mmarg != null ? m2mmarg + marg : marg);
		Long m2mfees = mrkmkt.totalFeesMm.get(mm);
		mrkmkt.totalFeesMm.put(mm, m2mfees != null ? m2mfees + fees : fees);
	}

	@Override
	public void onCancelReplace(Order o, Boolean oidchg) {

		// TODO: handle confirmation of cxr
		
	}
	
	public void prettyPrint() {
		for (Integer symbolIndex : m2m.keySet()) {
			MarkToMarket mrkmkt = getSymbolMark(symbolIndex);
			System.out.println(" ** RiskManager: " + si.getSymbol(symbolIndex).displayName);
			System.out.println("    - Fees: " + mrkmkt.totalFees);
			System.out.println("    - Closed Pnl: " + mrkmkt.closedPnl);
			System.out.println("    - Closed Pos: " + mrkmkt.closedPos);
			System.out.println("    - Closed Cnt: " + mrkmkt.closedCount);
			System.out.println("    - Open Marg: " + mrkmkt.openMargin);
			System.out.println("    - Open Pos: " + mrkmkt.openPos);
			System.out.println("    - Open Cnt: " + mrkmkt.openCount);
		}
	}
}
