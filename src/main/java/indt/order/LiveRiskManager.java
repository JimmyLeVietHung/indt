package indt.order;

import indt.event.*;
import indt.event.Event.*;
import indt.utils.*;

public class LiveRiskManager extends RiskManager implements EventListener {
	public LiveRiskManager(OrderManager om, SymbolIndex s, EventLoop e) {
		super(om, s);
		e.addListener(this);
		
		// TODO: establish connection to position server
		
	}
	
	public void getPositionFromServer() {
		
		// TODO: get ground truth position from position server
		
	}
	
	@Override
	public void onEvent(Event e) {
		// Process position event
		if (e.getEventType() == EventType.POSITION) {

			/* TODO: process position event from position server
			public void onPositionMismatch(Order o, Boolean pxchg);
			public void onPositionUpdate();
			 */
			
		}
	}	
}
