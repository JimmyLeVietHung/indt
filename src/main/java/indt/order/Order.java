package indt.order;

import java.util.*;
import indt.utils.*;
import indt.utils.MarketIndex.*;

public class Order {
	// Order specification
	public int symbolIndex;
	public String exchSymbol;
	public MarketIndex.MarketMaker mm;
	public long clientID;
	public enum OrderType {
		NA,
		MARKET,
		LIMIT,
	}
	public OrderType oType;
	
	public enum OrderSide {
		BUY,
		SELL,
		SHORT,
	}
	public OrderSide side;
	public int pxDivisor;
	public LinkedList<Integer> currentPx = new LinkedList<Integer>();
	public LinkedList<Integer> currentSize = new LinkedList<Integer>();

	// Reply updates
	public LinkedList<Long> exchID = new LinkedList<Long>();
	public enum OrderStatus {
		NEW,
		SEQUENCE_NEW,
		CONFIRM_NEW,
		FILL,
		CANCEL,
		CXR,
		SEQUENCE_CXR,
		CONFIRM_CXR,
	}
	public OrderStatus status;
	public LinkedList<Integer> fillPx = new LinkedList<Integer>();
	public LinkedList<Integer> fillSize = new LinkedList<Integer>();
	public LinkedList<Integer> cxlSize = new LinkedList<Integer>();
	public LinkedList<Long> seqTime = new LinkedList<Long>();
	public LinkedList<Long> cnfTime = new LinkedList<Long>();
	public LinkedList<Long> fillTime = new LinkedList<Long>();
	public LinkedList<Long> cxlTime = new LinkedList<Long>();

	public Order(int si, String sym, OrderType ot, OrderSide s, int pxD, int px, int sz, long cid) {
		symbolIndex = si;
		exchSymbol = sym;
		mm = MarketMaker.NA;
		clientID = cid;
		oType = ot;
		side = s;
		pxDivisor = pxD;
		currentPx.clear();
		currentPx.offer(px);
		currentSize.clear();
		currentSize.offer(sz);
		exchID.clear();
		status = OrderStatus.NEW;
		fillPx.clear();
		fillSize.clear();
		cxlSize.clear();
		seqTime.clear();
		cnfTime.clear();
		fillTime.clear();
		cxlTime.clear();
	}
}
