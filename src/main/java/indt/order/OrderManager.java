package indt.order;

import java.util.*;

import indt.event.*;
import indt.event.EventListener;
import indt.event.Event.*;
import indt.order.Order.*;
import indt.trader.*;
import indt.utils.*;
import indt.utils.MarketIndex.*;

public class OrderManager implements EventListener {
	protected LinkedList<OrderListener> ol = new LinkedList<OrderListener>();
	private SymbolIndex si;
	private static long static_counter = 1;
	private ArrayList<Trader> traders = new ArrayList<Trader>();
	private ArrayList<HashMap<Long, Order> > newMmMap = new ArrayList<HashMap<Long, Order> >();
	private ArrayList<HashMap<Long, Order> > exchMmMap = new ArrayList<HashMap<Long, Order> >();
	private long lastnocheck = -1;

	public OrderManager(EventLoop e, SymbolIndex _si) {
		si = _si;
		e.addListener(this);
	}

	public void addListener(OrderListener _ol) {
		ol.offer(_ol);
	}
	
	public void addTrader(Trader t) {
		int mmIdx = t.getMM().ordinal();
		if (mmIdx >= traders.size()) {
			int delta = mmIdx - traders.size() + 1;
			for (int i = 0; i < delta; i++)
				traders.add(null);
		}
		traders.set(mmIdx, t);
	}

	private Trader getTrader(MarketMaker mm) {
		int mmIdx = mm.ordinal();
		if (mmIdx >= traders.size()) {
			int delta = mmIdx - traders.size() + 1;
			for (int i = 0; i < delta; i++)
				traders.add(null);
		}
		return traders.get(mmIdx);
	}
	
	public long getNewClientID() {
		long clientID;
		synchronized(this) {
			clientID = static_counter;
			static_counter++;
		}
		return clientID;
	}
	
	private HashMap<Long, Order> findMap(ArrayList<HashMap<Long, Order> > mp, MarketMaker m) {
		int mmIdx = m.ordinal();
		if (mmIdx >= mp.size()) {
			int delta = mmIdx - mp.size() + 1;
			for (int i = 0; i < delta; i++)
				mp.add(null);
		}
		HashMap<Long, Order> h = mp.get(mmIdx);
		if (h == null) {
			h = new HashMap<Long, Order>();
			mp.set(mmIdx, h);
		}
		return h;
	}
	
	private void addNewOrder(Order o) {
		HashMap<Long, Order> mp = findMap(newMmMap, o.mm);
		if (mp.containsKey(o.clientID)) {
			System.err.println("ERROR: duplicate clientID " + o.clientID + ", unable to add new order to OM");
			return;
		}
		mp.put(o.clientID, o);
	}
	
	private Order findNewOrder(long cid, MarketMaker mm, Boolean remove) {
		HashMap<Long, Order> mp = findMap(newMmMap, mm);
		Order o = mp.get(cid);
		if (o != null && remove)
			mp.remove(cid);
		return o;
	}
	
	private void addCnfOrder(Order o) {
		HashMap<Long, Order> mp = findMap(exchMmMap, o.mm);
		if (mp.containsKey(o.exchID.getFirst())) {
			System.err.println("ERROR: duplicate exchID " + o.exchID.getFirst() + ", unable to add cnf order to OM");
			return;
		}
		mp.put(o.exchID.getFirst(), o);
	}

	private Order findCnfOrder(long eid, MarketMaker mm, Boolean remove) {
		HashMap<Long, Order> mp = findMap(exchMmMap, mm);
		Order o = mp.get(eid);
		if (o != null && remove)
			mp.remove(eid);
		return o;
	}
	
	public Boolean trade(Order o) {
		Trader t = getTrader(o.mm);
		if (t != null)
			if (t.trade(o)) {
				addNewOrder(o);
				return true;
			}
		return false;
	}
	
	@Override
	public void onEvent(Event e) {
		// Check noseq, nocnf orders
		if (lastnocheck > 0 && e.timestamp > lastnocheck + 5000000) {
			
			// TODO: check for noseq & nocnf orders
			
		}
		
		// Filter order events
		if (e.getEventType() != EventType.ORDER)
			return;

		// Process
		Boolean complete;
		Order o;
		OrderEvent oe = (OrderEvent)e;
		switch (oe.getActionType()) {
		case SEQUENCE:
			o = findNewOrder(oe.clientID, oe.mm, false);
			o.seqTime.offer(oe.timestamp);
			o.status = OrderStatus.SEQUENCE_NEW;
			for (OrderListener l : ol)
				l.onSequence(o);
			break;
		case CONFIRM:
			o = findNewOrder(oe.clientID, oe.mm, true);
			o.cnfTime.offer(e.timestamp);
			o.status = OrderStatus.CONFIRM_NEW;
			o.exchID.offer(oe.exchangeID);
			Boolean pxchg = false;
			if (o.currentPx.getFirst() != oe.px) {
				pxchg = true;
				o.currentPx.set(0, oe.px);
			}
			addCnfOrder(o);
			for (OrderListener l : ol)
				l.onConfirm(o, pxchg);
			break;
		case FILL:
			FillOrderEvent fe = (FillOrderEvent)oe;
			complete = false;
			if (fe.sizeLeft <= 0)
				complete = true;
			o = findCnfOrder(fe.exchangeID, fe.mm, complete);
			o.status = OrderStatus.FILL;
			o.currentSize.offer(fe.sizeLeft);
			o.fillPx.offer(fe.px);
			o.fillSize.offer(fe.size);
			o.fillTime.offer(fe.timestamp);
			for (OrderListener l : ol)
				l.onFill(o);
			break;
		case CANCEL:
			CancelOrderEvent ce = (CancelOrderEvent)oe;
			complete = false;
			if (ce.sizeLeft <= 0)
				complete = true;
			o = findCnfOrder(oe.exchangeID, oe.mm, complete);
			o.status = OrderStatus.CANCEL;
			o.currentSize.offer(ce.sizeLeft);
			o.cxlSize.offer(ce.size);
			o.cxlTime.offer(ce.timestamp);
			for (OrderListener l : ol)
				l.onCancel(o);
			break;			
		case CONFIRM_CXR:
			
			// TODO: handle cxr of limit orders
			//public void onCancelReplace(Order o, Boolean oidchg);
			
			break;			
		default:
			System.err.println("Unhandled order event status type " + oe.getActionType().ordinal());
			break;
		}
	}
}
