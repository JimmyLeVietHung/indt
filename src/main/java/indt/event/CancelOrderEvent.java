package indt.event;

import indt.utils.MarketIndex.*;

public class CancelOrderEvent extends OrderEvent {
	public int sizeLeft;
	
	public CancelOrderEvent(String sym, int sz, int szleft, long eid, MarketMaker m, long ts) {
		super(sym, 0, sz, 0, 0, 0, eid, m, ts);
		sizeLeft = szleft;
	}
	
	@Override
	public ActionType getActionType() {
		return ActionType.CANCEL;
	}
}
