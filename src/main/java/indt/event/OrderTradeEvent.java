package indt.event;

import indt.order.Order.*;
import indt.utils.MarketIndex.*;

public class OrderTradeEvent extends TradeEvent {
	public OrderTradeEvent(String sym, int s, int sz, int p, int pxd, long eid, MarketMaker m, OrderType t, long ts) {
		super(sym, s, sz, p, pxd, eid, m, t, ts);
	}
	
	@Override
	public ActionType getActionType() {
		return ActionType.ORDER;
	}
}
