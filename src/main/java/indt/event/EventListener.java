package indt.event;

public interface EventListener {
	public void onEvent(Event e);
}
