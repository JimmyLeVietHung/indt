package indt.event;

import indt.utils.MarketIndex.*;

public class FillOrderEvent extends OrderEvent {
	public int sizeLeft;
	
	public FillOrderEvent(String sym, int s, int sz, int szleft, int p, int pxd, long eid, MarketMaker m, long ts) {
		super(sym, s, sz, p, pxd, 0, eid, m, ts);
		sizeLeft = szleft;
	}
	
	@Override
	public ActionType getActionType() {
		return ActionType.FILL;
	}
}
