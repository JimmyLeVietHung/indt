package indt.event;

import indt.order.Order.*;
import indt.utils.MarketIndex.*;

public class TradeEvent extends Event {
	public int side;
	public int size;
	public int px;
	public int pxDivisor;
	public long exchangeID;
	public MarketMaker mm = MarketMaker.NA;
	public OrderType ot = OrderType.NA;
	
	public TradeEvent(String sym, int s, int sz, int p, int pxd, long eid, MarketMaker m, OrderType t, long ts) {
		super();
		symbol = sym;
		side = s;
		size = sz;
		px = p;
		pxDivisor = pxd;
		exchangeID = eid;
		mm = m;
		ot = t;
		timestamp = ts;
	}
	
	@Override
	public EventType getEventType() {
		return EventType.TRADE;
	}

	public enum ActionType {
		NA,
		ORDER,
		CANCEL,
		CXR,
	}
	public ActionType getActionType() {
		return ActionType.NA;
	}
	
	@Override
	public MarketMaker getMM() {
		return mm;
	}
}
