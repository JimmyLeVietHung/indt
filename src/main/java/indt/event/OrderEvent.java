package indt.event;

import indt.utils.MarketIndex.*;

public class OrderEvent extends Event {
	public int side;
	public int size;
	public int px;
	public int pxDivisor;
	public long clientID;
	public long exchangeID;
	public MarketMaker mm = MarketMaker.NA;
	
	public OrderEvent(String sym, int s, int sz, int p, int pxd, long cid, long eid, MarketMaker m, long ts) {
		super();
		symbol = sym;
		side = s;
		size = sz;
		px = p;
		pxDivisor = pxd;
		clientID = cid;
		exchangeID = eid;
		mm = m;
		timestamp = ts;
	}
	
	@Override
	public EventType getEventType() {
		return EventType.ORDER;
	}

	public enum ActionType {
		NA,
		SEQUENCE,
		CONFIRM,
		FILL,
		CANCEL,
		CONFIRM_CXR,
	}
	public ActionType getActionType() {
		return ActionType.NA;
	}
	
	@Override
	public MarketMaker getMM() {
		return mm;
	}
}
