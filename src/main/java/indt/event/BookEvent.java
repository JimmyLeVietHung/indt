package indt.event;

public class BookEvent extends Event {
	public int side;
	public int size;
	public int px;
	public int pxDivisor;
	public long orderID;
	public int oldSize;
	public int oldPx;
	public long oldOrderID;
	public boolean done;
	
	public enum ActionType {
		NA,
		ADD,		// add new price level
		REMOVE,		// remove size (including partial)
		UPDATE,		// price change or size increase
	}
	public ActionType actType;

	public BookEvent(String sym, int s, int sz, int p, int pd, long oid, ActionType at, long ts, boolean dn) {
		super();
		symbol = sym;
		side = s;
		size = sz;
		px = p;
		pxDivisor = pd;
		orderID = oid;
		actType = at;
		timestamp = ts;
		done = dn;
	}
	
	public BookEvent(String sym, int s, int sz, int p, int pd, long oid, int osz, int op, long ooid, ActionType at, long ts, boolean dn) {
		this(sym, s, sz, p, pd, oid, at, ts, dn);
		oldSize = osz;
		oldPx = op;
		oldOrderID = ooid;
	}

	@Override
	public EventType getEventType() {
		return EventType.BOOK;
	}

	public ActionType getActionType() {
		return actType;
	}
}
