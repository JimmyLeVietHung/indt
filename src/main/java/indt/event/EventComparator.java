package indt.event;

import java.util.*;

public class EventComparator implements Comparator<Event> {
	@Override
	public int compare(Event o1, Event o2) {
		if (o1.timestamp < o2.timestamp)
			return -1;
		else if (o1.timestamp > o2.timestamp)
			return 1;
		else if (o1.counter < o2.counter)
			return -1;
		else if (o1.counter > o2.counter)
			return 1;
		return 0;
	}
}
