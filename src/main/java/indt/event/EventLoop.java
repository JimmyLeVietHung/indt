package indt.event;

import java.util.*;

import indt.datasource.*;
import indt.utils.*;

public class EventLoop {
	protected SymbolIndex si;
	protected DataSource ds;
	protected LinkedList<EventListener> el = new LinkedList<EventListener>();
	protected Boolean active = false;
	public long timestamp = 0;
	
	public EventLoop(SymbolIndex _si, DataSource _ds) {
		si = _si;
		ds = _ds;
	}
	
	public void addListener(EventListener _el) {
		el.offer(_el);
	}

	public void start() {
		active = true;
		
		//Event e;
		//while (active && (e = ds.next()) != null) {
		while (active) {
			Event e = ds.next();
			//System.out.println(e);
			int idx = si.getIndex(new Symbol(e.symbol.toString()));
			if (idx >= 0) {
				e.symbolIndex = idx;
				timestamp = e.timestamp;
				//System.out.println("sss "+ el.size());
				for (EventListener l : el)
					l.onEvent(e);
			}
		}
	}
	
	public void stop() {
		active = false;
	}
}
