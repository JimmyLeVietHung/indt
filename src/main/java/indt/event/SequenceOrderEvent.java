package indt.event;

import indt.utils.MarketIndex.*;

public class SequenceOrderEvent extends OrderEvent {
	public SequenceOrderEvent(String sym, int s, int sz, int p, int pxd, long cid, MarketMaker m, long ts) {
		super(sym, s, sz, p, pxd, cid, 0, m, ts);
	}
	
	@Override
	public ActionType getActionType() {
		return ActionType.SEQUENCE;
	}
}
