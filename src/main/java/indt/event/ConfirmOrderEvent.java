package indt.event;

import indt.utils.MarketIndex.*;

public class ConfirmOrderEvent extends OrderEvent {
	public ConfirmOrderEvent(String sym, int s, int sz, int p, int pxd, long cid, long eid, MarketMaker m, long ts) {
		super(sym, s, sz, p, pxd, cid, eid, m, ts);
	}
	
	@Override
	public ActionType getActionType() {
		return ActionType.CONFIRM;
	}
}
