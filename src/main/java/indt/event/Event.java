package indt.event;

import indt.utils.MarketIndex.*;

public class Event {
	public String symbol;
	public long timestamp;  // in us
	public long counter;
	private static long static_counter = 0;
	
	// Populated by handler after event is read
	public int symbolIndex = -1;
	
	public Event() {
		synchronized(this) {
			counter = static_counter;
			static_counter++;
		}
	}
	
	public MarketMaker getMM() {
		return MarketMaker.NA;
	}
	
	public enum EventType {
		NA,
		BOOK,
		ORDER,
		TRADE,  // only used for sim
		POSITION,
	}
	public EventType getEventType() {
		return EventType.NA;
	}
}
