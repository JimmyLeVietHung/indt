package indt.event;

import indt.utils.*;

public class SHFEBookEvent extends BookEvent {
	public SHFEBookEvent(String sym, int s, int sz, int p, int pd, long oid, ActionType at, long ts, boolean dn) {
		super(sym, s, sz, p, pd, oid, at, ts, dn);
	}

	public SHFEBookEvent(String sym, int s, int sz, int p, int pd, long oid, int osz, int op, long ooid, ActionType at, long ts, boolean dn) {
		super(sym, s, sz, p, pd, oid, osz, op, ooid, at, ts, dn);
	}

	@Override
	public MarketIndex.MarketMaker getMM() {
		return MarketIndex.MarketMaker.SHFE;
	}
}
