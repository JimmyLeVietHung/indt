package indt.datasource;

import java.util.PriorityQueue;

import indt.event.*;
import indt.order.*;
import indt.utils.*;
import indt.utils.MarketIndex.MarketMaker;

public class SimDataSource extends FileDataSource {
	private SymbolIndex si;
	private static final int INITPQSIZE = 64;
	private EventComparator ec = new EventComparator();
	private PriorityQueue<Event>epq = new PriorityQueue<Event>(INITPQSIZE, ec);

	public SimDataSource(SymbolIndex _s) {
		super(-1, new Symbol("SIM"));
		si = _s;
	}

	@Override
	public DataSourceType getType() {
		return DataSourceType.SIM;
	}
	
	@Override
	public Event next() {
		// Return next available event
		if (epq.size() > 0)
			return epq.poll();
		return null;
	}

	public boolean hasNext() {
		if (epq.size() > 0)
			return true;
		return false;
	}
	
	public void insertSequenceOrderEvent(Order o, long ts) {
		SequenceOrderEvent se = new SequenceOrderEvent(si.getSymbol(o.symbolIndex).displayName, o.side.ordinal(), o.currentSize.getFirst(), o.currentPx.getFirst(), o.pxDivisor, o.clientID, o.mm, ts);
		epq.offer(se);
	}
	
	public void insertConfirmOrderEvent(Order o, long ts) {
		ConfirmOrderEvent ce = new ConfirmOrderEvent(si.getSymbol(o.symbolIndex).displayName, o.side.ordinal(), o.currentSize.getFirst(), o.currentPx.getFirst(), o.pxDivisor, o.clientID, o.clientID, o.mm, ts);
		epq.offer(ce);
	}
		
	public void insertFillOrderEvent(String sym, int s, int sz, int szleft, int p, int pxd, long eid, MarketMaker m, long ts) {
		FillOrderEvent fe = new FillOrderEvent(sym, s, sz, szleft, p, pxd, eid, m, ts);
		epq.offer(fe);
	}
	
	public void insertCancelOrderEvent(String sym, int sz, int szleft, long eid, MarketMaker m, long ts) {
		CancelOrderEvent ce = new CancelOrderEvent(sym, sz, szleft, eid, m, ts);
		epq.offer(ce);
	}
	
	public void insertConfirmCxrOrderEvent() {

		// TODO: add cxr order event
		//CxrOrderEvent
		
	}
	
	public void insertOrderTradeEvent(Order o, long ts) {
		OrderTradeEvent oe = new OrderTradeEvent(si.getSymbol(o.symbolIndex).displayName, o.side.ordinal(), o.currentSize.getFirst(), o.currentPx.getFirst(), o.pxDivisor, o.clientID, o.mm, o.oType, ts);
		epq.offer(oe);
	}
	
 	public void insertCancelTradeEvent(Order o, long ts) {

 		// TODO: insert cancel trade event
 		//CancelTradeEvent

 	}
 	
 	public void insertCxrTradeEvent() {

 		// TODO: insert cxr trade event
 		//CxrTradeEvent
 		
 	}
}
