package indt.datasource;

import java.util.*;
import java.io.*;
import java.text.*;

import indt.event.*;
import indt.utils.*;

//tick data from http://disktrading.is99.com/disktrading/
public class DiskTradingFileDataSource extends FileDataSource {
	private BufferedReader br = null;
	private static final String FILEROOTPATH = "E:/IntradayData/";
	private Symbol symbol = null;
	private int multiplier = 0;
	private int orderNum = 0;
	private int prevPrice = 0;
	
	//dte doesn't matter
	//sym like 6AU3
	public DiskTradingFileDataSource(int dte, Symbol sym, int mult) {
		super(dte, sym);
		symbol = sym;
		multiplier = mult;
		
		String filePath = String.format("%s/%s.csv", FILEROOTPATH, sym.displayName);
		try {
			br = new BufferedReader(new FileReader(filePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		skipNextBlock(); //clear header
		while (!hasNext()) {//read until have first proper line
			
			readNextBlock();  
		}
		System.out.println("cc" +epq.size() );
	}

	private void skipNextBlock() {
		try {
			System.out.println(br.readLine());
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	@Override
	protected void readNextBlock() {	

		try {
			if (br == null) {
				return;
			}
			String line = br.readLine();
			if (line == null) {
				br.close();
				br = null;
			} else {
	
				//System.out.println("count1:"+count1);
				String[] lineEntries = line.split(",");
				//last few columns are bid/ask & sizes
				String dateStr = lineEntries[0];
				String timeStr = lineEntries[1];
				String priceStr = lineEntries[5];
				
				long dateTimeNum = parseTime (dateStr, timeStr);
				int price = Math.round(Float.parseFloat(priceStr) * multiplier);
				
				if (price == 0) { //faulty data
					return;
				}
				
				if (orderNum==0) {
					//first one is add event for bid & ask
					epq.offer(new SHFEBookEvent(symbol.displayName, 0, 1, price, multiplier, -1, SHFEBookEvent.ActionType.ADD, dateTimeNum, false)); //use price for bid price
					epq.offer(new SHFEBookEvent(symbol.displayName, 1, 1, price, multiplier, -1, SHFEBookEvent.ActionType.ADD, dateTimeNum, false)); //use price for ask price
					
				} else {
					//subsequent one is update event for bid & ask
					epq.offer(new SHFEBookEvent(symbol.displayName, 0, 1, price, multiplier, -1, 1, prevPrice, -1, SHFEBookEvent.ActionType.UPDATE, dateTimeNum, false)); //use price for bid price
					epq.offer(new SHFEBookEvent(symbol.displayName, 1, 1, price, multiplier, -1, 1, prevPrice, -1, SHFEBookEvent.ActionType.UPDATE, dateTimeNum, false)); //use price for ask price
				}
		
				prevPrice = price;
				orderNum+=2;
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	private long parseTime(String dateStr, String timeStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date parsedDate = null;
		try {
			parsedDate =  formatter.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		String hrStr = timeStr.substring(0, 2);
		String minStr = timeStr.substring(2, 4);
		int hr = Integer.valueOf(hrStr);
		int min = Integer.valueOf(minStr);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(parsedDate);
		cal.add(Calendar.HOUR, hr);
		cal.add(Calendar.MINUTE, min);
		
		Date parsedDateTime = cal.getTime();
		long result = parsedDateTime.getTime() * 1000;
		
		return result;
	}
}
