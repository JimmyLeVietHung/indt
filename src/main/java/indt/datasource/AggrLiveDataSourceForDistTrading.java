package indt.datasource;

import java.util.*;
import indt.event.*;

public class AggrLiveDataSourceForDistTrading implements DataSource {
	private static final int INITPQSIZE = 64;
	private EventComparator ec = new EventComparator();
	protected PriorityQueue<Event>epq = new PriorityQueue<Event>(INITPQSIZE, ec);
	private LinkedList<FileDataSource> dfs = new LinkedList<FileDataSource>();

	public void addSource(FileDataSource d) {
		if (d.hasNext() || d.getType() == DataSourceType.SIM)
			dfs.offer(d);
	}

	@Override
	public DataSourceType getType() {
		return DataSourceType.LIVE;
	}
		
	@Override
	public Event next() {
		if (dfs.size() > 0) {
			Boolean validadd = false;
			while (!validadd) {
				// Get next available datasource
				FileDataSource d = dfs.poll();
				
				// Read all for current timestamp
				long lasttimestamp = -1;
				while (d.hasNext()) {
					Event e = d.next();
					epq.offer(e);
					validadd = true;
					
					if (lasttimestamp < 0)
						lasttimestamp = e.timestamp;
					else if (e.timestamp > lasttimestamp)
						break;
				}
				if (d.hasNext())
					dfs.offer(d);
				else if (d.getType() == DataSourceType.SIM) {
					dfs.offer(d);
					if (dfs.size() == 1)
						validadd = true;
				}
			}
		}
		
		// Return next available event
		if (epq.size() > 0)
			return epq.poll();
		return null;
	}
}
