package indt.datasource;

import java.util.PriorityQueue;

import indt.event.*;
import indt.utils.*;

public class FileDataSource implements DataSource {
	protected static final int INITPQSIZE = 64;
	protected EventComparator ec = new EventComparator();
	public PriorityQueue<Event>epq = new PriorityQueue<Event>(INITPQSIZE, ec);

	public FileDataSource(int dte, Symbol sym) {
		// Loads venue-specific historical file in venue-specific class
		// Just a function signature here
	}

	public DataSourceType getType() {
		return DataSourceType.HISTORICAL;
	}
	
	protected void readNextBlock() {
		// Reads next block in data file
		// Just a function signature here
	}

	public Event next() {
		// Read next block of data
		
		readNextBlock();
		
		// Return next event
		return epq.poll();
	}

	public boolean hasNext() {
		if (epq.size() > 0)
			return true;
		return false;
	}
	
	public long nextTS() {
		// Returns the timestamp of the next event to be returned by next()
		if (epq.size() > 0)
			return epq.peek().timestamp;
		return 0;
	}
}
