package indt.datasource;

import java.util.*;
import java.io.*;
import java.text.*;

import indt.event.*;
import indt.utils.*;
public class DiskTradingLiveDataSource extends FileDataSource {
	private Symbol symbol = null;
	private int multiplier = 0;
	private int orderNum = 0;
	private int prevPrice = 0;
	
	//dte doesn't matter
	//sym like 6AU3
	public DiskTradingLiveDataSource(int dte, Symbol sym, int mult) {
		super(dte, sym);
		symbol = sym;
		multiplier = mult;
	}



	private long parseTime(String dateStr, String timeStr) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date parsedDate = null;
		try {
			parsedDate =  formatter.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		String hrStr = timeStr.substring(0, 2);
		String minStr = timeStr.substring(2, 4);
		int hr = Integer.valueOf(hrStr);
		int min = Integer.valueOf(minStr);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(parsedDate);
		cal.add(Calendar.HOUR, hr);
		cal.add(Calendar.MINUTE, min);
		
		Date parsedDateTime = cal.getTime();
		long result = parsedDateTime.getTime() * 1000;
		
		return result;
	}
}
