package indt.datasource;

import indt.event.*;

public interface DataSource {
	public enum DataSourceType {
		HISTORICAL,
		LIVE,
		SIM,
	}
	public DataSourceType getType();
	public Event next();
}
