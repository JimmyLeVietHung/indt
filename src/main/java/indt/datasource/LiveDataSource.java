package indt.datasource;

import java.util.PriorityQueue;

import indt.event.*;
import indt.utils.*;

public class LiveDataSource implements DataSource {
	protected static final int INITPQSIZE = 64;
	protected EventComparator ec = new EventComparator();
	public PriorityQueue<Event>epq = new PriorityQueue<Event>(INITPQSIZE, ec);

	public LiveDataSource(int dte, Symbol sym) {
		// Loads venue-specific historical file in venue-specific class
		// Just a function signature here
	}

	public DataSourceType getType() {
		return DataSourceType.LIVE;
	}
	
	

	public Event next() {
	
		return epq.poll();
	}

	public boolean hasNext() {
		if (epq.size() > 0)
			return true;
		return false;
	}
	
	public long nextTS() {
		// Returns the timestamp of the next event to be returned by next()
		if (epq.size() > 0)
			return epq.peek().timestamp;
		return 0;
	}
}
