package indt.datasource;

import indt.event.Event;
import indt.event.EventComparator;
import indt.utils.Symbol;
import indt.event.*;

import com.ib.client.AnyWrapperMsgGenerator;
import com.ib.client.CommissionReport;
import com.ib.client.Contract;
import com.ib.client.ContractDetails;
import com.ib.client.EClientSocket;
import com.ib.client.EWrapper;
import com.ib.client.EWrapperMsgGenerator;
import com.ib.client.Execution;
import com.ib.client.Order;
import com.ib.client.OrderState;
import com.ib.client.UnderComp;
import com.ib.client.Util;
import com.ib.client.ComboLeg;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Vector;
import java.io.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.lang.Thread;

public class IBLiveDataSourceForTrendPeriodic extends LiveDataSource implements
		EWrapper {

	private ArrayList<Double> closeMinutePrice = new ArrayList<Double>();
	private Symbol symbol = null;
	private int multiplier = 0;
	private int orderNum = 0;
	private int prevPrice = 0;

	private EClientSocket mClient = new EClientSocket(this);
	private Contract mContract = new Contract(0, "EUR", "CASH", "", 0, "", "",
			"IDEALPRO", "USD", "", "", new java.util.Vector<ComboLeg>(),
			"IDEALPRO", false, "", "");
	private static DateFormat dateFormat = new SimpleDateFormat(
			"yyyy/MM/dd HH:mm");
	private static String keepTime = dateFormat.format(Calendar.getInstance()
			.getTime());

	private static final int INITPQSIZE = 1024;
	private EventComparator ec = new EventComparator();
	private PriorityQueue<Event> epq = new PriorityQueue<Event>(INITPQSIZE, ec);
	private boolean TheQueueIsEmpty = true;

	public IBLiveDataSourceForTrendPeriodic(int dte, Symbol sym, int mult) {
		super(dte, sym);
		symbol = sym;
		multiplier = mult;

		// just to make sure epq is NOT empty, will handle this problem later
		// epq.offer(new SHFEBookEvent(symbol.displayName, 0, 1,10977,
		// multiplier, -1, SHFEBookEvent.ActionType.ADD, 2320455, false)); //use
		// price for bid price
		// epq.offer(new SHFEBookEvent(symbol.displayName, 1, 1,12343,
		// multiplier, -1, SHFEBookEvent.ActionType.ADD, 2320467, false)); //use
		// price for ask price

		connect();
	}

	public void connect() {
		mClient.eConnect(null, 7496, 3);
		mClient.reqMktData(1, mContract, null, false);
		strategyCheckLoop();
	}

	/*
	 * public void Init() { mClient.eConnect("", 7496, 0); if
	 * (mClient.isConnected()) {
	 * System.out.println("IB connected to Tws server version " +
	 * mClient.serverVersion() + " at " + mClient.TwsConnectionTime()); } else {
	 * System.err.println("IB failed to connect"); }
	 * 
	 * 
	 * mClient.reqRealTimeBars(0, mContract, 5, "TRADES", true);
	 * mClient.reqRealTimeBars(0, mContract, 5, "BID", true);
	 * mClient.reqRealTimeBars(0, mContract, 5, "ASK", true); }
	 */
	public void strategyCheckLoop() {
		try {
			Thread.sleep(2000);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public Event next() {
		synchronized (epq) {
			while (epq.isEmpty()) {
				try {
					epq.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return epq.poll();
		}

	}

	@Override
	public void error(Exception e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void error(String str) {
		// TODO Auto-generated method stub

	}

	@Override
	public void error(int id, int errorCode, String errorMsg) {
		// TODO Auto-generated method stub

	}

	@Override
	public void connectionClosed() {
		// TODO Auto-generated method stub

	}

	
	public void tickPrice11(int tickerId, int field, double price,
			int canAutoExecute) {
		synchronized (epq) {
			epq.offer(new SHFEBookEvent(symbol.displayName, 0, 1, (int) Math
					.round(price * multiplier), multiplier, -1,
					SHFEBookEvent.ActionType.ADD, 190000, false)); // use price
																	// for bid
																	// price
			epq.notify();
		}
	}
	@Override
	public void tickPrice(int tickerId, int field, double price,
			int canAutoExecute) {
			try {
				String currentMinute = dateFormat.format(Calendar.getInstance()
						.getTime());
				if (keepTime.compareTo(currentMinute) == 0) {
					if (field == 1) {// bid
						closeMinutePrice.add(price);
					}
				} else {
					System.out.println(currentMinute);
					keepTime = currentMinute;
					if (closeMinutePrice.size() > 0) {
						long dateTimeNum = new Date().getTime() * 1000;
						int price1 = (int) Math.round(closeMinutePrice.get(closeMinutePrice.size()-1) * multiplier);
						System.out.println("TickPrice: " + price1 );
						synchronized (epq) {
							if (orderNum == 0) {
								// first one is add event for bid & ask
								epq.offer(new SHFEBookEvent(symbol.displayName,
										0, 1, price1, multiplier, -1,
										SHFEBookEvent.ActionType.ADD,
										dateTimeNum, false)); // use price for
																// bid price
								epq.offer(new SHFEBookEvent(symbol.displayName,
										1, 1, price1, multiplier, -1,
										SHFEBookEvent.ActionType.ADD,
										dateTimeNum, false)); // use price for
																// ask price

							} else {
								// subsequent one is update event for bid & ask
								epq.offer(new SHFEBookEvent(symbol.displayName,
										0, 1, price1, multiplier, -1, 1,
										prevPrice, -1,
										SHFEBookEvent.ActionType.UPDATE,
										dateTimeNum, false)); // use price for
																// bid price
								epq.offer(new SHFEBookEvent(symbol.displayName,
										1, 1, price1, multiplier, -1, 1,
										prevPrice, -1,
										SHFEBookEvent.ActionType.UPDATE,
										dateTimeNum, false)); // use price for
																// ask price
							}
							epq.notify();
						}
						prevPrice = price1;
						orderNum += 2;
					}
					closeMinutePrice = new ArrayList<Double>();
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("ERROR in TickPrice: " + e);
			}
	}

	@Override
	public void tickSize(int tickerId, int field, int size) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tickOptionComputation(int tickerId, int field,
			double impliedVol, double delta, double optPrice,
			double pvDividend, double gamma, double vega, double theta,
			double undPrice) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tickGeneric(int tickerId, int tickType, double value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tickString(int tickerId, int tickType, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tickEFP(int tickerId, int tickType, double basisPoints,
			String formattedBasisPoints, double impliedFuture, int holdDays,
			String futureExpiry, double dividendImpact, double dividendsToExpiry) {
		// TODO Auto-generated method stub

	}

	@Override
	public void orderStatus(int orderId, String status, int filled,
			int remaining, double avgFillPrice, int permId, int parentId,
			double lastFillPrice, int clientId, String whyHeld) {
		// TODO Auto-generated method stub

	}

	@Override
	public void openOrder(int orderId, Contract contract, Order order,
			OrderState orderState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void openOrderEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAccountValue(String key, String value, String currency,
			String accountName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updatePortfolio(Contract contract, int position,
			double marketPrice, double marketValue, double averageCost,
			double unrealizedPNL, double realizedPNL, String accountName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateAccountTime(String timeStamp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountDownloadEnd(String accountName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void nextValidId(int orderId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contractDetails(int reqId, ContractDetails contractDetails) {
		// TODO Auto-generated method stub

	}

	@Override
	public void bondContractDetails(int reqId, ContractDetails contractDetails) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contractDetailsEnd(int reqId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void execDetails(int reqId, Contract contract, Execution execution) {
		// TODO Auto-generated method stub

	}

	@Override
	public void execDetailsEnd(int reqId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMktDepth(int tickerId, int position, int operation,
			int side, double price, int size) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateMktDepthL2(int tickerId, int position,
			String marketMaker, int operation, int side, double price, int size) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateNewsBulletin(int msgId, int msgType, String message,
			String origExchange) {
		// TODO Auto-generated method stub

	}

	@Override
	public void managedAccounts(String accountsList) {
		// TODO Auto-generated method stub

	}

	@Override
	public void receiveFA(int faDataType, String xml) {
		// TODO Auto-generated method stub

	}

	@Override
	public void historicalData(int reqId, String date, double open,
			double high, double low, double close, int volume, int count,
			double WAP, boolean hasGaps) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scannerParameters(String xml) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scannerData(int reqId, int rank,
			ContractDetails contractDetails, String distance, String benchmark,
			String projection, String legsStr) {
		// TODO Auto-generated method stub

	}

	@Override
	public void scannerDataEnd(int reqId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void realtimeBar(int reqId, long time, double open, double high,
			double low, double close, long volume, double wap, int count) {
		System.out
				.println(String
						.format("reqID: %d, time: %d, open: %f, high: %f, low: %f, close: %f, volume: %d, wap: %f, count: %d",
								reqId, time, open, high, low, close, volume,
								wap, count));
	}

	@Override
	public void currentTime(long time) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fundamentalData(int reqId, String data) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deltaNeutralValidation(int reqId, UnderComp underComp) {
		// TODO Auto-generated method stub

	}

	@Override
	public void tickSnapshotEnd(int reqId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void marketDataType(int reqId, int marketDataType) {
		// TODO Auto-generated method stub

	}

	@Override
	public void commissionReport(CommissionReport commissionReport) {
		// TODO Auto-generated method stub

	}

	@Override
	public void position(String account, Contract contract, int pos,
			double avgCost) {
		// TODO Auto-generated method stub

	}

	@Override
	public void positionEnd() {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountSummary(int reqId, String account, String tag,
			String value, String currency) {
		// TODO Auto-generated method stub

	}

	@Override
	public void accountSummaryEnd(int reqId) {
		// TODO Auto-generated method stub

	}

}
