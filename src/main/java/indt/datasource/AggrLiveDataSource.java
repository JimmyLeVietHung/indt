package indt.datasource;

import java.util.*;
import indt.event.*;

public class AggrLiveDataSource implements DataSource {
	private static final int INITPQSIZE = 64;
	private EventComparator ec = new EventComparator();
	private PriorityQueue<Event>epq = new PriorityQueue<Event>(INITPQSIZE, ec);
	private LinkedList<LiveDataSource> dfs = new LinkedList<LiveDataSource>();

	public void addSource(LiveDataSource d) {
		if (d.hasNext() || d.getType() == DataSourceType.LIVE)
			dfs.offer(d);
	}

	@Override
	public DataSourceType getType() {
		return DataSourceType.LIVE;
	}
		
	@Override
	public Event next() {
		LiveDataSource d = dfs.peek();//dfs.poll();
		Event e = d.next();
		epq.offer(e);
		return epq.poll();
	}

	
}
