package indt.datasource;

import indt.event.*;
import indt.utils.*;

public class SHFEFileDataSource extends FileDataSource {
	public SHFEFileDataSource(int dte, Symbol sym) {
		super(dte, sym);
		
		// Load historical file for specified date
		// TODO: add logic to load date & symbol specific file
		//       currently pushes a few sample events only (remove this when readNextBlock is implemented)
		insertSHFEBookEvents(sym);
		
		// Read first block
		readNextBlock();
	}
	
	private void insertSHFEBookEvents(Symbol sym) {
		if (sym.displayName.equals("AUF5")) {
			// AUF5 - Add
			epq.offer(new SHFEBookEvent("AUF5", 0, 10, 1235, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, false));
			epq.offer(new SHFEBookEvent("AUF5", 0, 88, 1234, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, false));
			epq.offer(new SHFEBookEvent("AUF5", 0, 100, 1233, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, false));
			epq.offer(new SHFEBookEvent("AUF5", 0, 99, 1231, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, false));
			epq.offer(new SHFEBookEvent("AUF5", 0, 50, 1228, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, false));	
			epq.offer(new SHFEBookEvent("AUF5", 1, 1000, 1238, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, false));
			epq.offer(new SHFEBookEvent("AUF5", 1, 105, 1240, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, false));
			epq.offer(new SHFEBookEvent("AUF5", 1, 32, 1241, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, false));
			epq.offer(new SHFEBookEvent("AUF5", 1, 85, 1242, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, false));
			epq.offer(new SHFEBookEvent("AUF5", 1, 64, 1245, 1000, -1, SHFEBookEvent.ActionType.ADD, 1412758451, true));
			// AUF5 - Remove	
			epq.offer(new SHFEBookEvent("AUF5", 0, 0, 1234, 1000, -1, SHFEBookEvent.ActionType.REMOVE, 1412758454, false));
			epq.offer(new SHFEBookEvent("AUF5", 0, 11, 1231, 1000, -1, SHFEBookEvent.ActionType.REMOVE, 1412758454, false));
			epq.offer(new SHFEBookEvent("AUF5", 1, 888, 1238, 1000, -1, SHFEBookEvent.ActionType.REMOVE, 1412758454, false));
			epq.offer(new SHFEBookEvent("AUF5", 1, 0, 1241, 1000, -1, SHFEBookEvent.ActionType.REMOVE, 1412758454, true));
			// AUF5 - Update
			epq.offer(new SHFEBookEvent("AUF5", 0, 120, 1233, 1000, -1, 100, 1233, -1, SHFEBookEvent.ActionType.UPDATE, 1412758455, false));
			epq.offer(new SHFEBookEvent("AUF5", 1, 90, 1243, 1000, -1, 105, 1240, -1, SHFEBookEvent.ActionType.UPDATE, 1412758455, true));

		} else if (sym.displayName.equals("AUG5")) {
			// AUG5 - Add
			epq.offer(new SHFEBookEvent("AUG5", 0, 20, 1335, 1000, 1, SHFEBookEvent.ActionType.ADD, 1412758458, false));
			epq.offer(new SHFEBookEvent("AUG5", 0, 88, 1333, 1000, 2, SHFEBookEvent.ActionType.ADD, 1412758458, false));
			epq.offer(new SHFEBookEvent("AUG5", 0, 50, 1328, 1000, 3, SHFEBookEvent.ActionType.ADD, 1412758458, false));	
			epq.offer(new SHFEBookEvent("AUG5", 1, 99, 1338, 1000, 4, SHFEBookEvent.ActionType.ADD, 1412758458, false));
			epq.offer(new SHFEBookEvent("AUG5", 1, 24, 1341, 1000, 5, SHFEBookEvent.ActionType.ADD, 1412758458, false));
			epq.offer(new SHFEBookEvent("AUG5", 1, 73, 1345, 1000, 6, SHFEBookEvent.ActionType.ADD, 1412758458, true));
			// AUG5 - Remove	
			epq.offer(new SHFEBookEvent("AUG5", 0, 25, 1328, 1000, 3, SHFEBookEvent.ActionType.REMOVE, 1412758459, false));	
			epq.offer(new SHFEBookEvent("AUG5", 1, 0, 1338, 1000, 4, SHFEBookEvent.ActionType.REMOVE, 1412758459, true));
			// AUG5 - Update
			epq.offer(new SHFEBookEvent("AUG5", 0, 25, 1334, 1000, 7, 88, 1333, 2, SHFEBookEvent.ActionType.UPDATE, 1412758462, false));	
			epq.offer(new SHFEBookEvent("AUG5", 1, 99, 1345, 1000, 8, 73, 1345, 6, SHFEBookEvent.ActionType.UPDATE, 1412758462, true));

		}
	}

	@Override
	protected void readNextBlock() {
		// TODO: add logic to check for end-of-file & read next chunk of data from file,
		//       keep reading in this loop until latest timestamp is different from last read timestamp
	}
}
