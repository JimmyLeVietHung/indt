package indt.datasource;

import java.util.*;
import java.io.*;
import java.text.*;

import indt.event.*;
import indt.utils.*;

public class CNINFileDataSource extends FileDataSource {
	private BufferedReader br = null;
	private static final String FILEROOTPATH = "C:/cnindata/cf/";
	//private static final String FILEROOTPATH = "/Users/eow/Private/cn/cf/";
	private Symbol symbol = null;
	private int multiplier = 0;
	private int orderNum = 0;
	private int prevBidPrice = 0;
	private int prevBidSize = 0;
	private int prevAskPrice = 0;
	private int prevAskSize = 0;
	
	//dte like 201309
	//sym like DCa0001
	//so that file is FILEROOTPATH/cf_$dtem/$sym_$dte.csv
	public CNINFileDataSource(int dte, Symbol sym, int mult) {
		super(dte, sym);
		symbol = sym;
		multiplier = mult;
		
		String filePath = String.format("%s/cf_%dm/%s_%d.csv", FILEROOTPATH, dte, sym.displayName, dte);
		try {
			br = new BufferedReader(new FileReader(filePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		skipNextBlock(); //clear header
		while (!hasNext()) //read until have first proper line
			readNextBlock();  
	}

	private void skipNextBlock() {
		try {
			br.readLine(); 
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	@Override
	protected void readNextBlock() {		
		try {
			if (br == null) {
				return;
			}
			String line = br.readLine();
			if (line == null) {
				br.close();
				br = null;
			} else {
				String[] lineEntries = line.split(",");
				//last few columns are bid/ask & sizes
				String dateTimeStr = lineEntries[2]; 
				String bidPriceStr = lineEntries[lineEntries.length - 4];
				String askPriceStr = lineEntries[lineEntries.length - 3];
				String bidSizeStr = lineEntries[lineEntries.length - 2];
				String askSizeStr = lineEntries[lineEntries.length - 1];
				
				long dateTimeNum = parseTime (dateTimeStr);
				int bidPrice = Math.round(Float.parseFloat(bidPriceStr) * multiplier);
				int askPrice = Math.round(Float.parseFloat(askPriceStr) * multiplier);
				int bidSize = Integer.valueOf(bidSizeStr);
				int askSize = Integer.valueOf(askSizeStr);
				
				if ((bidPrice == 0) || (askPrice == 0) || (bidSize == 0) || (askSize == 0)) { //faulty data
					return;
				}
				
				if (orderNum==0) {
					//first one is add event for bid & ask
					epq.offer(new SHFEBookEvent(symbol.displayName, 0, bidSize, bidPrice, multiplier, -1, SHFEBookEvent.ActionType.ADD, dateTimeNum, false));
					epq.offer(new SHFEBookEvent(symbol.displayName, 1, askSize, askPrice, multiplier, -1, SHFEBookEvent.ActionType.ADD, dateTimeNum, true));
					
				} else {
					//subsequent one is update event for bid & ask
					epq.offer(new SHFEBookEvent(symbol.displayName, 0, bidSize, bidPrice, multiplier, -1, prevBidSize, prevBidPrice, -1, SHFEBookEvent.ActionType.UPDATE, dateTimeNum, false));
					epq.offer(new SHFEBookEvent(symbol.displayName, 1, askSize, askPrice, multiplier, -1, prevAskSize, prevAskPrice, -1, SHFEBookEvent.ActionType.UPDATE, dateTimeNum, true));
				}
				prevBidPrice = bidPrice;
				prevBidSize = bidSize;
				prevAskPrice = askPrice;
				prevAskSize = askSize;
				orderNum+=2;
				//System.out.println("OrderNum: " + orderNum);
				
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	private long parseTime(String datetimeStr) {
		String dateStr = datetimeStr.substring(0, 10);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date parsedDate = null;
		try {
			parsedDate =  formatter.parse(dateStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		int firstColonPos = datetimeStr.indexOf(':');
		String hrStr = datetimeStr.substring(firstColonPos-2, firstColonPos);
		String minStr = datetimeStr.substring(firstColonPos+1, firstColonPos+3);
		String secStr = datetimeStr.substring(firstColonPos+4, firstColonPos+6);
		int hr = Integer.valueOf(hrStr);
		int min = Integer.valueOf(minStr);
		int sec = Integer.valueOf(secStr);
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(parsedDate);
		cal.add(Calendar.HOUR, hr);
		cal.add(Calendar.MINUTE, min);
		cal.add(Calendar.SECOND, sec);
		
		Date parsedDateTime = cal.getTime();
		long result = parsedDateTime.getTime() * 1000;
		
		return result;
	}
}
