package com.numericalmethod;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.numericalmethod.suanshu.algebra.linear.matrix.doubles.matrixtype.dense.DenseMatrix;
import com.numericalmethod.suanshu.algebra.linear.vector.doubles.ImmutableVector;
import com.numericalmethod.suanshu.algebra.linear.vector.doubles.Vector;
import com.numericalmethod.suanshu.algebra.linear.vector.doubles.dense.DenseVector;
import com.numericalmethod.suanshu.stats.cointegration.CointegrationMLE;
import com.numericalmethod.suanshu.stats.cointegration.JohansenAsymptoticDistribution;
import com.numericalmethod.suanshu.stats.cointegration.JohansenTest;
import com.numericalmethod.suanshu.stats.timeseries.datastructure.multivariate.realtime.inttime.MultivariateSimpleTimeSeries;

public class copyCointegration {

	public copyCointegration() {

	}
	public static void main(String[] args) throws Exception {
		copyCointegration co = new copyCointegration();
		//tets average beta
		//co.test1("E://HighFrequencyTrading//testAverageBeta_input3",0.1, 1, 15, 30, 90);
		
		//test updating beta from opening  to current minutes
		//co.test2("E://HighFrequencyTrading//test_input3",0.1, 1, 15, 30, 90);
		// from 5 days previous weeks
		co.test3(1,"E://HighFrequencyTrading//test_input3",0.1, 1, 15, 30, 90);
		co.test3(5,"E://HighFrequencyTrading//test_input3",0.1, 1, 15, 30, 90);
		//yesterday
		
		
	}
	public void test1(String path,double confident_level, int maxx, int paraTime,int marketSignal1, int marketSignal2) throws IOException{
		File folder= new File(path);
		File[]listFiles=folder.listFiles();
		//AUDA0_NZDA0_outputDAYdata_with_tcost_IntermediateData
		for (File fd : listFiles) {
			double[] beta=this.getAverageCointegrationVector(fd.getAbsolutePath()+File.separator+"trainDATA", 0.1);
 this.testFolders2(fd.getName(),beta , fd.getAbsolutePath()+File.separator+"testDATA", confident_level, maxx, paraTime, marketSignal1, marketSignal2, true);
 this.testFolders2(fd.getName(),beta , fd.getAbsolutePath()+File.separator+"testDATA", confident_level, maxx, paraTime, marketSignal1, marketSignal2, false);

	}
}
	public void test2(String path,double confident_level,int maxx, int paraTime,int marketSignal1,int marketSignal2) throws IOException{

		File folder= new File(path);
		File[]listFiles=folder.listFiles();
		  for (File fname : listFiles) {
			  this.testFolder(fname.getName(),fname.getAbsolutePath(),confident_level,maxx,paraTime,marketSignal1,marketSignal2, true);
			  this.testFolder(fname.getName(),fname.getAbsolutePath(),confident_level,maxx,paraTime,marketSignal1,marketSignal2, false);

		  }
		
	}
	
	
	public void test3(int numberOfLastDays_to_Calculate_Beta,String path,double confident_level,int maxx, int paraTime,int marketSignal1,int marketSignal2) throws IOException{

		File folder= new File(path);
		File[]listFiles=folder.listFiles();
		  for (File fname : listFiles) {
			  this.testFolders3(numberOfLastDays_to_Calculate_Beta,fname.getName(),fname.getAbsolutePath(),confident_level,maxx,paraTime,marketSignal1,marketSignal2, true);
			  this.testFolders3(numberOfLastDays_to_Calculate_Beta,fname.getName(),fname.getAbsolutePath(),confident_level,maxx,paraTime,marketSignal1,marketSignal2, false);

		  }
		
	}
	
	public static double getMean(ArrayList<Double>data)
    {
        double sum = 0.0;
        for(double a : data)
            sum += a;
        return sum/data.size();
    }

    public static double getVariance(ArrayList<Double>data)
    {
        double mean = getMean(data);
        double temp = 0.0;
        for(double a :data)
            temp += (mean-a)*(mean-a);
        return temp/data.size();
    }

    public static double getStdDev(ArrayList<Double>data)
    {
        return Math.sqrt(getVariance(data));
    }
	
	public ArrayList<double[]> getALLcointegrationVectors(String folderPath,double confident_level) throws IOException{
		ArrayList<double[]> ALL = new ArrayList<double[]> ();
		    File folder= new File(folderPath);
		    //File folder= new File("E://HighFrequencyTrading//input3//New folder");
		    File[]listFiles=folder.listFiles();
		    for (File fname : listFiles) {
		            ArrayList<ArrayList<Double>> DAYdata=this.readDAYdata2(fname.getAbsolutePath());
			//		System.out.println(DAYdata.get(0).get(0));
		//			System.out.println(DAYdata.get(1).get(0));

					try{
						double[] beta= this.getCointegratioRatioOf_one_file(confident_level, DAYdata);
					//System.out.println("beta "+beta.length);
					if(beta.length>0) ALL.add(beta);
					}
					catch(Exception e){
						e.printStackTrace();
					}
	}
	return ALL;
	}
	public double[] getAverageCointegrationVector(String folderPath,double confident_level) throws IOException{
		double[] coV=this.getAverage(this.getALLcointegrationVectors(folderPath, confident_level));	
		String s="";
		for(int i=0;i<coV.length;i++)s=s+coV[i]+",";
		System.out.println("Average covector: "+ s);
		return coV;}
	
	public double testFolder(String pairNAME,String folderPath,double confident_level,int maxx, int paraTime,int marketSignal1,int marketSignal2,boolean withTcost) throws IOException{
		ArrayList<Double> balances = new ArrayList<Double>();
		String ss= "_with_tcost";
		if(withTcost){}else ss="_NO_tcost";
		String path = pairNAME+ss+ ".csv";
		    int count = 0;
		    FileWriter writer = new FileWriter(path);
		    File folder= new File(folderPath);
		    //File folder= new File("E://HighFrequencyTrading//input3//New folder");
		    File[]listFiles=folder.listFiles();
		    for (File fname : listFiles) {
		            count = count + 1;
		            System.out.println("file " + count + " out of "+listFiles.length );
		            ArrayList<ArrayList<Double>> DAYdata=this.readDAYdata(fname.getAbsolutePath());
		            double balance= this.meanReversionUsingUpdatingCointegrationRatio(DAYdata,confident_level, maxx, paraTime, marketSignal1, marketSignal2, withTcost);
		            //System.out.println("file " + count + " :" + balance);
		            String[] r = fname.getName().split("_");
		            String date = r[4].substring(0, r[4].length()-4);
		            //20121223
		            String yyyy=date.substring(0, 4);
		            String mm=date.substring(4,6);
		            String dd=date.substring(6,8);
		            //if (balance != 0){
		            writer.write(mm+"/"+dd+"/"+yyyy+","+balance+"\n");
		            balances.add(balance);
		            
		    }
		    double shRatio = (Math.sqrt(252) * getMean(balances)) / getStdDev(balances);
		    System.out.println("Sharp ratio: " + shRatio);
		    writer.flush();
		    writer.close();
		    return shRatio;
	}
	
	public double testFolders2(String pairNAME,double[] coVector, String folderPath,double confident_level,int maxx, int paraTime,int marketSignal1,int marketSignal2,boolean withTcost) throws IOException{
		ArrayList<Double> balances = new ArrayList<Double>();
		String ss= "_with_tcost";
		if(withTcost){}else ss="_NO_tcost";
		String path = pairNAME+ss+ ".csv";
		    int count = 0;
		    FileWriter writer = new FileWriter(path);
		    File folder= new File(folderPath);
		    //File folder= new File("E://HighFrequencyTrading//input3//New folder");
		    File[]listFiles=folder.listFiles();
		    for (File fname : listFiles) {
		            count = count + 1;
		            System.out.println("file " + count + " out of "+listFiles.length );
		            ArrayList<ArrayList<Double>> dayDATA=this.readDAYdata(fname.getAbsolutePath());
		            double balance= this.meanReversionCointegration(coVector, dayDATA, confident_level, maxx, paraTime, marketSignal1, marketSignal2, withTcost);
		            //System.out.println("file " + count + " :" + balance);
		            String[] r = fname.getName().split("_");
		            String date = r[4].substring(0, r[4].length()-4);
		            //20121223
		            String yyyy=date.substring(0, 4);
		            String mm=date.substring(4,6);
		            String dd=date.substring(6,8);
		           // if (balance != 0)
		            writer.write(mm+"/"+dd+"/"+yyyy+","+balance+"\n");
		            balances.add(balance);
		            
		    }
		    double shRatio = (Math.sqrt(252) * getMean(balances)) / getStdDev(balances);
		    System.out.println("Sharp ratio: " + shRatio);
		    writer.flush();
		    writer.close();
		    return shRatio;
	}
	
	public double testFolders3(int numberOfLastDays_to_Calculate_Beta,String pairNAME, String folderPath,double confident_level,int maxx, int paraTime,int marketSignal1,int marketSignal2,boolean withTcost) throws IOException{
		//double[] coVector=null;
		ArrayList<Double> balances = new ArrayList<Double>();
		String ss= "_with_tcost";
		if(withTcost){}else ss="_NO_tcost";
		ss=numberOfLastDays_to_Calculate_Beta+"_"+ss;
		String path = pairNAME.replaceFirst("outputDAYdata", "")+ss+ ".csv";
		    int count = 0;
		    FileWriter writer = new FileWriter(path);
		    File folder= new File(folderPath);
		    //File folder= new File("E://HighFrequencyTrading//input3//New folder");
		    File[]listFiles=folder.listFiles();
		    //ArrayList<ArrayList<ArrayList<Double>>> week= new ArrayList<ArrayList<ArrayList<Double>>>(5);
		    ArrayList<ArrayList<ArrayList<Double>>> theLast_n_days_data= new ArrayList<ArrayList<ArrayList<Double>>>(numberOfLastDays_to_Calculate_Beta);
		    for (File fname : listFiles) {
		    	 count = count + 1;
		    	 ArrayList<ArrayList<Double>> dayDATA=this.readDAYdata(fname.getAbsolutePath());
		    	 if (count<=numberOfLastDays_to_Calculate_Beta){
		    		// week.add(dayDATA);
		    		 theLast_n_days_data.add(dayDATA);
	            }
		          
		    	 else{
		            
		            System.out.println("file " + count + " out of "+listFiles.length );
		            ArrayList<ArrayList<Double>> addALL= new ArrayList<ArrayList<Double>>(dayDATA.size()/3);
		            for(int j=0;j<dayDATA.size()/3;j++)addALL.add(new ArrayList<Double>());
		            
		            for(int j=0;j<dayDATA.size()/3;j++){
		            	for(int k=0;k<theLast_n_days_data.size();k++){
		            	addALL.get(j).addAll(theLast_n_days_data.get(k).get((j+1)*3-1));}}
		            
		            double[] coVector=this.cointegrated(confident_level, addALL);
		            //ArrayList<ArrayList<Double>> dayDATA=this.readDAYdata(fname.getAbsolutePath());
		            double balance= this.meanReversionCointegration(coVector, dayDATA, confident_level, maxx, paraTime, marketSignal1, marketSignal2, withTcost);
		            //System.out.println("file " + count + " :" + balance);
		            String[] r = fname.getName().split("_");
		            String date = r[4].substring(0, r[4].length()-4);
		            //20121223
		            String yyyy=date.substring(0, 4);
		            String mm=date.substring(4,6);
		            String dd=date.substring(6,8);
		            if (balance != 0.0){
		            writer.write(mm+"/"+dd+"/"+yyyy+","+balance+"\n");
		            balances.add(balance);
		            }
		            // updating, keep n=5 days to calculate cointegration vector
		            theLast_n_days_data.remove(0);
		            theLast_n_days_data.add(dayDATA);
		            
		            
		            }
		            
		    }
		    double shRatio = (Math.sqrt(252) * getMean(balances)) / getStdDev(balances);
		    System.out.println("Sharp ratio: " + shRatio);
		    writer.flush();
		    writer.close();
		    return shRatio;
	}
	
	public double meanReversionUsingUpdatingCointegrationRatio(ArrayList<ArrayList<Double>> dayDATA,double confident_level, int maxx, double paraTime,int marketSignal1,int marketSignal2, boolean withTcost){
	    /*
	     AverageCointerationVectors: the average cointegration ratio are  estimated from historical data.
	     The updating ratios will be ignore if they are VERY far away from the average.
	     ex: (1.0,3.2), (1.0,3.6), (1.0,3.9) are OK
	     but (1.0,-91.2), (1.0,173) are eliminate
	     */
		double balance = 0;
	    int position = 0;
	    int numberColumn=dayDATA.size();
	    int leng=dayDATA.get(0).size();
	    //store column M_1,M_2,...for johansen test
	    //ArrayList<ArrayList<Double>> M_s=new ArrayList<ArrayList<Double>>();
	    //store 30 values of column M_1,M_2,...for market signal
	    //marketSignal1=30,marketSignal2=90. Compare MA30 to MA90
	    ArrayList<ArrayList<Double>> M30_s=new ArrayList<ArrayList<Double>>();
	    ArrayList<ArrayList<Double>> M90_s=new ArrayList<ArrayList<Double>>();
	    ArrayList<ArrayList<Double>> upToCurrent=new ArrayList<ArrayList<Double>>();
	    //Date Time H_1 L_1 M_1 H_2 L_2 M_2 H_3 L_3 M_3
	    for (int i=0;i<numberColumn/3;i++){
	
	    	//M_s.add(new ArrayList<Double>());
	    	M30_s.add(new ArrayList<Double>());
	    	M90_s.add(new ArrayList<Double>());
	    	upToCurrent.add(new ArrayList<Double>());
	    	}
	    
	    //int total = 0;
	    //int count = 0;
	    double[] beta= new double[0];
	    for (int i=0;i<leng;i++){
	    	// MA30
	        if (M30_s.get(0).size() < marketSignal1){ //marketSignal1=30: if 30 minutes list doesn't have enough 30 minutes dayData, then continue to add dayData into the list
	            for (int k=0;k<numberColumn/3;k++)
	    		 	M30_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	            }
	        else {// # if enough 30 minutes dayData, I delete 1st dayData point, and add in 1 dayData point at the end
	            for (int k=0;k<numberColumn/3;k++){
	            	M30_s.get(k).remove(0);
	    		 	M30_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	    		 	}
	        }
	        
	        //MA90
	        if (M90_s.get(0).size() < marketSignal2){ //marketSignal2=90: if 90 minutes list doesn't have enough 90 minutes dayData, then continue to add dayData into the list
	            for (int k=0;k<numberColumn/3;k++)
	    		 	M90_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	            }
	        else {// # if enough 90 minutes dayData, I delete 1st dayData point, and add in 1 dayData point at the end
	            for (int k=0;k<numberColumn/3;k++){
	            	M90_s.get(k).remove(0);
	    		 	M90_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	    		 	}
	        }
	        // keep all data up to current of the day. Cointegration is long term relation ship
	        for (int k=0;k<numberColumn/3;k++)
	        	upToCurrent.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	        
	       
	        //double[] beta= new double[0];
	        if (i >= 89&&((i + 1) % paraTime == 0)){//only process from minute 90, and every 15 minutes afterward
	        	//total = total + 1;
	        	//System.out.println(i);
	        	try {
	        	beta=this.cointegrated(confident_level, upToCurrent);
	        	}catch (Exception e){e.printStackTrace();
	        		beta= new double[0];
	        		System.out.println("Exception: NO solution  solving linear equation system");
	        	}
	            if (beta.length > 0){
	                //count = count + 1;
	                double MA30=MovingAverage(beta,M30_s);
	                double MA90=MovingAverage(beta,M90_s);

	                if (position > 0){
	                    if (MA30 < MA90){
	                        if (position < maxx){
	                            if (withTcost){
	                        //if we have some long lots, and it current down, and will up, then we long more until numberOf lots=maxx
	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	                    	// 2A, 5C will up,3B will down =>long 2A,5C, short 3B <=>long only one (2A - 3B+ 5C)
							for(int j=0;j<beta.length;j++){
					if (beta[j]>=0)balance=balance-beta[j]*(dayDATA.get(3*j).get(i));
					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
					// buy at ask,sell at bid
					else balance=balance-beta[j]*(dayDATA.get((j+1)*3-2).get(i));
				}
	                            }
	                            else{
	                                for(int j=0;j<beta.length;j++)
										balance=balance-beta[j]*(dayDATA.get((j+1)*3-1).get(i));
	                            }
	                            position += 1;
	                    }
	                        }
	                    else if (MA30 > MA90) { 
	                             if (withTcost){
	                        //if we have some long lots, and price current up, and will down=>short ALL lots
	                    	// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B <=>short ALL (2A - 3B+ 5C)
							for(int j=0;j<beta.length;j++){
					if (beta[j]>=0)balance=balance+position*beta[j]*(dayDATA.get((j+1)*3-2).get(i));
					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
					// buy at ask,sell at bid
					else balance=balance+position*beta[j]*(dayDATA.get(3*j).get(i));
				}
	                            }
							
	                            else{
	                                for(int j=0;j<beta.length;j++)
	                                	balance=balance+position*beta[j]*(dayDATA.get((j+1)*3-1).get(i));
				}
	                         position = 0;
	                }
	                }// if position >0
					
	                    
	                    else if (position == 0){
	                    if (MA30 < MA90){  
	                    	 if (withTcost){
	 	                        //position = 0, and it current down, and will up, then we long ONE LOT
	 	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	 	                    	// 2A, 5C will up,3B will down =>long 2A,5C, short 3B <=>long only one (2A - 3B+ 5C)
	 							for(int j=0;j<beta.length;j++){
	 					if (beta[j]>=0)balance=balance-beta[j]*(dayDATA.get(3*j).get(i));
	 					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L coulmn, (j+1)*3-1: M column
	 					// buy at ask,sell at bid
	 					else balance=balance-beta[j]*(dayDATA.get((j+1)*3-2).get(i));
	 				}
	 	                            }
	 	                            else{
	 	                                for(int j=0;j<beta.length;j++)
	 										balance=balance-beta[j]*(dayDATA.get((j+1)*3-1).get(i));
	 	                            }
	                            position += 1;
	                    }
	                         
	                    else if (MA30 > MA90){  
	                      if (withTcost){
	                    		// position = 0, and it's now up, will down, then we short one lot
	  	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	  	                    	// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B <=>short one (2A - 3B+ 5C)
	  							for(int j=0;j<beta.length;j++){
	  					if (beta[j]>=0)balance=balance+beta[j]*(dayDATA.get((j+1)*3-2).get(i));
	  					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
	  					// buy at ask,sell at bid
	  					else balance=balance+beta[j]*(dayDATA.get(3*j).get(i));
	  				}
	  	                            }
	  							
	  	                            else{
	  	                                for(int j=0;j<beta.length;j++){
	  					balance=balance+beta[j]*(dayDATA.get((j+1)*3-1).get(i));
	  					
	  				}
	  	                            }
	                        position -= 1;
	                    }
	                 // else MA30=MA90:  stay the same
	                   }
	                        
	                     
	                else {//position < 0
	                    if (MA30 > MA90){ 
	                    //if we have some short lots, and it now up, will down, then we short one lot more until -maxx
	                        if (position > -maxx){
	                        	if (withTcost){
		                    		
		  	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
		  	                    	// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B <=>short one (2A - 3B+ 5C)
		  							for(int j=0;j<beta.length;j++){
		  					if (beta[j]>=0)balance=balance+beta[j]*(dayDATA.get((j+1)*3-2).get(i));
		  					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
		  					// buy at ask,sell at bid
		  					else balance=balance+beta[j]*(dayDATA.get(3*j).get(i));
		  				}
		  	                            }
		  							
		  	                            else{
		  	                                for(int j=0;j<beta.length;j++){
		  					balance=balance+beta[j]*(dayDATA.get((j+1)*3-1).get(i));
		  					
		  				}
		  	                            }    
	                            position -= 1;  // short  1 lot more
	                        }
	                    }
	                            
	                    else if (MA30 < MA90){//long all
	                        
	                        if (withTcost){
		                        //if we have some long lots, and it current down, and will up, then we long ALLs
		                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
		                    	// 2A, 5C will up,3B will down =>long 2A,5C, short 3B <=>long only ALL lot (2A - 3B+ 5C)
								for(int j=0;j<beta.length;j++){
						if (beta[j]>=0)balance=balance-(-position)*beta[j]*(dayDATA.get(3*j).get(i));
						// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
						// buy at ask,sell at bid
						else balance=balance-(-position)*beta[j]*(dayDATA.get((j+1)*3-2).get(i));
					}
		                            }
		                            else{
		                                for(int j=0;j<beta.length;j++){
											balance=balance-(-position)*beta[j]*(dayDATA.get((j+1)*3-1).get(i));
						
					}
		                            }
	                        position = 0;
	                
	                }
	                    
	                }// else position<0
	        } //  if (beta.length > 0)
	        }// if paratime 
	    }//for loop

	    //closing the day
	    if (position > 0){// closing position at the last minute of the day
	       if (withTcost){
                    for (int j=0;j<beta.length;j++){
                    int u=-100;
                    //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
                    //3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
                    //short 2A,5C, long 3B
                    if (beta[j]>0) u=(j+1)*3-2; else u=3*j;
                    balance=balance+position*beta[j]*(dayDATA.get(u).get(leng-1));
            		}
                    }
	       else{
         
               for (int j=0;j<beta.length;j++)
            	   balance=balance+position*beta[j]*(dayDATA.get((j+1)*3-1).get(leng-1));	    
           }
	    }
	    
	    else {//position < 0 closing position at the last minute of the day
		       if (withTcost){
	                    for (int j=0;j<beta.length;j++){
	                    int u=-100;
	                    //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	                    //3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
	                    //long 2A,5C, short 3B
	                    if (beta[j]>0)u=3*j; else u=(j+1)*3-2;
	                    balance=balance+position*beta[j]*(dayDATA.get(u).get(leng-1));
	            		}
	                    }
		       else{
	         
	               for (int j=0;j<beta.length;j++)
	            	   balance=balance+position*beta[j]*(dayDATA.get((j+1)*3-1).get(leng-1));	    
	           }
		    }
	    
	     
	   /* if (((double)count) / total < 0.2){
	    	System.out.println("percent of cointegrated  "+((double)count) / total);
	        balance = 0;
	    }*/
	    return balance;
}

	public double meanReversionUsingCointegrationRatio_weeklyUpdating(double[] beta,ArrayList<ArrayList<Double>> dayDATA,double confident_level, int maxx, double paraTime,int marketSignal1,int marketSignal2, boolean withTcost){
	    /*
	     AverageCointerationVectors: the average cointegration ratio are  estimated from historical data.
	     The updating ratios will be ignore if they are VERY far away from the average.
	     ex: (1.0,3.2), (1.0,3.6), (1.0,3.9) are OK
	     but (1.0,-91.2), (1.0,173) are eliminate
	     */
		//double[] beta=this.cointegrated(confident_level, previousWeekToCurrent);
		double balance = 0;
	    int position = 0;
	    int numberColumn=dayDATA.size();
	    int leng=dayDATA.get(0).size();
	    //store column M_1,M_2,...for johansen test
	    //ArrayList<ArrayList<Double>> M_s=new ArrayList<ArrayList<Double>>();
	    //store 30 values of column M_1,M_2,...for market signal
	    //marketSignal1=30,marketSignal2=90. Compare MA30 to MA90
	    ArrayList<ArrayList<Double>> M30_s=new ArrayList<ArrayList<Double>>();
	    ArrayList<ArrayList<Double>> M90_s=new ArrayList<ArrayList<Double>>();
	   // ArrayList<ArrayList<Double>> upToCurrent=new ArrayList<ArrayList<Double>>();
	    //Date Time H_1 L_1 M_1 H_2 L_2 M_2 H_3 L_3 M_3
	    for (int i=0;i<numberColumn/3;i++){
	    	//M_s.add(new ArrayList<Double>());
	    	M30_s.add(new ArrayList<Double>());
	    	M90_s.add(new ArrayList<Double>());
	    	//upToCurrent.add(new ArrayList<Double>());
	    	}
	    
	    //int total = 0;
	    //int count = 0;
	   // double[] beta= new double[0];
	    for (int i=0;i<leng;i++){
	    	// MA30
	        if (M30_s.get(0).size() < marketSignal1){ //marketSignal1=30: if 30 minutes list doesn't have enough 30 minutes dayData, then continue to add dayData into the list
	            for (int k=0;k<numberColumn/3;k++)
	    		 	M30_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	            }
	        else {// # if enough 30 minutes dayData, I delete 1st dayData point, and add in 1 dayData point at the end
	            for (int k=0;k<numberColumn/3;k++){
	            	M30_s.get(k).remove(0);
	    		 	M30_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	    		 	}
	        }
	        
	        //MA90
	        if (M90_s.get(0).size() < marketSignal2){ //marketSignal2=90: if 90 minutes list doesn't have enough 90 minutes dayData, then continue to add dayData into the list
	            for (int k=0;k<numberColumn/3;k++)
	    		 	M90_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	            }
	        else {// # if enough 90 minutes dayData, I delete 1st dayData point, and add in 1 dayData point at the end
	            for (int k=0;k<numberColumn/3;k++){
	            	M90_s.get(k).remove(0);
	    		 	M90_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	    		 	}
	        }
	        // keep all data up to current of the day. Cointegration is long term relation ship
	      //  for (int k=0;k<numberColumn/3;k++)
	        	//upToCurrent.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	        
	       
	        //double[] beta= new double[0];
	        if (i >= 89&&((i + 1) % paraTime == 0)){//only process from minute 90, and every 15 minutes afterward
	        	//total = total + 1;
	        	//System.out.println(i);
	        	try {
	        	//beta=this.cointegrated(confident_level, upToCurrent);
	        	}catch (Exception e){e.printStackTrace();
	        		beta= new double[0];
	        		System.out.println("Exception: NO solution  solving linear equation system");
	        	}
	            if (beta.length > 0){
	                //count = count + 1;
	                double MA30=MovingAverage(beta,M30_s);
	                double MA90=MovingAverage(beta,M90_s);

	                if (position > 0){
	                    if (MA30 < MA90){
	                        if (position < maxx){
	                            if (withTcost){
	                        //if we have some long lots, and it current down, and will up, then we long more until numberOf lots=maxx
	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	                    	// 2A, 5C will up,3B will down =>long 2A,5C, short 3B <=>long only one (2A - 3B+ 5C)
							for(int j=0;j<beta.length;j++){
					if (beta[j]>=0)balance=balance-beta[j]*(dayDATA.get(3*j).get(i));
					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
					// buy at ask,sell at bid
					else balance=balance-beta[j]*(dayDATA.get((j+1)*3-2).get(i));
				}
	                            }
	                            else{
	                                for(int j=0;j<beta.length;j++)
										balance=balance-beta[j]*(dayDATA.get((j+1)*3-1).get(i));
	                            }
	                            position += 1;
	                    }
	                        }
	                    else if (MA30 > MA90) { 
	                             if (withTcost){
	                        //if we have some long lots, and price current up, and will down=>short ALL lots
	                    	// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B <=>short ALL (2A - 3B+ 5C)
							for(int j=0;j<beta.length;j++){
					if (beta[j]>=0)balance=balance+position*beta[j]*(dayDATA.get((j+1)*3-2).get(i));
					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
					// buy at ask,sell at bid
					else balance=balance+position*beta[j]*(dayDATA.get(3*j).get(i));
				}
	                            }
							
	                            else{
	                                for(int j=0;j<beta.length;j++)
	                                	balance=balance+position*beta[j]*(dayDATA.get((j+1)*3-1).get(i));
				}
	                         position = 0;
	                }
	                }// if position >0
					
	                    
	                    else if (position == 0){
	                    if (MA30 < MA90){  
	                    	 if (withTcost){
	 	                        //position = 0, and it current down, and will up, then we long ONE LOT
	 	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	 	                    	// 2A, 5C will up,3B will down =>long 2A,5C, short 3B <=>long only one (2A - 3B+ 5C)
	 							for(int j=0;j<beta.length;j++){
	 					if (beta[j]>=0)balance=balance-beta[j]*(dayDATA.get(3*j).get(i));
	 					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L coulmn, (j+1)*3-1: M column
	 					// buy at ask,sell at bid
	 					else balance=balance-beta[j]*(dayDATA.get((j+1)*3-2).get(i));
	 				}
	 	                            }
	 	                            else{
	 	                                for(int j=0;j<beta.length;j++)
	 										balance=balance-beta[j]*(dayDATA.get((j+1)*3-1).get(i));
	 	                            }
	                            position += 1;
	                    }
	                         
	                    else if (MA30 > MA90){  
	                      if (withTcost){
	                    		// position = 0, and it's now up, will down, then we short one lot
	  	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	  	                    	// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B <=>short one (2A - 3B+ 5C)
	  							for(int j=0;j<beta.length;j++){
	  					if (beta[j]>=0)balance=balance+beta[j]*(dayDATA.get((j+1)*3-2).get(i));
	  					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
	  					// buy at ask,sell at bid
	  					else balance=balance+beta[j]*(dayDATA.get(3*j).get(i));
	  				}
	  	                            }
	  							
	  	                            else{
	  	                                for(int j=0;j<beta.length;j++){
	  					balance=balance+beta[j]*(dayDATA.get((j+1)*3-1).get(i));
	  					
	  				}
	  	                            }
	                        position -= 1;
	                    }
	                 // else MA30=MA90:  stay the same
	                   }
	                        
	                     
	                else {//position < 0
	                    if (MA30 > MA90){ 
	                    //if we have some short lots, and it now up, will down, then we short one lot more until -maxx
	                        if (position > -maxx){
	                        	if (withTcost){
		                    		
		  	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
		  	                    	// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B <=>short one (2A - 3B+ 5C)
		  							for(int j=0;j<beta.length;j++){
		  					if (beta[j]>=0)balance=balance+beta[j]*(dayDATA.get((j+1)*3-2).get(i));
		  					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
		  					// buy at ask,sell at bid
		  					else balance=balance+beta[j]*(dayDATA.get(3*j).get(i));
		  				}
		  	                            }
		  							
		  	                            else{
		  	                                for(int j=0;j<beta.length;j++){
		  					balance=balance+beta[j]*(dayDATA.get((j+1)*3-1).get(i));
		  					
		  				}
		  	                            }    
	                            position -= 1;  // short  1 lot more
	                        }
	                    }
	                            
	                    else if (MA30 < MA90){//long all
	                        
	                        if (withTcost){
		                        //if we have some long lots, and it current down, and will up, then we long ALLs
		                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
		                    	// 2A, 5C will up,3B will down =>long 2A,5C, short 3B <=>long only ALL lot (2A - 3B+ 5C)
								for(int j=0;j<beta.length;j++){
						if (beta[j]>=0)balance=balance-(-position)*beta[j]*(dayDATA.get(3*j).get(i));
						// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
						// buy at ask,sell at bid
						else balance=balance-(-position)*beta[j]*(dayDATA.get((j+1)*3-2).get(i));
					}
		                            }
		                            else{
		                                for(int j=0;j<beta.length;j++){
											balance=balance-(-position)*beta[j]*(dayDATA.get((j+1)*3-1).get(i));
						
					}
		                            }
	                        position = 0;
	                
	                }
	                    
	                }// else position<0
	        } //  if (beta.length > 0)
	        }// if paratime 
	    }//for loop

	    //closing the day
	    if (position > 0){// closing position at the last minute of the day
	       if (withTcost){
                    for (int j=0;j<beta.length;j++){
                    int u=-100;
                    //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
                    //3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
                    //short 2A,5C, long 3B
                    if (beta[j]>0) u=(j+1)*3-2; else u=3*j;
                    balance=balance+position*beta[j]*(dayDATA.get(u).get(leng-1));
            		}
                    }
	       else{
         
               for (int j=0;j<beta.length;j++)
            	   balance=balance+position*beta[j]*(dayDATA.get((j+1)*3-1).get(leng-1));	    
           }
	    }
	    
	    else {//position < 0 closing position at the last minute of the day
		       if (withTcost){
	                    for (int j=0;j<beta.length;j++){
	                    int u=-100;
	                    //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	                    //3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
	                    //long 2A,5C, short 3B
	                    if (beta[j]>0)u=3*j; else u=(j+1)*3-2;
	                    balance=balance+position*beta[j]*(dayDATA.get(u).get(leng-1));
	            		}
	                    }
		       else{
	         
	               for (int j=0;j<beta.length;j++)
	            	   balance=balance+position*beta[j]*(dayDATA.get((j+1)*3-1).get(leng-1));	    
	           }
		    }
	    
	     
	   /* if (((double)count) / total < 0.2){
	    	System.out.println("percent of cointegrated  "+((double)count) / total);
	        balance = 0;
	    }*/
	    return balance;
}
	
	
	
	
	
	public double meanReversionCointegration(double[] coVector,ArrayList<ArrayList<Double>> dayDATA,double confident_level, int maxx, double paraTime,int marketSignal1,int marketSignal2, boolean withTcost){
	    /*
	     AverageCointerationVectors: the average cointegration ratio are  estimated from historical data.
	     The updating ratios will be ignore if they are VERY far away from the average.
	     ex: (1.0,3.2), (1.0,3.6), (1.0,3.9) are OK
	     but (1.0,-91.2), (1.0,173) are eliminate
	     */
		if (coVector.length ==0) return 0.0;
		double balance = 0;
	    int position = 0;
	    int numberColumn=dayDATA.size();
	    int leng=dayDATA.get(0).size();
	    //store column M_1,M_2,...for johansen test
	    //ArrayList<ArrayList<Double>> M_s=new ArrayList<ArrayList<Double>>();
	    //store 30 values of column M_1,M_2,...for market signal
	    //marketSignal1=30,marketSignal2=90. Compare MA30 to MA90
	    ArrayList<ArrayList<Double>> M30_s=new ArrayList<ArrayList<Double>>();
	    ArrayList<ArrayList<Double>> M90_s=new ArrayList<ArrayList<Double>>();
	   // ArrayList<ArrayList<Double>> upToCurrent=new ArrayList<ArrayList<Double>>();
	    //Date Time H_1 L_1 M_1 H_2 L_2 M_2 H_3 L_3 M_3
	    for (int i=0;i<numberColumn/3;i++){
	    	//M_s.add(new ArrayList<Double>());
	    	M30_s.add(new ArrayList<Double>());
	    	M90_s.add(new ArrayList<Double>());
	    	//upToCurrent.add(new ArrayList<Double>());
	    	}
	    
	    //int total = 0;
	    //int count = 0;
	    //double[] coVector= new double[0];
	    for (int i=0;i<leng;i++){
	    	// MA30
	        if (M30_s.get(0).size() < marketSignal1){ //marketSignal1=30: if 30 minutes list doesn't have enough 30 minutes dayData, then continue to add dayData into the list
	            for (int k=0;k<numberColumn/3;k++)
	    		 	M30_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	            }
	        else {// # if enough 30 minutes dayData, I delete 1st dayData point, and add in 1 dayData point at the end
	            for (int k=0;k<numberColumn/3;k++){
	            	M30_s.get(k).remove(0);
	    		 	M30_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	    		 	}
	        }
	        
	        //MA90
	        if (M90_s.get(0).size() < marketSignal2){ //marketSignal2=90: if 90 minutes list doesn't have enough 90 minutes dayData, then continue to add dayData into the list
	            for (int k=0;k<numberColumn/3;k++)
	    		 	M90_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	            }
	        else {// # if enough 90 minutes dayData, I delete 1st dayData point, and add in 1 dayData point at the end
	            for (int k=0;k<numberColumn/3;k++){
	            	M90_s.get(k).remove(0);
	    		 	M90_s.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	    		 	}
	        }
	        // keep all data up to current of the day. Cointegration is long term relation ship
	        /*
	        for (int k=0;k<numberColumn/3;k++)
	        	upToCurrent.get(k).add(dayDATA.get((k+1)*3-1).get(i));
	        */
	       
	        //double[] beta= new double[0];
	        if (i >= 89&&((i + 1) % paraTime == 0)){//only process from minute 90, and every 15 minutes afterward
	        	//total = total + 1;
	        	//System.out.println(i);
	        	/*
	        	try {
	        	beta=this.cointegrated(confident_level, upToCurrent,AverageCointerationVectors);
	        	}catch (Exception e){e.printStackTrace();
	        		beta= new double[0];
	        		System.out.println("Exception: NO solution  solving linear equation system");
	        	}*/
	            if (coVector.length > 0){
	                //count = count + 1;
	                double MA30=MovingAverage(coVector,M30_s);
	                double MA90=MovingAverage(coVector,M90_s);
	                //System.out.println("MA90= "+MA90);
	                if (position > 0){
	                    if (MA30 < MA90){
	                        if (position < maxx){
	                            if (withTcost){
	                        //if we have some long lots, and it current down, and will up, then we long more until numberOf lots=maxx
	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	                    	// 2A, 5C will up,3B will down =>long 2A,5C, short 3B <=>long only one (2A - 3B+ 5C)
							for(int j=0;j<coVector.length;j++){
					if (coVector[j]>=0)balance=balance-coVector[j]*(dayDATA.get(3*j).get(i));
					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
					// buy at ask,sell at bid
					else balance=balance-coVector[j]*(dayDATA.get((j+1)*3-2).get(i));
				}
	                            }
	                            else{
	                                for(int j=0;j<coVector.length;j++)
										balance=balance-coVector[j]*(dayDATA.get((j+1)*3-1).get(i));
	                            }
	                            position += 1;
	                    }
	                        }
	                    else if (MA30 > MA90) { 
	                             if (withTcost){
	                        //if we have some long lots, and price current up, and will down=>short ALL lots
	                    	// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B <=>short ALL (2A - 3B+ 5C)
							for(int j=0;j<coVector.length;j++){
					if (coVector[j]>=0)balance=balance+position*coVector[j]*(dayDATA.get((j+1)*3-2).get(i));
					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
					// buy at ask,sell at bid
					else balance=balance+position*coVector[j]*(dayDATA.get(3*j).get(i));
				}
	                            }
							
	                            else{
	                                for(int j=0;j<coVector.length;j++)
	                                	balance=balance+position*coVector[j]*(dayDATA.get((j+1)*3-1).get(i));
				}
	                         position = 0;
	                }
	                }// if position >0
					
	                    
	                    else if (position == 0){
	                    if (MA30 < MA90){  
	                    	 if (withTcost){
	 	                        //position = 0, and it current down, and will up, then we long ONE LOT
	 	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	 	                    	// 2A, 5C will up,3B will down =>long 2A,5C, short 3B <=>long only one (2A - 3B+ 5C)
	 							for(int j=0;j<coVector.length;j++){
	 					if (coVector[j]>=0)balance=balance-coVector[j]*(dayDATA.get(3*j).get(i));
	 					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L coulmn, (j+1)*3-1: M column
	 					// buy at ask,sell at bid
	 					else balance=balance-coVector[j]*(dayDATA.get((j+1)*3-2).get(i));
	 				}
	 	                            }
	 	                            else{
	 	                                for(int j=0;j<coVector.length;j++)
	 										balance=balance-coVector[j]*(dayDATA.get((j+1)*3-1).get(i));
	 	                            }
	                            position += 1;
	                    }
	                         
	                    else if (MA30 > MA90){  
	                      if (withTcost){
	                    		// position = 0, and it's now up, will down, then we short one lot
	  	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	  	                    	// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B <=>short one (2A - 3B+ 5C)
	  							for(int j=0;j<coVector.length;j++){
	  					if (coVector[j]>=0)balance=balance+coVector[j]*(dayDATA.get((j+1)*3-2).get(i));
	  					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
	  					// buy at ask,sell at bid
	  					else balance=balance+coVector[j]*(dayDATA.get(3*j).get(i));
	  				}
	  	                            }
	  							
	  	                            else{
	  	                                for(int j=0;j<coVector.length;j++){
	  					balance=balance+coVector[j]*(dayDATA.get((j+1)*3-1).get(i));
	  					
	  				}
	  	                            }
	                        position -= 1;
	                    }
	                 // else MA30=MA90:  stay the same
	                   }
	                        
	                     
	                else {//position < 0
	                    if (MA30 > MA90){ 
	                    //if we have some short lots, and it now up, will down, then we short one lot more until -maxx
	                        if (position > -maxx){
	                        	if (withTcost){
		                    		
		  	                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
		  	                    	// 2A, 5C will down ,3B will up =>short 2A,5C, long 3B <=>short one (2A - 3B+ 5C)
		  							for(int j=0;j<coVector.length;j++){
		  					if (coVector[j]>=0)balance=balance+coVector[j]*(dayDATA.get((j+1)*3-2).get(i));
		  					// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
		  					// buy at ask,sell at bid
		  					else balance=balance+coVector[j]*(dayDATA.get(3*j).get(i));
		  				}
		  	                            }
		  							
		  	                            else{
		  	                                for(int j=0;j<coVector.length;j++){
		  					balance=balance+coVector[j]*(dayDATA.get((j+1)*3-1).get(i));
		  					
		  				}
		  	                            }    
	                            position -= 1;  // short  1 lot more
	                        }
	                    }
	                            
	                    else if (MA30 < MA90){//long all
	                        
	                        if (withTcost){
		                        //if we have some long lots, and it current down, and will up, then we long ALLs
		                        //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
		                    	// 2A, 5C will up,3B will down =>long 2A,5C, short 3B <=>long only ALL lot (2A - 3B+ 5C)
								for(int j=0;j<coVector.length;j++){
						if (coVector[j]>=0)balance=balance-(-position)*coVector[j]*(dayDATA.get(3*j).get(i));
						// 3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
						// buy at ask,sell at bid
						else balance=balance-(-position)*coVector[j]*(dayDATA.get((j+1)*3-2).get(i));
					}
		                            }
		                            else{
		                                for(int j=0;j<coVector.length;j++){
											balance=balance-(-position)*coVector[j]*(dayDATA.get((j+1)*3-1).get(i));
						
					}
		                            }
	                        position = 0;
	                
	                }
	                    
	                }// else position<0
	        } //  if (beta.length > 0)
	        }// if paratime 
	    }//for loop

	    //closing the day
	    if (position > 0){// closing position at the last minute of the day
	       if (withTcost){
                    for (int j=0;j<coVector.length;j++){
                    int u=-100;
                    //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
                    //3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
                    //short 2A,5C, long 3B
                    if (coVector[j]>0) u=(j+1)*3-2; else u=3*j;
                    balance=balance+position*coVector[j]*(dayDATA.get(u).get(leng-1));
            		}
                    }
	       else{
         
               for (int j=0;j<coVector.length;j++)
            	   balance=balance+position*coVector[j]*(dayDATA.get((j+1)*3-1).get(leng-1));	    
           }
	    }
	    
	    else {//position < 0 closing position at the last minute of the day
		       if (withTcost){
	                    for (int j=0;j<coVector.length;j++){
	                    int u=-100;
	                    //a lot = a combination of long and short A, B, C:  2A - 3B+ 5C
	                    //3j=H_1,H_2,H_3column,(j+1)*3-2= L column, (j+1)*3-1: M column
	                    //long 2A,5C, short 3B
	                    if (coVector[j]>0)u=3*j; else u=(j+1)*3-2;
	                    balance=balance+position*coVector[j]*(dayDATA.get(u).get(leng-1));
	            		}
	                    }
		       else{
	         
	               for (int j=0;j<coVector.length;j++)
	            	   balance=balance+position*coVector[j]*(dayDATA.get((j+1)*3-1).get(leng-1));	    
	           }
		    }
	    
	    /* 
	    if (((double)count) / total < 0.2){
	    	System.out.println("percent of cointegrated  "+((double)count) / total);
	        balance = 0;
	    }*/
	    return balance;
}
	
	public static double MovingAverage(double[] beta,
			ArrayList<ArrayList<Double>> M) {
		int windowSize = M.get(0).size();
		ArrayList<Double> residual = new ArrayList<Double>(windowSize);
		for (int i = 0; i < windowSize; i++) {
			double i_th = 0;
			for (int j = 0; j < beta.length; j++) {
				i_th = i_th + beta[j] * M.get(j).get(i);
			}
			residual.add(i_th);
		}

		return average(residual);
	}

	public static double average(List<Double> list) {
		if (list == null || list.isEmpty())
			return 0.0;
		double sum = 0;
		int n = list.size();
		for (int i = 0; i < n; i++)
			sum += list.get(i);
		return sum / n;
	}

	public ArrayList<ArrayList<Double>> readDAYdata(String fname) {
		//Date Time H_1 L_1 H_2 L_2 : 6 colums, M_1=(H_1+L_1)/2, M_2=(H_2+L_2)/2
		int size=this.countSize(fname)-2;//size=4
	//	System.out.println(size);
		int size1=size*3/2;
		ArrayList<ArrayList<Double>> dayDATA = new ArrayList<ArrayList<Double>>(size1);
		for (int i = 0; i < size1; i++)
			dayDATA.add(new ArrayList<Double>());
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		// Date Time H_1 L_1  H_2 L_2  
		int count=0;
		try {

			br = new BufferedReader(new FileReader(fname));
			while ((line = br.readLine()) != null) {
				count++;
				if (count>=2){
				String[] r = line.split(cvsSplitBy);
				for (int i = 0; i < size/2; i++){
					int h=(i+1)*3-3; //H column		0,3,6
					int l=(i+1)*3-2;// L column		1,4,7
					int m=(i+1)*3-1;// mid =(H+L)/2 2,5,8
					dayDATA.get(h).add(Double.parseDouble(r[2*i+2].trim()));
					dayDATA.get(l).add(Double.parseDouble(r[2*i+1+2].trim()));//j-1+2
					dayDATA.get(m).add((Double.parseDouble(r[2*i+2].trim())+Double.parseDouble(r[2*i+1+2].trim()))/2);//j-2+2
					
				}
				}
				

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		return dayDATA;
	}
	
	public ArrayList<ArrayList<Double>> readDAYdata2(String fname) {
		//Date Time H_1 L_1 H_2 L_2 : 6 colums, M_1=(H_1+L_1)/2, M_2=(H_2+L_2)/2
		int size=this.countSize(fname)-2;//size=4
	//	System.out.println(size);
	//	int size1=size*3/2;
		int size2=size/2;
		ArrayList<ArrayList<Double>> dayDATA = new ArrayList<ArrayList<Double>>(size2);
		for (int i = 0; i < size2; i++)
			dayDATA.add(new ArrayList<Double>());
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		// Date Time H_1 L_1  H_2 L_2  
		int count=0;
		try {

			br = new BufferedReader(new FileReader(fname));
			while ((line = br.readLine()) != null) {
				count++;
				if (count>=2){
				String[] r = line.split(cvsSplitBy);
				for (int i = 0; i < size/2; i++){
					int h=2*i; //H column		0,3,6
					int l=2*i+1;// L column		1,4,7
					dayDATA.get(i).add((Double.parseDouble(r[2*i+2].trim())+Double.parseDouble(r[2*i+1+2].trim()))/2);//j-2+2
					
				}
				}
				

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		return dayDATA;
	}
	
	public int countSize(String fname) {
		int count=0;
		int x=0;
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		// Date Time H_1 L_1 C_1 H_2 L_2 C_2
		// we dont use C_1, and C_2 colunm

		try {

			br = new BufferedReader(new FileReader(fname));
			while (x<3&&(line = br.readLine()) != null) {
				String[] r = line.split(cvsSplitBy);
				count=r.length;
				x++;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return count;
	}

	public static double[] getRandomeData1() {
		File file = new File("matrix.txt");
		double[] mydata = new double[100];
		try {

			Scanner sc = new Scanner(file);
			int i = 0;

			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] r = line.split(",");
				mydata[i] = Double.parseDouble(r[0].trim());
				i++;
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		//System.out.print(mydata.length);
		return mydata;
	}

	public static double[] getRandomeData2() {
		File file = new File("matrix.txt");
		double[] mydata = new double[100];
		try {

			Scanner sc = new Scanner(file);
			int i = 0;

			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] r = line.split(",");
				mydata[i] = Double.parseDouble(r[1].trim());
				i++;
			}
			sc.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		System.out.print(mydata.length);
		return mydata;
	}
	public double[] cointegrated1111(double level, double[][] myDATA) {
		// level = 0.1 =>90%
		// return the first cointegration vector if cointegrated else return
		// null
		// the i-th column represent for the i-th time series
		JohansenTest johansenTest = new JohansenTest(
				JohansenAsymptoticDistribution.Test.EIGEN,
				JohansenAsymptoticDistribution.TrendType.CONSTANT, 2);
		// DenseMatrix matrix = new DenseMatrix(100, 2);
		// matrix.setColumn(1, data1);
		// matrix.setColumn(2, data2);
		// MultivariateSimpleTimeSeries ts = new
		// MultivariateSimpleTimeSeries(matrix);
		MultivariateSimpleTimeSeries ts = new MultivariateSimpleTimeSeries(
				myDATA);

		CointegrationMLE coint = new CointegrationMLE(ts, true, 2, null);
		ImmutableVector stats = johansenTest.getStats(coint);
		System.out.println("test statistics");
		System.out.println(stats);

		Vector eigenvalues = coint.getEigenvalues();
		System.out.println("eigenvalues:");
		System.out.println(eigenvalues);
		System.out.println("cointegrating factors");
		System.out.println(coint.beta(1));
		System.out.println(coint.beta(2));
		System.out.println("speeds of adjustment");
		System.out.println(coint.alpha());
		// System.out.println("test statistics");
		int r = johansenTest.r(coint, level);
		double[] beta = null;
		if (r > 0) {
			beta = coint.beta(1).toArray();
		}
		return beta;
	}
	public double[] cointegrated2(double level, ArrayList<ArrayList<Double>> myDATA,ArrayList<Double>AverageCointerationVectors) {
		int n=myDATA.get(0).size();
		int m=myDATA.size();
		double[][] myDATA1= new double[n][m];
		for (int i=0;i<n;i++)
			for (int j=0;j<m;j++){
		myDATA1[i][j]=myDATA.get(j).get(i);
			}
		CointegrationMLE coint = new CointegrationMLE( new MultivariateSimpleTimeSeries(myDATA1), true, 2);

        Vector eigenvalues = coint.getEigenvalues();
        //System.out.println("eigenvalues:");
       // System.out.println(eigenvalues);
        //System.out.println("cointegrating factors");
        //System.out.println(coint.beta());
        //System.out.println("speeds of adjustment");
        //System.out.println(coint.alpha());
       // System.out.println("test statistics");
        JohansenTest test = new JohansenTest(
                JohansenAsymptoticDistribution.Test.EIGEN,
                JohansenAsymptoticDistribution.TrendType.CONSTANT,
                eigenvalues.size());
        //System.out.println(test.getStats(coint));
		int r = test.r(coint, level);
		double[] beta = new double[0];
		if (r > 0&&r<m) {
			beta = coint.beta(1).toArray();
			//if (isOUTlier(AverageCointerationVectors,beta))
				//beta = new double[0];// ignore outlier
			}
		
		return beta;
	}
/*
public boolean isOUTlier(ArrayList<Double>AverageCointerationVectors,double[] beta){
	// cointegration vector ust NOT far away from historical , average one
	boolean b= false;
	for(int j=0;j<AverageCointerationVectors.size();j++){
		if(Math.abs(beta[j])>=9){
			b=true; 
			System.out.println("cointegetion is too BIGGGG or SMALLLLL");
			break;
		
		if(Math.abs(beta[j]-AverageCointerationVectors.get(j))>10){
			b=true; 
			System.out.println("cointegetion is too BIGGGG or SMALLLLL");
			break;
			}
		}}
	return b;
}**/
public void testCointegratedForOneinputFile(String name){
	ArrayList<ArrayList<Double>> myDATA=this.readDAYdata(name);
	this.cointegrated_ignore_average(0.3, myDATA);
}

public double[] cointegrated_ignore_average(double level,ArrayList<ArrayList<Double>> myDATA) {
	// level = 0.1 =>90%
	// return the first cointegration vector 
	// the i-th column represent for the i-th time series
	/*int rowNumber = myDATA.get(0).size();
	int colNumber = myDATA.size();
	DenseMatrix matrix = new DenseMatrix(rowNumber, colNumber);
	for (int i = 0; i < colNumber; i++) {
		matrix.setColumn(i + 1, new DenseVector(myDATA.get(i)));
	}
	MultivariateSimpleTimeSeries ts = new MultivariateSimpleTimeSeries(
			matrix);
	*/
	
	int n=myDATA.get(0).size();
	int m=myDATA.size();
	double[][] myDATA1= new double[n][m];
	for (int i=0;i<n;i++)
		for (int j=0;j<m;j++){
	myDATA1[i][j]=myDATA.get(j).get(i);
		}
	
	
		for (int j=0;j<m;j++){
			//System.out.println(myDATA1[0][j]);
		}
	
	CointegrationMLE coint = new CointegrationMLE( new MultivariateSimpleTimeSeries(myDATA1), true, 2);
	
	//System.out.println(matrix.toString());
	//CointegrationMLE coint = new CointegrationMLE(ts, true, 2, null);
	//CointegrationMLE coint = new CointegrationMLE(ts, true, 2);
	Vector eigenvalues = coint.getEigenvalues();
	//System.out.println("eigenvalues:");
	//System.out.println(eigenvalues);
	JohansenTest johansenTest = new JohansenTest(
			JohansenAsymptoticDistribution.Test.EIGEN,
			JohansenAsymptoticDistribution.TrendType.CONSTANT, eigenvalues.size());
	
	ImmutableVector stats = johansenTest.getStats(coint);
	//System.out.println("test statistics");
	//System.out.println(stats);


	System.out.println("cointegrating factors");
	System.out.println(coint.beta(1));
	System.out.println(coint.beta(2));
	//System.out.println("speeds of adjustment");
	//System.out.println(coint.alpha());
	// System.out.println("test statistics");
	int r = johansenTest.r(coint, level);
	double[] beta = new double[0];
	if (r > 0) {
		beta = coint.beta(1).toArray();
	}
	return beta;
}	
public double[] cointegrated(double level,
			ArrayList<ArrayList<Double>> myDATA) {
		// level = 0.1 =>90%
		// return the first cointegration vector 
		// the i-th column represent for the i-th time series
	//for (int j=0;j<myDATA.size();j++){
		//System.out.println("data "+myDATA.get(j).get(0));
	//}
		int rowNumber = myDATA.get(0).size();
		int colNumber = myDATA.size();
		DenseMatrix matrix = new DenseMatrix(rowNumber, colNumber);
		for (int i = 0; i < colNumber; i++) {
			matrix.setColumn(i + 1, new DenseVector(myDATA.get(i)));
		}
		MultivariateSimpleTimeSeries ts = new MultivariateSimpleTimeSeries(
				matrix);
		double[] beta = new double[0];
		try{
		CointegrationMLE coint = new CointegrationMLE(ts, true, 2);
		Vector eigenvalues = coint.getEigenvalues();
		JohansenTest johansenTest = new JohansenTest(
				JohansenAsymptoticDistribution.Test.EIGEN,
				JohansenAsymptoticDistribution.TrendType.CONSTANT, eigenvalues.size());
		
		ImmutableVector stats = johansenTest.getStats(coint);
		int r = johansenTest.r(coint, level);
		//double[] beta = new double[0];
		if (r > 0&&r<colNumber) {
			beta = coint.beta(1).toArray();
			System.out.println("cointegrating factors");
			System.out.println(coint.beta(1));
			}
		}
		catch(Exception e){e.printStackTrace();}
		return beta;
	}

public double[] getAverage(ArrayList<double[]> ALLcointegrationVectors){
	int size = ALLcointegrationVectors.get(0).length;
	int numberVector=ALLcointegrationVectors.size();
	double[] beta= new double[size];
	for(int i=0;i<size;i++){
	ArrayList<Double> list= new ArrayList<Double>(numberVector)	;
	for(int j=0;j<numberVector;j++){list.add(ALLcointegrationVectors.get(j)[i]);}
	beta[i]=average(list);
	}
	return beta;
	
}

public double[] getCointegratioRatioOf_one_file(double level,ArrayList<ArrayList<Double>> myDATA) {
	// level = 0.1 =>90%
	// return the first cointegration vector 
	// the i-th column represent for the i-th time series

	int rowNumber = myDATA.get(0).size();
	int colNumber = myDATA.size();
	DenseMatrix matrix = new DenseMatrix(rowNumber, colNumber);
	for (int i = 0; i < colNumber; i++) {
		matrix.setColumn(i + 1, new DenseVector(myDATA.get(i)));
	}
	MultivariateSimpleTimeSeries ts = new MultivariateSimpleTimeSeries(
			matrix);
	//System.out.println(matrix.toString());
	//CointegrationMLE coint = new CointegrationMLE(ts, true, 2, null);
	CointegrationMLE coint = new CointegrationMLE(ts, true, 2);
	

	Vector eigenvalues = coint.getEigenvalues();
	//System.out.println("eigenvalues:");
	//System.out.println(eigenvalues);
	JohansenTest johansenTest = new JohansenTest(
			JohansenAsymptoticDistribution.Test.EIGEN,
			JohansenAsymptoticDistribution.TrendType.CONSTANT, eigenvalues.size());
	
	ImmutableVector stats = johansenTest.getStats(coint);
	//System.out.println("test statistics");
	//System.out.println(stats);


	//System.out.println("cointegrating factors");
   // System.out.println(coint.beta());

	//System.out.println(coint.beta(1));
	//System.out.println(coint.beta(2));
	//System.out.println("speeds of adjustment");
	//System.out.println(coint.alpha());
	// System.out.println("test statistics");
	int r = johansenTest.r(coint, level);
	double[] beta = new double[0];
	if (r>0&&r<colNumber ) {
		//System.out.println("cointegrating factors");
		beta = coint.beta(1).toArray();
		System.out.println(coint.beta(1));
		//System.out.println(coint.beta());
		//if (isOUTlier(AverageCointerationVectors,beta))
				//beta = new double[0];// ignore outlier
		
	}
	return beta;
}





}