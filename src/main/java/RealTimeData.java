// RealTimeData.java    
// Version 1.0
// 20141028
// R. Holowczak

// Import Java utilities and Interactive Brokers API
import java.util.Vector;

import com.ib.client.Contract;
import com.ib.client.ContractDetails;
import com.ib.client.EClientSocket;
import com.ib.client.EWrapper;
import com.ib.client.Execution;
import com.ib.client.Order;
import com.ib.client.OrderState;
import com.ib.client.TagValue;
import com.ib.client.CommissionReport;
import com.ib.client.UnderComp;

// RealTimeData Class is an implementation of the 
// IB API EWrapper class
public class RealTimeData implements EWrapper
{
	// Keep track of the next ID
	private int nextOrderID = 0;
	// The IB API Client Socket object
	private EClientSocket client = null;
    
	public RealTimeData ()
	{
		// Create a new EClientSocket object
      		client = new EClientSocket (this);
		// Connect to the TWS or IB Gateway application
		// Leave null for localhost
		// Port Number (should match TWS/IB Gateway configuration
		client.eConnect (null, 7496, 0);

		// Pause here for connection to complete
		try 
		{
			// Thread.sleep (1000);
			while (! (client.isConnected()));
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		// Create a new contract
		Contract contract = new Contract ();
		contract.m_symbol = "EUR";
		contract.m_exchange = "IDEALPRO";
		contract.m_secType = "CASH";
		contract.m_currency = "USD";
		// Create a TagValue list
		Vector mktDataOptions = new Vector();
		// Make a call to start off data retrieval
		//client.reqMktData(0, contract, null, false, mktDataOptions);
		client.reqMktData(0, contract,null, false);
		
     
        try {Thread.sleep(5000);} catch (Exception e) {e.printStackTrace();}
		
		// At this point our call is done and any market data events
		// will be returned via the tickPrice method

	} // end RealTimeData

	
	 public void error(Exception e) {
		   e.printStackTrace();
			   System.out.println(e);
		   }

		  

		   public void error(int id, int errorCode, String errorMsg) {
		      System.out.println("error: id = " + id + ", code = " + errorCode + ", msg = " + errorMsg);
		   }
	
 

    public void bondContractDetails(ContractDetails contractDetails)
    {
    }

    public void contractDetails(ContractDetails contractDetails)
    {
    }

   

    public void displayGroupList(int requestId, String contraftInfo)
    {
    }


    public void displayGroupUpdated(int requestId, String contractInfo)
    {
    }

    
  

  

    public void openOrder(int orderId, Contract contract, Order order, OrderState orderState) {
        System.out.println("openOrder");
     }

     public void openOrderEnd() {
        System.out.println("openOrderEnd");
     }

     public void updateAccountValue(String key, String value, String currency, String accountName) {
        System.out.println("updateAccountValue");
     }

     public void updatePortfolio(Contract contract, int position, double marketPrice, double marketValue,
        double averageCost, double unrealizedPNL, double realizedPNL, String accountName) {
        System.out.println("updatePortfolio");
     }

     public void updateAccountTime(String timeStamp) {
        System.out.println("updateAccountTime: " + timeStamp);
     }

     public void accountDownloadEnd(String accountName) {
        System.out.println("accountDownloadEnd");
     }

     public void contractDetails(int reqId, ContractDetails contractDetails) {
        System.out.println("contractDetails");
     }

     public void contractDetailsEnd(int reqId) {
        System.out.println("contractDetailsEnd");
     }

     public void bondContractDetails(int reqId, ContractDetails contractDetails) {
        System.out.println("bondContractDetails");
     }

     public void execDetails(int reqId, Contract contract, Execution execution) {
        System.out.println("execDetails");
     }

     public void execDetailsEnd(int reqId) {
        System.out.println("execDetailsEnd");
     }

     public void updateMktDepth(int tickerId, int position, int operation, int side, double price, int size) {
        System.out.println("updateMktDepth");
     }

     public void updateMktDepthL2(int tickerId, int position, String marketMaker, int operation,
        int side, double price, int size) {
        System.out.println("updateMktDepthL2");
     }

     public void updateNewsBulletin(int msgId, int msgType, String message, String origExchange) {
        System.out.println("updateNewsBulletin");
     }

     public void managedAccounts(String accountsList) {
        System.out.println("managedAccounts");
     }

     public void receiveFA(int faDataType, String xml) {
        System.out.println("receiveFA");
     }

     public void historicalData(int reqId, String date, double open, double high, double low,
        double close, int volume, int count, double WAP, boolean hasGaps) {
  	   //logIn("historicalData, "+reqId+" , "+date+" , "+open+" , "+close+" , hasgaps: , "+hasGaps);
     }

     public void scannerParameters(String xml) {
        System.out.println("scannerParameters");
     }

     public void scannerData(int reqId, int rank, ContractDetails contractDetails, String distance,
        String benchmark, String projection, String legsStr) {
        System.out.println("scannerData");
     }

     public void scannerDataEnd(int reqId) {
        System.out.println("scannerDataEnd");
     }

     public void realtimeBar(int reqId, long time, double open, double high, double low, double close,
        long volume, double wap, int count) {
        System.out.println("realtimeBar");
     }

     public void currentTime(long millis) {
        System.out.println("currentTime");
     }

     public void fundamentalData(int reqId, String data) {
        System.out.println("fundamentalData");
     }

     public void deltaNeutralValidation(int reqId, UnderComp underComp) {
        System.out.println("deltaNeutralValidation");
     }

  @Override
  public void tickOptionComputation(int tickerId, int field, double impliedVol,
  		double delta, double optPrice, double pvDividend, double gamma,
  		double vega, double theta, double undPrice) {
  	// TODO Auto-generated method stub
  	
  }

  @Override
  public void marketDataType(int reqId, int marketDataType) {
  	// TODO Auto-generated method stub
  	
  }

  @Override
  public void commissionReport(CommissionReport commissionReport) {
  	// TODO Auto-generated method stub
  	
  }

  @Override
  public void position(String account, Contract contract, int pos, double avgCost) {
  	// TODO Auto-generated method stub
  	
  }

  @Override
  public void positionEnd() {
  	// TODO Auto-generated method stub
  	
  }

  @Override
  public void accountSummary(int reqId, String account, String tag, String value,
  		String currency) {
  	// TODO Auto-generated method stub
  	
  }

  @Override
  public void accountSummaryEnd(int reqId) {
  	// TODO Auto-generated method stub
  	
  }

  

   



    public void tickEFP(int tickerId, int tickType, double basisPoints, String formattedBasisPoints, double impliedFuture, int holdDays,
            String futureExpiry, double dividendImpact, double dividendsToExpiry)
{  String toBeReturned = "Ticker ID = " + tickerId + "\n";
toBeReturned += "Tick Type = " + tickType + "\n";
toBeReturned += "Basis Points = " + basisPoints + "\n";
toBeReturned += "Formatted Basis Points = " + formattedBasisPoints + "\n";
toBeReturned += "Implied Future = " + impliedFuture + "\n";
toBeReturned += "Hold Days = " + holdDays + "\n";
toBeReturned += "Future Expiry = " + futureExpiry + "\n";
toBeReturned += "Dividend Impact = " + dividendImpact + "\n";
toBeReturned += "Dividends to Expiry = " + dividendsToExpiry + "\n";
System.out.println(toBeReturned);

}

public void orderStatus(int orderId, String status, int filled, int remaining, double avgFillPrice, int permId, int parentId, 
double lastFillPrice, int clientId, String whyHeld) 
{   String toBeReturned = "Client ID = " + clientId + "\n";
   toBeReturned += "Order ID = " + orderId + "\n";
   toBeReturned += "Status := " + status + "\n";
   toBeReturned += "Filled = " + filled + "\n";
   toBeReturned += "Remaining = " + remaining + "\n";
   toBeReturned += "Avg. Fill Price = " + avgFillPrice + "\n";
   toBeReturned += "Perm ID = " + permId + "\n";
   toBeReturned += "Parent ID = " + parentId + "\n";
   toBeReturned += "Last Fill Price = " + lastFillPrice+ "\n";
   toBeReturned += "HELD ?? = " + whyHeld + "\n";
System.out.println(toBeReturned);
}
    

    public void tickPrice(int tickerId, int field, double price, int canAutoExecute) 
    {
 	   try
 	    {
 		   System.out.println(" Price Function - tickPrice: tickerId = "+tickerId+", field = "+field+", price = "+price);
 	    }
 	    catch(Exception e)
 	    {
 	    	e.printStackTrace();
 	      System.out.println("ERROR in TickPrice: "  + e);
 	    }
      
    }


   

    public void error(String str)
    {
	// Print out the error message
        System.err.println (str);
    }

    public void connectionClosed() {
        System.out.println("--------------------- CLOSED ---------------------");
     }
    
    public void nextValidId (int orderId)
    {
	// Return the next valid OrderID
        nextOrderID = orderId;
    }


   /* public void tickPrice(int orderId, int field, double price,
            int canAutoExecute)
    {
	try 
	{
		// Print out the current price
		// field will provide the price type:
		// 1 = bid,  2 = ask, 4 = last
		// 6 = high, 7 = low, 9 = close
		System.out.println("tickPrice: " + orderId + "," + field + "," + price);
	} 
	catch (Exception e)
        {
		e.printStackTrace ();
        }
 
	
    }*/
    public void tickSize(int tickerId, int field, int size) {
        System.out.println("Size Fundtion - tickSize: tickerId = "+tickerId+", field = "+field+", size = "+size);
     }

     public void tickGeneric(int tickerId, int tickType, double value) 
     { String toBeReturned = "Ticker ID = " + tickerId + "\n";
        toBeReturned += "Tick Type = " + tickType + "\n";
        toBeReturned += "Value = " + value + "\n";
        System.out.println(toBeReturned);
     }

     public void tickString(int tickerId, int tickType, String value)
     {  String toBeReturned = "Ticker ID = " + tickerId + "\n";
  	  toBeReturned += "Tick Type = " + tickType + "\n";
  	  toBeReturned += "Value = " + value + "\n";
  	  System.out.println(toBeReturned);
     }
    
    

     public void tickSnapshotEnd(int tickerId) 
     { String toBeReturned = "Ticker ID = " + tickerId + "\n";
       System.out.println(toBeReturned);
     }

     public void tickOptionComputation(int tickerId, int field, double impliedVol,
        double delta, double modelPrice, double pvDividend) {
        System.out.println("tickOptionComputation");
     }


    public static void main (String args[])
    {
        try
        {
		// Create an instance
		// At this time a connection will be made
		// and the request for market data will happen
		RealTimeData myData = new RealTimeData();
        }
        catch (Exception e)
        {
            e.printStackTrace ();
        }
    } // end main

} // end public class RealTimeData