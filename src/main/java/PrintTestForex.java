import com.ib.client.Contract;
import com.ib.client.EClientSocket;
import com.ib.client.Order;
import com.numericalmethod.copyCointegration;

public class PrintTestForex {
    public static void main(String[] args) {
		
    	
        PrintWrapper wrapper = new PrintWrapper();
        EClientSocket client = new EClientSocket(wrapper);
        int clientId = 3;
        client.eConnect("", 7496, clientId);
        Contract con = new Contract();
        con.m_symbol = "EUR";
       // con.m_localSymbol = "EUR";
        con.m_currency = "USD";
        con.m_exchange = "IDEALPRO";
        con.m_secType = "CASH";
        int tickerId = 1;//System.currentTimeMillis()/1000;
        //wrapper.nextValidId(tickerId);
        client.reqMktData(tickerId, con, "", true);
        System.out.println("Now Placing A Buy Order :" + con.m_localSymbol + "." + con.m_currency);
        Order ord = new Order();
        ord.m_totalQuantity = 2;
        //tickerId++;
        ord.m_orderId = tickerId;
        ord.m_action = "BUY";
        ord.m_orderType = "MKT";
        ord.m_lmtPrice = 0.0;
        client.placeOrder(tickerId, con, ord); 
        try {Thread.sleep(50);} catch (Exception e) {}
        
//        System.out.println("Now Placing A Sell Order :" + con.m_localSymbol + "." + con.m_currency);
//        ord.m_totalQuantity = 15000;
//        ord.m_action = "SELL";
//        tickerId++;
//        ord.m_orderId = tickerId;
//        client.placeOrder(tickerId, con, ord); System.out.println();
//        try {Thread.sleep(50);} catch (Exception e) {}
//        System.out.println("Now Exiting the System");
       //System.exit(1);
        
    }

}